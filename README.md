# CRF Virtual Meetings Frontend

This is the Next.js app that comprises CRF's virtual meetings frontend. It queries the CRF Virtual Meeting CMS (Craft CMS), and the CRF Agenda Manager (Emma) for data.

## Requirements

This app requires node 12.20.0 (exactly) in order to align with the production environment (Digital Ocean Apps platform). Please align your local environment. Mac users can use the program [n](https://github.com/tj/n) to manage local Node versions.

It is not necessary to create a domain on a local server. All development is performed with the built-in local dev server.

## Installation

1. Clone this repository.

1. Create a .env.local file at the root of the project and enter the values from the production app.
   * Production app environment variables can be retrieved from the DO App [here](https://cloud.digitalocean.com/apps/48caec1e-00a4-4c0a-8e16-5db8a3a5d33b/settings/crf-virtual-meetings-frontend?i=01f579)
   * To work with a local version of Emma, in .env.local set the EMMA_API_URL and the NEXT_PUBLIC_EMMA_API_URL
   * To work with a local version of the Craft CMS, in .env.local set the CRAFT_GQL_URL and the  NEXT_PUBLIC_CRAFT_GQL_URL
   * See ".Env files" below for more details on how Next uses .env files
    
1. Run `npm install` from the project root

## Working

To view and work on the site locally:

`npm run dev`

Open [http://localhost:3000](http://localhost:3000) in your browser to see the site.

---

To run the production version of the site locally:

```
npm run build
npm run start
```

Open [http://localhost:3000](http://localhost:3000) in your browser to see the result.

## Notes

### Next.js

This is a [Next.js](https://nextjs.org/) project originally bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). [Next.js Documentation](https://nextjs.org/docs).

### .Env files

.env, .env.development, and .env.production hold default values and are committed to source control.

.env.local overrides those files, is not commited to source control, and should be the bearer of sensitive settings.

Also note that the environment vars on the production site on DO Apps are set through the DO Apps interface.

### Important Packages and Tech Foundations

- GraphQL for accessing data from the Craft CMS
- Chakra UI for basic UI components, theming, and a CSS styling system

### Multi-site

This app appropriates Next's internationalization feature in order to display a different meeting based on the domain used to access the site. This is currently the only method of achieving static page generation for a multi-domain app.

### Switching Sites Manually in a Local Environment

When working from the built in localhost server, it's not possible to experience the automatic meeting data switching based on domains. To view a different meeting, set the defaultLocale in the next.config.js to the key that corresponds to the appropriate domain. You will need to stop and restart the local dev server for this change to take effect.

## To Update Your Local App

1. Pull changes from remote repo
2. Run `npm install`

## Deploy

The site is auto-deployed to a Digital Ocean App on every push to the master branch of the CRF Github org remote repo.
