import {useEffect, useState} from "react";
import Head from 'next/head'
import {useRouter} from "next/router";

import {Box, Flex, Stack, Center, AspectRatio, Heading, Text, Link, Image, Circle, DarkMode} from "@chakra-ui/react";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";
import {useConferenceEvents} from "services/emma/emma";
import withSession from "../../src/lib/session";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import ChannelMenu from "components/portal/channelMenu";
import TrackList from "components/portal/trackList";
import Ad from "components/ads/ad"
import Login from "../login";

import Slider from "react-slick";
import ChannelCarousel from "components/portal/channelCarousel";
import {VideoReady, VideoNotReady, ComingUp, ComingUpNext, NowPlaying} from "components/portal/slide-overlays";
import { CustomPrevArrow, CustomNextArrow } from "components/carousel/arrows";
import {addSeconds, differenceInSeconds, isBefore, isAfter} from "date-fns";
import _ from "lodash";
import AdBlock from "components/ads/adBlock";
import LoadingIndicator from "components/ui/loadingIndicator";

export default function Channels({site, channels, user}) {

    // authorize -- TODO: move control of this to Craft
    if (!user) return <Login site={site}/>

    const router = useRouter()
    const { mock } = router.query

    const [carousels, setCarousels] = useState()
    const [mockTime, setMockTime] = useState(mock) // 2021-05-08T09:30:00

    const slickSettings = {
        infinite: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: true
    };

    const { status, data, error, isFetching } = useConferenceEvents(
        site.meeting.emmaMeetingId,
        {filter_status: 'All'},
        {
            prepEventListForChannel: true,
            removePrivateAndNotOnDemandSessions: true
        }
    );

    useEffect(() => {

        if (status !== "success") return false

        const carousels = channels.flatMap(channel => {
            const carousel = {
                title: channel.title
            }

            carousel.events = getChannelEvents(channel)

            // skip channels without ay events
            if (carousel.events.length === 0) return []

            // default overlays
            carousel.events.map(event => event.overlay = event.brightcove_id ? <VideoReady /> : <VideoNotReady />)

            // if this is not an on-demand only channel
            if (!carousel.events[0].params.on_demand) {

                // re-order events based on the current time: push past events to the end
                // to test: /channels?mock=2021-05-07T18:25:00

                // split the events by those with an end time that has not elapsed
                const futureAndPastEvents = _.partition(carousel.events, function(event) {
                    return isBefore(
                        getTheTime(),
                        new Date(event.datetime_end)
                    )
                })

                // set a default overlay for future events
                futureAndPastEvents[0].map(event => event.overlay = <ComingUp />)

                carousel.events = futureAndPastEvents[0].concat(futureAndPastEvents[1])

                // Add overlays to event slides based on the current time
                let firstEvent = carousel.events[0]
                let secondEvent = carousel.events[1]
                if (
                    isAfter(getTheTime(), new Date(firstEvent.datetime_start))
                    && isBefore(getTheTime(), new Date(firstEvent.datetime_end))
                ) {
                    firstEvent.overlay = <NowPlaying />
                    if (isBefore(getTheTime(), new Date(secondEvent.datetime_start))) {
                        secondEvent.overlay = <ComingUpNext />
                    }
                } else if (isBefore(getTheTime(), new Date(firstEvent.datetime_start))) {
                    firstEvent.overlay = <ComingUpNext />
                }

            }

            carousel.channel = channel

            return carousel.events.length ? carousel : []
        })

        setCarousels(carousels)

    }, [data])

    function getChannelEvents(channel) {
        if (channel.channelType === "sessionType") {
            return data.filter(event => {
                return event.session_type_id === parseInt(channel.sessionTypeID, 10)
            })
        } else {
            return data.filter(event => {
                return event.tracks.includes(channel.sessionTypeID)
            })
        }
    }

    function getTheTime() {
        if (mockTime) {
            return new Date(mockTime)
        } else {
            return new Date()
        }
    }

    return (
        <Layout site={site} bg="#ccc">
            <Head>
                <title>Live & On-Demand Channels : {site.meeting.title}</title>
            </Head>

            <Slider className="channels-top-slider" {...slickSettings}>
                {channels.map((channel, i) => {
                    return (<ChannelsSlideshowSlide {...channel} key={i}/>)
                })}
            </Slider>

            <ChannelMenu currentChannel={null} channels={channels} variant={site.theme.channelNavigation} context="channels"/>

            <Wrapper mb="100px">

                {site.meeting.leaderboardAdChannelsPage && (
                    <Flex justify="center" align="center" pt="20px" pb="35px" my="30px" direction="column">
                        <Text color="#333" fontSize={11} mb={1}>Advertisement</Text>
                        <AdBlock
                            adUnit={site.meeting.leaderboardAdChannelsPage}
                            sizes={[
                                [970,90],
                                [728,210],
                                [728,90],
                                [320,100],
                                [320,50]
                            ]}
                        />
                    </Flex>
                )}

                <Box px="10px">
                    {carousels ? (
                        carousels.map((carousel, i) => {
                            return (
                                <Box key={i} >
                                    <Heading as="h2" mt={10} mb={3} pl="5px" color="#333" fontSize="30px">{carousel.title}</Heading>
                                    <ChannelCarousel {...carousel} />
                                </Box>
                            )
                        })
                    ) : (
                        <LoadingIndicator />
                    )}
                </Box>
            </Wrapper>

        </Layout>
    )

}

function ChannelsSlideshowSlide(channel) {

    const mediaSource = channel.channelsPageSlideshowMedia.length ? channel.channelsPageSlideshowMedia[0].url : null

    const buttonStyle = {
        color: "#333",
        bg: "#fff",
        p: {base: "15px", lg: "25px"},
        width: {base: "270px", lg: "320px"},
        fontSize: 18,
        fontWeight: 600,
        textTransform: "uppercase",
        textAlign: "center",
        _hover: {textDecoration: "none"}
    }

    return (
        <Center pos="relative" bg="#111" overflow="hidden">
            {mediaSource ? (
                <video src={mediaSource} style={{minHeight: "390px", minWidth: "1203px", objectFit: "contain"}} autoPlay muted playsInline loop></video>
            ) : (
                <AspectRatio bg="#000" ratio={1600/480} w="100%" style={{minHeight: "390px", minWidth: "1203px", objectFit: "contain"}}><Box></Box></AspectRatio>
            )}
            <Flex
                position="absolute"
                w={"100%"}
                h={"100%"}
                top={0}
                left={0}
                align="center"
                justify="center"
            >
                <Flex
                    direction="column"
                    align="center"
                    maxWidth="800px"
                >
                    <Heading
                        as="h3"
                        color={channel.color1}
                        px="10px"
                        mt={0}
                        mb={2}
                        fontSize={{base: "35px", lg: "65px"}}
                        lineHeight={1}
                        textTransform="uppercase"
                        textAlign="center"
                        dangerouslySetInnerHTML={{ __html: channel.htmlTitle }}
                    />

                    <Box m="0 0 20px" px="50px" textAlign="center" dangerouslySetInnerHTML={{ __html: channel.channelSlideshowCopy }} />

                    <Stack justify="space-around" spacing={3} direction={{base: "column", lg: "row"}}>
                        {channel.currentlyAvailableVersions.includes('live') &&
                            <Link {...buttonStyle} href={"/live/" + channel.slug}>Watch Livestream</Link>
                        }
                        {channel.currentlyAvailableVersions.includes('ondemand') &&
                            <Link {...buttonStyle} href={"/ondemand/" + channel.slug} bg={channel.color1} color="#fff" >Watch On Demand</Link>
                        }
                    </Stack>
                </Flex>
            </Flex>
        </Center>
    )
}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            channels: data.site.channels,
            locale: locale,
            user: user || false
        }
    }

})
