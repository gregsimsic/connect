import React from "react";
import Head from "next/head";

import {getSiteIdByLocale} from "services/sites";
import { getSiteData } from "services/craft/craft";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import {Flex, Heading} from "@chakra-ui/react";

export default function Custom404(props) {

    return (
        <Layout site={props.site}>
            <Head>
                <title>404</title>
            </Head>

            <Wrapper pt="80px" >
                <Heading as="h1" fontSize="42px" fontWeight={600} lineHeight="1" >
                    Page Not Found
                </Heading>
            </Wrapper>

        </Layout>
    )

}

// set a default locale only to suppress an error that occurs during build & while initiating the dev app
export async function getStaticProps({ params, locale = 'tct2021' }) {

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: locale
        }
    }

}
