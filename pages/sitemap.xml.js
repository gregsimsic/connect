import { getSiteIdByLocale } from "services/sites";
import { getSiteData } from "services/craft/craft";

export default function Sitemap() {}

const dateString = new Date().toISOString();

const createSitemap = (menuItems, host) => `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        ${menuItems.map(({ slug, typeHandle, navigationPath }) => {
            
            // for path menu items, use the navigationPath field for the path instead of the slug
            const path = (typeHandle === 'path') ? navigationPath : slug
            
            return `
                        <url>
                            <loc>${`https://${host}/${path}`}</loc>
                            <lastmod>${dateString}</lastmod>
                        </url>
                    `;
        })
    .join('')}
    </urlset>
    `;

export const getServerSideProps = async function ({ req, res, locale }) {

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await getSiteData(craftSiteId)

    // filter out null and subhead menu items
    const menuItems = data.flatmenu.filter(menuItem => {
        return !['null','subhead'].includes(menuItem.typeHandle)
    })

    res.setHeader('Content-Type', 'text/xml');
    res.write(createSitemap(menuItems, req.headers.host));
    res.end();
    return { props: {} };

}