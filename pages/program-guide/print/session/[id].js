import {useState, useEffect} from "react"
import {Box, Flex, Heading, Text, LightMode} from "@chakra-ui/react";

import {getSiteIdByLocale, getEmmaMeetingIdByLocale} from "services/sites";
// import {getSiteData} from "services/craft/craft"

import withSession from "lib/session";
import Session from "components/agenda/session";

export default function Page({sessionData, type}) {

    const [session, setSession] = useState(null);

    useEffect(() => {
        setSession(sessionData)
    }, [])

    return (
            <Box color="#333" p={4}>
                {session && (
                    <>
                        <Heading as="h1">{session.title}</Heading>
                        <Session {...session} type={type} suppressLinks={true}/>
                    </>
                )}
            </Box>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale, query }) {

    const type = query.type || "live"

    const user = req.session.get("user");

    res.setHeader(
        'Cache-Control',
        'public, s-maxage=1, stale-while-revalidate=59'
    );

    // const craftSiteId = getSiteIdByLocale(locale)
    const emmaMeetingId = getEmmaMeetingIdByLocale(locale)

    const sessionId = params.id

    const data = await Promise.all([
        // TODO: best way to handle getting the meeting id ??
        // TODO: how to use react-query with SSR
        // TODO: replace this call
        fetch(process.env.EMMA_API_URL + '/conferences/' + emmaMeetingId + '/events/' + sessionId + '?fresh=true&depth=all' ).then(response => { return response.json() }),
        // getSiteData(craftSiteId)
    ]).then(result => {
        return {
            session: result[0].event,
            // site: result[1]
        };
    });

    // handle 404s
    if (!data.session) {
        return {
            notFound: true,
        }
    }

    return {
        props: { // will be passed to the page component as props
            sessionData: data.session,
            type: type
        }
    }
})
