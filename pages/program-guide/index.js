import Head from 'next/head'

import { getSiteIdByLocale } from "services/sites";
import { getSiteData } from "services/craft/craft";
import withSession from "../../src/lib/session";

import {Box, Flex, Heading, Text, useTheme} from "@chakra-ui/react";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import LoadingIndicator from "components/ui/loadingIndicator";
import SectionBanner from "components/header/sectionBanner";
import Breadcrumbs from "components/header/breadcrumbs";
import FilterableAgenda from "components/agenda/filterableAgenda";
import AdBlock from "components/ads/adBlock";

export default function ProgramGuide({site, channels}) {

    const theme = useTheme()

    return (
        <Layout site={site}>

            <Head>
                <title>Program Guide : {site.meeting.meetingName}</title>
            </Head>

            <SectionBanner
                label="Program Guide"
                flatmenu={site.flatmenu}
                theme={site.theme}
            />

            <Breadcrumbs flatmenu={site.flatmenu}/>

            <Flex bg="#fff" justify="center" align="center" pt="20px" pb="35px" direction="column">
                {site.meeting.leaderboardAdProgramGuide && (
                    <>
                        <Text color="#333" fontSize={11} mb={1}>Advertisement</Text>
                        <AdBlock
                            adUnit={site.meeting.leaderboardAdProgramGuide}
                            sizes={[
                                [728,210],
                                [728,90],
                                [320,100],
                                [320,50]
                            ]}
                        />
                    </>
                )}
            </Flex>

            <Wrapper bg="#fff" py="50px">

                <FilterableAgenda
                    meetingId={site.meeting.emmaMeetingId}
                    craftSiteId={site.meeting.siteId}
                    site={site}
                    channels={channels}
                    type="byDay"
                    showSearch={true}
                    showFilters={true}
                />

            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: locale,
            channels: data.site.channels,
            user: user || false
        }
    };

})