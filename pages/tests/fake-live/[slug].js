import React, {useEffect, useState} from "react";
import Head from 'next/head'

import _ from "lodash";
import {format, differenceInMinutes, isBefore, differenceInSeconds} from 'date-fns'

import {Box, Flex, Stack, Center, Heading, Text, useTheme} from "@chakra-ui/react";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";
import {useConferenceEvents} from "services/emma/emma";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import LiveTrackCarousel from "components/portal/liveTrackCarousel";
import ChannelMenu from "components/portal/channelMenu";
// import UpcomingEvents from "components/portal/upcomingEvents";
import LivePlayer from "components/portal/players/livePlayer";
import FakeLivePlayer from "components/portal/players/fakeLivePlayer";
import Chat from "components/portal/chat";
import Ad from "components/ads/ad"
import ModeratorList from "components/agenda-basic/moderatorList";
import PlayerEventInfo from "components/portal/playerEventInfo";
import TrackList from "components/portal/trackList";
import MockClock from "components/portal/mockClock";

export default function Live(props) {

    const {site, channels, slug} = props

    const theme = useTheme()
    const headerBackground = theme.colors.color2 + " url(" + theme.backgroundImage[0].url + ")"

    // TODO: potential to switch channels client-side
    // const [channelNowPLaying, setChannelNowPLaying] = useState(channels[0])

    function getChannelBySlug(slug) {
        return _.find(channels, channel => {
            return channel.theSlug === slug && channel.channelMode === 'ondemand'
        })
    }
    const channel = getChannelBySlug(slug)

    const { status, data, error, isFetching } = useConferenceEvents(
        site.meeting.emmaMeetingId,
        {filter_session_type_id: channel.sessionTypeID},
        true
    );

    const [eventNowPlaying, setEventNowPlaying] = useState(false)

    const [pastEvents, setPastEvents] = useState([])

    const [futureEvents, setFuturetEvents] = useState(data)

    const [currentTime, setCurrentTime] = useState(new Date())

    const [fakeTime, setFakeTime] = useState(new Date())

    const [seekTime, setSeekTime] = useState(0)

    // initiate the upcoming events list
    // && update page state when current time changes
    useEffect(() => {
        if (data?.length) {
            findCurrentEvent()
            updateEventInfo()
            updateEventsLists()
        }
    }, [data, currentTime]);

    // initiate clock
    useEffect(() => {
        // every 10 seconds update the page state
        const updateInterval = setInterval(() => {
            setCurrentTime(new Date())
        }, 10000)
        return () => clearInterval(updateInterval);
    }, [])

    // update info under the player
    const updateEventInfo = () => {

    }

    // update future and past events lists
    const updateEventsLists = () => {

        // find the first event whose end time has not elapsed
        const currentEventIndex = _.findIndex(data, event => {
            return isBefore(
                currentTime,
                new Date(event.endTime)
            )
        })

        if (currentEventIndex === -1) return false

        // update upcoming events list
        setFuturetEvents( _.slice(data, currentEventIndex))

        const _currentEvent = data[currentEventIndex]

        // play event & set seek time
        setEventNowPlaying(_currentEvent)

        const seekTime = differenceInSeconds(currentTime, new Date(_currentEvent.startTime))

        setSeekTime(seekTime)

    }

    // allow updates to the clock
    function updateTime(t) {
        setCurrentTime(t)
    }

    function findCurrentEvent() {

    }



    return (
        <Layout site={site}>
            <Head>
                <title>{site.title} Live</title>
            </Head>

            <Wrapper bg={headerBackground} h="150px" justify="center" as="header">
                <Heading as="h1">{channel.title} <Text as="span" color="color4">Live</Text></Heading>
                <MockClock fakeItLikeIts={fakeTime} setFakeTime={updateTime}/>
            </Wrapper>

            <ChannelMenu currentChannel={channel} channels={channels}/>

            <Wrapper>
                <Flex mt={12} direction={{base: 'column', md: 'row'}}>
                    <Box w={{base: '100%', md: '66%'}} pr={{base: 0, md: '30px'}} >
                        {/*<LivePlayer channelId={channel} />*/}
                        <FakeLivePlayer
                            events={data}
                            currentEvent={eventNowPlaying}
                            seek={seekTime}
                        />

                        <Heading as="h2">{channel.title}</Heading>

                    </Box>
                    <Box w={{base: '100%', md: '34%'}} >
                        <Chat />
                    </Box>
                </Flex>
            </Wrapper>

            <Flex justify="center" bg="gray.200" py="20px" my="30px">
                <Ad slot={{w: "720px", h: "90px", label: "728 x 90 Horizontal Banner Ad"}}/>
            </Flex>

            <Wrapper mb="30px">
                <Flex>
                    <Box w="75%">
                        <Heading as="h2" mb={6}>Coming Up</Heading>

                        <UpcomingEvents
                            events={futureEvents}
                        />

                        <Heading as="h2" mb={6}>Past Events</Heading>

                    </Box>

                    <Box w="25%">
                        <Ad slot={{w: "240px", h: "400px", label: "240 x 400 Vertical Banner Ad 1"}}/>
                        <Ad slot={{w: "240px", h: "400px", label: "240 x 400 Vertical Banner Ad 2"}}/>
                    </Box>

                </Flex>

            </Wrapper>

        </Layout>
    )

}

export async function getServerSideProps({ req, res, params, locale }) {

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            channels: data.site.channels,
            slug: params.slug
        }
    }

}
