import React, {useEffect, useState} from "react";
import Head from 'next/head'
import { useRouter } from 'next/router'

import {format} from 'date-fns'

import {Box, Flex, Heading, Text, useTheme} from "@chakra-ui/react";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import VideoPlayer from "components/portal/players/videoPlayer";

export default function BCVideo(props) {

    const {site, brightcove_id} = props

    const theme = useTheme()

    const router = useRouter()

    const headerBackground = theme.colors.color2 + " url(" + theme.backgroundImage[0].url + ")"

    const [seek, setSeek] = useState(0)

    useEffect (() => {
        const query = router.query

        if (router.query.seek) {
            setSeek(router.query.seek)
        }

    }, [])

    return (
        <Layout site={site}>
            <Head>
                <title>Brightcove Video: {brightcove_id}</title>
            </Head>

            <Wrapper bg={headerBackground} h="150px" justify="center" as="header">
                <Heading as="h1">Brightcove Video: {brightcove_id}</Heading>
            </Wrapper>

            <Wrapper>
                <Flex mt={12} direction={{base: 'column', md: 'row'}}>
                    <Box w={{base: '100%', md: '66%'}} pr={{base: 0, md: '30px'}} >
                        <VideoPlayer
                            videoId={brightcove_id}
                            seek={seek}
                        />

                    </Box>
                </Flex>
            </Wrapper>

        </Layout>
    )

}

export async function getServerSideProps({ req, res, params, locale }) {

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            brightcove_id: params.id
        }
    }

}
