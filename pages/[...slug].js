import Head from 'next/head'
import nookies from 'nookies'

import {getSiteIdByLocale} from "services/sites";
import {getPage, getSiteData} from "services/craft/craft";

import {Box, Container, Flex, Heading, Text, Center} from "@chakra-ui/react"

import Layout from 'components/layout/layout'
import Wrapper from 'components/layout/wrapper'
import Body from 'components/matrix/body'
import AccordionComponent from 'components/matrix/accordion'
import AgendaSession from 'components/matrix/agendaSession'
import AdvancedAgenda from 'components/matrix/advancedAgenda'
import Ad from 'components/matrix/ad'
import withSession from "../src/lib/session";

import Breadcrumbs from "components/header/breadcrumbs";
import SectionBanner from "components/header/sectionBanner";

export default function Page(page) {

    const hasAd = page.data.leaderboardAd.length && page.data.leaderboardAd[0].adUnit

    const colorMode = page.data.colorMode || 'dark'
    const styles = {
        'light': {
            bgColor: '#fff',
            pageTitleColor: 'color2'
        },
        'dark': {
            bgColor: 'transparent',
            pageTitleColor: 'color1'
        }
    }

    let pageBackground = styles[colorMode].bgColor
    const bgStyles = {}
    if (page.data.pageBackgroundMedia && page.data.pageBackgroundMedia[0]?.url) {
        pageBackground += " url(" + page.data.pageBackgroundMedia[0]?.url + ")"
        bgStyles.backgroundSize = "cover"
        bgStyles.backgroundPosition = "center center"
    }

    return (
        <Layout site={page.site} bg={pageBackground} bgStyles={bgStyles}>

            <Head>
                <title>{page.data.title + ' | ' + page.site.meeting.seoMetaTitle}</title>
                <meta name="description" value={page.site.meeting.seoMetaDescription}/>
            </Head>

            {page.data.showPageHeader ? (
                <>
                    <SectionBanner flatmenu={page.site.flatmenu} theme={page.site.theme} />
                    <Breadcrumbs flatmenu={page.site.flatmenu}/>
                </>
            ) : ( null )}

            {hasAd ? (
                <Ad block={page.data.leaderboardAd[0]} />
            ) : null }

            <Flex as="main" direction="column" align="center" mb="60px" bg={styles[colorMode].bgColor}>

                {page.data.showPageHeader ? (
                    <Wrapper pt="80px" >
                        <Heading as="h1" color={styles[colorMode].pageTitleColor} fontWeight="600" lineHeight="1" >{page.data.title}</Heading>
                    </Wrapper>
                ) : ( null )}

                {page.data.matrix.map((block, i) => {
                    switch (block.type) {
                        case 'body': {
                            return (<Body key={i} block={block} colorMode={colorMode}/>)
                            break
                        }
                        case 'accordion': {
                            return (<AccordionComponent key={i} block={block}/>)
                            break
                        }
                        case 'agendaSession': {
                            return (<AgendaSession
                                key={i}
                                block={block}
                                meetingId={page.site.meeting.emmaMeetingId}
                                channels={page.channels}
                            />)
                            break
                        }
                        case 'advancedAgenda': {
                            return (
                                <AdvancedAgenda
                                    key={i}
                                    block={block}
                                    site={page.site}
                                    channels={page.channels}
                                />
                                )
                            break
                        }
                        case 'ad': {
                            return ( <Ad key={i} block={block} /> )
                            break
                        }
                    }
                })}

            </Flex>

        </Layout>
    )
}

export const getServerSideProps = withSession(async function (ctx) {

    const user = ctx.req.session.get("user");

    ctx.res.setHeader(
        'Cache-Control',
        'public, s-maxage=1, stale-while-revalidate=59'
    );

    const craftSiteId = getSiteIdByLocale(ctx.locale)

    // enable previewing drafts and Craft's live preview feature
    const previewToken = (ctx.query['x-craft-preview'] || ctx.query['x-craft-live-preview']) ? ctx.query.token : null
    
    const data = await Promise.all([
        getPage(craftSiteId, ctx.params.slug, ctx.query.draftId, previewToken),
        getSiteData(craftSiteId, true)
    ]).then( result => {
        return {
            page: result[0].pages[0],
            site: result[1]
        };
    });

    // page/slug not found, return 404
    if (!data.page) {
        return {
            notFound: true,
        }
    }

    // block guarded pages
    if (!user && data.page?.userStatus === 'loggedInOnly') {

        // save current path to cookie
        nookies.set(ctx, 'loginRedirect', ctx.resolvedUrl, {
            httpOnly: false,
            path: '/'
        })

        return {
            redirect: {
                destination: '/login',
                permanent: false,
            }
        }
    }

    return {
        props: { // will be passed to the page component as props
            data: data.page,
            channels: data.site.channels,
            site: data.site,
            locale: ctx.locale,
            user: user || false
        }
    }
})