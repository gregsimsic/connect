import {useState, useEffect} from 'react'
import _ from "lodash";
import {format} from "date-fns";
import {findEventById} from "../../src/util";

import Head from 'next/head'
import {Box, Flex, Heading, Text, Image, useBreakpointValue, Link, Icon} from "@chakra-ui/react";
import {Twitter} from "react-feather";

import {getSiteIdByLocale, getEmmaMeetingIdByLocale} from "services/sites";
import {getSiteData, getSiteDataWithChannels} from "services/craft/craft"
import withSession from "../../src/lib/session";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper"
import SectionBanner from "components/header/sectionBanner";
import {isSameDay} from "date-fns";
import {useConferenceEvents} from "services/emma/emma";
import LoadingIndicator from "components/ui/loadingIndicator";
import RenderLink from "components/agenda/renderLink";

export default function FacultyProfile({profile, site}) {

    const isMobile = useBreakpointValue({ base: true, md: false })

    const headerBackground = site.theme.colorPalette[0].color2 + " url(" + site.theme.backgroundImage[0].url + ")"

    const [structuredEvents, setStructuresEvents] = useState([])

    // TODO: include Private events ??
    const { status, data, error, isFetching } = useConferenceEvents(
        site.meeting.emmaMeetingId
    )

    useEffect(() => {

        if (!data) return false

        // exchange the event for the parent if the assignment is a presentation
        let sections = profile.assignments.flatMap(assignment => {

            // filter out Private events
            if (assignment.event.status === "Private") return []

            if (assignment.event.depth === 1) return assignment.event

            return findEventById(assignment.event.parent_id, data) || []
        })

        // remove duplicates
        sections = _.uniqBy(sections, 'id')

        // group by parent session
        const eventsGroupedBySession = _.groupBy(sections, event => event.parent_id)

        // set up sessions
        const sessions = _.map(eventsGroupedBySession, (events, sessionId) => {
            const session = findEventById(parseInt(sessionId, 10), data)
            return {
                sessionId: sessionId,
                title: session.title,
                day: format(new Date(events[0].datetime_start), 'EEEE, MMMM d'),
                events: events
            }
        })

        // then group sessions by day
        const sessionsGroupedByDay = _.groupBy(sessions, session => session.day)

        // set up days
        const days = _.map(sessionsGroupedByDay, (sessionGroup, day) => {
            return {
                day: day,
                sessionGroup: sessionGroup
            }
        })

        setStructuresEvents(days)

    }, [data])

    const ConditionalLink = function({event, children}) {
        
        if ( !site?.meeting.isLive || event.status === "Private") return children

        const channelSlug = getChannelSlug(event.session_type_id)

        return <RenderLink event={event} channelSlug={channelSlug} site={site}>{children}</RenderLink>

    }

    function getChannelSlug(sessionTypeId) {
        const channel = _.find(site.channels, channel => {
            return parseInt(channel.sessionTypeID, 10) === sessionTypeId
        })

        return channel ? channel.slug : false
    }

    return (
        <Layout site={site}>

            <Head>
                <title>{site.meeting.meetingName} : {profile.full_name} (faculty)</title>
            </Head>

            <SectionBanner
                label="Faculty"
                flatmenu={site.flatmenu}
                theme={site.theme}
            />

            <Wrapper>
                <Flex
                    m="50px 0 20px"
                    direction={isMobile ? 'column' : 'row'}
                >
                    <Box mr={5} mb={5}>
                        <Image
                            src={profile.image_url}
                            fallbackSrc="/person.png"
                            alt={profile.full_name}
                            maxWidth={200}
                        />
                    </Box>
                    <Box mb={12}>

                        <Heading as="h1" m={0} >{profile.full_name + ", " + profile.degree}</Heading>
                        {profile.social_twitter ? (
                            <Box m={0}>
                                <Link href={"https://twitter.com/" + profile.social_twitter} isExternal color="color4" fontSize="14px" _hover={{textDecoration: "none"}}>
                                    <Box display="inline" pr={1} ><Icon as={Twitter} w="18px" h="18px" fill="color4" verticalAlign="middle"/></Box>
                                    @{profile.social_twitter}
                                </Link>
                            </Box>
                        ) : ( null )}

                        <Box mt={6} className="body" dangerouslySetInnerHTML={{__html: profile.description}} />

                        {status === 'success' ? (
                            structuredEvents.length ? (
                                <>
                                    <Heading as="h2" fontSize="24px" mt={8}>Sessions</Heading>
                                    {structuredEvents.map((day, i) => {
                                        return (
                                            <Box key={i}>
                                                <Text fontSize="14px" color="color1" mt={6}>{day.day}</Text>
                                                {day.sessionGroup.map((sessionGroup, j) => {
                                                    return (
                                                        <Box key={j}>
                                                            <Text fontSize="17px" mt={j ? 6 : 1} fontWeight={600}>{sessionGroup.title}</Text>
                                                            {sessionGroup.events.map((event, k) => {
                                                                return (
                                                                    <Box key={k} mt={2}>
                                                                        <ConditionalLink event={event}>
                                                                            <Text as="span" fontSize="15px" >{event.title}</Text>
                                                                        </ConditionalLink>
                                                                        {/*<Text mt={0} color="color12" fontSize="12px">moderator_role</Text>*/}
                                                                    </Box>
                                                                )
                                                            })}
                                                        </Box>
                                                    )
                                                })}
                                            </Box>
                                        )
                                    })}
                                </>
                            ) : (
                                <Text fontStyle="italic"></Text>
                            )
                        ) : (
                            <LoadingIndicator color="#fff"/>
                        )}

                    </Box>
                </Flex>
            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    res.setHeader(
        'Cache-Control',
        'public, s-maxage=1, stale-while-revalidate=59'
    );

    const craftSiteId = getSiteIdByLocale(locale)
    const emmaMeetingId = getEmmaMeetingIdByLocale(locale)

    // extract the faculty id from the slug (ex: ziad-ali-4119)
    const facultyId = /[^-]*$/.exec(params.slug)[0];

    const data = await Promise.all([
        // TODO: best way to handle getting the meeting id ??
        // TODO: how to use react-query with SSR
        // TODO: replace this call
        fetch(process.env.EMMA_API_URL + '/conferences/' + emmaMeetingId + '/faculty/' + facultyId ).then(response => { return response.json() }),
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            profile: result[0].faculty,
            site: result[1]
        };
    });

    // handle 404s
    if (!data.profile) {
        return {
            notFound: true,
        }
    }

    return {
        props: { // will be passed to the page component as props
            profile: data.profile,
            site: data.site,
            locale: locale,
            user: user || false
        }
    }
})
