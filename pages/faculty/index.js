import Head from 'next/head'

import {Box, Heading, Text, CircularProgress, useTheme} from "@chakra-ui/react";

import { getSiteIdByLocale } from "services/sites";
import { getSiteData} from "services/craft/craft";
import {useConferenceFaculty, useConferenceModeratorRoles} from "services/emma/emma";
import withSession from "../../src/lib/session";
import {useGlobalState} from "../../src/providers/globalState";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import CourseDirectors from "components/faculty/courseDirectors"
import FilterableFaculty from "components/faculty/filterableFaculty";
import LoadingIndicator from "components/ui/loadingIndicator";

export default function Faculty(props) {

    const {site} = useGlobalState()
    const theme = useTheme()

    const { status, data, error, isFetching } = useConferenceFaculty(props.site.meeting.emmaMeetingId);
    const { status: moderatorRolesStatus, data: moderatorRolesData } = useConferenceModeratorRoles(props.site.meeting.emmaMeetingId);

    return (
        <Layout site={props.site}>

            <Head>
                <title>{props.site.meeting.meetingName} : Course Directors & Faculty</title>
            </Head>

            <Wrapper py="50px" >

                {site.meeting.enableFacultyProfileLinks && (
                    <Text m="0 0 30px">Note: To view disclosures and sessions, click on photo.</Text>
                )}

                <Heading as="h2" color="color1" fontWeight="600" lineHeight="1" m="0 0 25px">Course Directors</Heading>

                <CourseDirectors moderatorRoles={moderatorRolesData} meetingId={props.site.meeting.emmaMeetingId}/>

                <Heading as="h1" fontWeight="600" lineHeight="1" m="50px 0 0">Faculty</Heading>

                <Box mt={5}>
                    {data && data.length && moderatorRolesData?.length
                        ? <FilterableFaculty items={data} meetingId={props.site.meeting.emmaMeetingId} moderatorRoles={moderatorRolesData} />
                        : <LoadingIndicator color="#fff"/>
                    }
                </Box>

            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get agenda & site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: locale,
            user: user || false
        }
    };

})