import withSession from "../../../src/lib/session";

import okta from "@okta/okta-sdk-nodejs"

const oktaClient = new okta.Client({
    orgUrl: 'https://tctmd.okta.com/',
    token: '00FLMOGqh9Rq4pPrkhw-OQoQlDzclHXFOG9-HZkApT' // Obtained from Developer Dashboard
});

export default withSession(async (req, res) => {

    const user = req.session.get("user");

    if (user) {
        await oktaClient.endSession(user.oktaSessionToken)
            .then((response) => {

            })
            .catch((response) => {

            });

        // or could do this since we know the user id
        // https://github.com/okta/okta-sdk-nodejs
        // To end all sessions for a user, you must know the ID of the user:
        // client.clearUserSessions(user.id)
        //     .then(() => {
        //         console.log('All user sessions have ended');
        //     });

        // destroy local cookie
        req.session.destroy();
        res.json({isLoggedIn: false});
    } else {
        res.json('no local user session found')
    }

});