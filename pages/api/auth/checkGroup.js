import withSession from "../../../src/lib/session";

import axios from "axios";
import sites from 'config/sites'

export default withSession(async (req, res) => {

    const { oktaId, meetingKey } = await req.body

    let isInGroup = false

    const url = "https://tctmd.okta.com/api/v1/users/" + oktaId + "/groups"

    const token = process.env.OKTA_API_TOKEN

    const config = {
        headers: { Authorization: `SSWS ${token}` },
        withCredentials: true
    };

    await axios.get(url, config)
        .then((response) => {

            const meetingOktaGroupProfileNames = sites[meetingKey].oktaGroups
            meetingOktaGroupProfileNames.push('CRF_STAFF')

            // look for this meeting in the user's assigned groups & also check for the CRF staff group
            isInGroup = response.data.some(group => {
                return meetingOktaGroupProfileNames.includes(group.profile.name)
            })

        })
        .catch ((error) => {
            console.log('users/groups error', error)
        })

    if (!isInGroup) {
        res.status(403).json({message: 'The user is not in the group for this meeting.'});
    }

    res.json({isInGroup: isInGroup})

});