import withSession from "../../../src/lib/session";

import {OktaAuth} from "@okta/okta-auth-js";

const oktaAuthClient = new OktaAuth({
    issuer: 'https://profile.crf.org/oauth2/default'
});

import okta from "@okta/okta-sdk-nodejs"

const oktaClient = new okta.Client({
    orgUrl: 'https://tctmd.okta.com/',
    token: '00FLMOGqh9Rq4pPrkhw-OQoQlDzclHXFOG9-HZkApT' // Obtained from Developer Dashboard
});

export default withSession(async (req, res) => {

    const { username, password } = await req.body

    let user = false

    await oktaAuthClient.signIn({
        username: username,
        password: password
    })
        .then(response => {

            console.log(response.sessionToken)

            user = {
                isLoggedIn: true,
                oktaId: response.user.id,
                oktaSessionToken: response.sessionToken,
                expiresAt: response.expiresAt,
                name: response.user.profile.firstName + ' ' + response.user.profile.lastName,
                email: response.user.profile.login,
                timeZone: response.user.profile.timeZone
            }

        })
        .catch(error => {
            console.log('Okta signin error', error)
            res.status(400).json(error.data);
        });

    if (user) {
        req.session.set("user", user);
        await req.session.save();

        // create Okta session by visiting URL

        console.log('before okta')

        const appRedirectUrl = "http://fellows.crfconnect.lcl:3000/faculty"
        const oktaRedirectUrl = 'https://tctmd.okta.com/login/sessionCookieRedirect?token=' + user.oktaSessionToken + '&redirectUrl=' + appRedirectUrl

        // console.log('res', res)

        // redirect to Okta, then Okta will redirect back to the local appRedirectUrl
        // need to set headers on this request ??
        // res.redirect(307, oktaRedirectUrl)

        // or, manually
        // res.writeHead(302, {
        //     'Location': 'your/404/path.html'
        //     //add other headers here...
        // });
        // res.end();



        // res.setHeader('Location', oktaRedirectUrl);
        // res.end();

        console.log("user.oktaSessionToken", user.oktaSessionToken)

        // TODO: Does not seem to create the cookie necessary for sessions/me to pick up
        // await oktaClient.createSession({sessionToken: user.oktaSessionToken})
        //     .then((session) => {
        //         console.log('Session created', session);
        //     });

        console.log('after okta')
    }

    res.json(user);

});