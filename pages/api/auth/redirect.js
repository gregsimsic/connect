import withSession from "../../../src/lib/session";

import {OktaAuth} from "@okta/okta-auth-js";

export default withSession(async (req, res) => {

    res.redirect(307, "/faculty")

});