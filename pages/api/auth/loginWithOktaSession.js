import withSession from "../../../src/lib/session";

export default withSession(async (req, res) => {

    const { user } = await req.body

    if (user) {
        req.session.set("user", user);
        await req.session.save();

        res.json(user)
        res.end()
    } else {
        res.status(400).json({error: 'no user data supplied'});
    }

});