import withSession from "../../../src/lib/session";

import okta from "@okta/okta-sdk-nodejs"

const oktaClient = new okta.Client({
    orgUrl: 'https://tctmd.okta.com/',
    token: '00FLMOGqh9Rq4pPrkhw-OQoQlDzclHXFOG9-HZkApT' // Obtained from Developer Dashboard
});

export default withSession(async (req, res) => {

    const user = req.session.get("user");

    // console.log('user', user)

    if (user) {
        // TODO: sessionToken is not correct, need the session ID
        await oktaClient.getSession(user.oktaSessionToken)
            .then((session) => {
                console.log('Session details', session);
                res.json({ session: session });
            });

        // TODO: handle 404 if session not found

    } else {
        res.json({ data: 'no user found' });
    }
});