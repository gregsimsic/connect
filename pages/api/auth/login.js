import sites from 'config/sites'

import withSession from "../../../src/lib/session";

import {OktaAuth} from "@okta/okta-auth-js";
import okta from "@okta/okta-sdk-nodejs"
import axios from "axios";

const oktaAuthClient = new OktaAuth({
    issuer: 'https://profile.crf.org/oauth2/default'
});

const oktaSdkClient = new okta.Client({
    orgUrl: 'https://tctmd.okta.com/',
    token: '00FLMOGqh9Rq4pPrkhw-OQoQlDzclHXFOG9-HZkApT' // Obtained from Developer Dashboard
});

export default withSession(async (req, res) => {

    const { username, password, meetingKey } = await req.body

    // const nextConfigModule = require('./next.config,js');

    let user = false
    let isInGroup = false

    // sign in to Okta with TCTMD credentials
    await oktaAuthClient.signIn({
        username: username,
        password: password
    })
    .then(response => {

        // console.log('login session', response)

        user = {
            isLoggedIn: true,
            oktaId: response.user.id,
            oktaSessionToken: response.sessionToken, // maybe this isn't the right value
            expiresAt: response.expiresAt,
            name: response.user.profile.firstName + ' ' + response.user.profile.lastName,
            email: response.user.profile.login
        }

    })
    .catch(error => {
        console.log('Okta signin error', error)
        res.status(401).json({message: 'Okta signin error'});
    });

    if (user) {

        const url = "https://tctmd.okta.com/api/v1/users/" + user.oktaId + "/groups"

        const token = process.env.OKTA_API_TOKEN

        const config = {
            headers: { Authorization: `SSWS ${token}` },
            withCredentials: true
        };

        await axios.get(url, config)
            .then((response) => {

                const groupProfileNames = sites[meetingKey].oktaGroups
                groupProfileNames.push('CRF_STAFF')
                
                // look for this meeting in the user's assigned groups & also check the CRF staff group
                isInGroup = response.data.some(group => {
                    return groupProfileNames.includes(group.profile.name)
                })

            })
            .catch ((error) => {
                console.log('users/groups error', error)
            })

        if (!isInGroup) {
            res.status(403).json({message: 'You are not registered for this meeting.'});
        }

        // Set a flag so that we can swap the session token on the client (necessary for proper Okta logout)
        user.sessionTokenNeedsReplacement = true

        req.session.set("user", user);
        await req.session.save();

        res.json(user)
    }

});