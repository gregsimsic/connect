import React from "react";
import Head from "next/head";
import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper"

import withSession from "../../src/lib/session";
import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";
import {Flex, Heading, Text} from "@chakra-ui/react";

import okta from "@okta/okta-sdk-nodejs"

const oktaClient = new okta.Client({
    orgUrl: 'https://tctmd.okta.com/',
    token: '00FLMOGqh9Rq4pPrkhw-OQoQlDzclHXFOG9-HZkApT'    // Obtained from Developer Dashboard
});

const SsrProfile = ({ site, user }) => {
    return (
        <Layout site={site} user={user}>

            <Head>
                <title>User Profile</title>
                <meta name="description" value="Login Page"/>
            </Head>

            <Wrapper pt="80px" >
                {user && (
                    <>
                        <Heading as="h2">{user.name}</Heading>
                        <Text>{user.email}</Text>
                        <Text>{user.oktaProfile.profile.title}</Text>
                        <Text>Subscription: {user.oktaProfile.profile.subscription_name}</Text>
                        {/*<Text>Timezone: {user.timeZone}</Text>*/}
                    </>
                )}
            </Wrapper>

        </Layout>
    );
};

export const getServerSideProps = withSession(async function ({ req, res, locale }) {

    const user = req.session.get("user");

    if (!user) {
        return {
            redirect: {
                permanent: false,
                destination: "/login"
            }
        }
    }

    // get user profile from Okta and add it to the user session data
    await oktaClient.getUser(user.oktaId)
        .then(oktaUser => {
            // console.log(oktaUser);
            // this hacky conversion is necessary because Okta returns an object and Next needs JSON
            user.oktaProfile = JSON.parse(JSON.stringify(oktaUser))
        });

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: locale,
            user: user || false
        }
    }

});

export default SsrProfile;

// function githubUrl(login) {
//     return `https://api.github.com/users/${login}`;
// }

// SsrProfile.propTypes = {
//     user: PropTypes.shape({
//         isLoggedIn: PropTypes.bool,
//         login: PropTypes.string,
//         avatarUrl: PropTypes.string,
//     }),
// };