import React, {useEffect} from 'react'
import { Router } from 'next/router'
import App from 'next/app'
import TagManager from 'react-gtm-module'

import {ChakraProvider} from '@chakra-ui/react'
import makeTheme from 'styles/theme'

import {QueryClient, QueryClientProvider} from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

import { AuthProvider } from '../src/providers/auth'
import OktaSession from '../src/services/oktaSession'

import { GlobalStateProvider } from '../src/providers/globalState'

import { DFPSlotsProvider } from 'react-dfp';

const queryClient = new QueryClient();

// TODO: can import a global stylesheet here
// https://nextjs.org/docs/basic-features/built-in-css-support#adding-a-global-stylesheet
import "styles/globals.css"
// import CookieBot from "react-cookiebot";

function MyApp({Component, pageProps}) {

    // TODO: these defaults are set here to suppress a build error on pages that do not receive the theme: need a better solution
    const themeDefault = pageProps.site?.theme || {
        colorPalette: [
            {
                color1: "#333",
                color2: "#333",
                color3: "#333",
                color4: "#333",
                color5: "#333",
                color6: "#333",
                color7: "#333",
                color8: "#333",
                color9: "#333",
                color10: "#333",
                color11: "#333",
                color12: "#333",
                color13: "#333",
                color14: "#333",
                color15: "#333",
                color16: "#333",
            }
        ]
    }

    const customTheme = makeTheme(themeDefault, pageProps.locale);

    // Google Tag Manager
    useEffect(() => {
        TagManager.initialize({ gtmId: 'GTM-PWDG2H7' });
    }, []);

    return (
        <ChakraProvider theme={customTheme} resetCSS={true}>
            <AuthProvider user={pageProps.user}>
                <GlobalStateProvider locale={pageProps.locale} site={pageProps.site}>
                    <DFPSlotsProvider
                        dfpNetworkId={pageProps.site?.meeting.googleAdsNetworkId}
                        lazyLoad={true}
                        sizeMapping={
                            [
                                { viewport: [728, 768], sizes: [[970, 90], [728, 210], [728, 90], [240, 400]] },
                                { viewport: [320, 480], sizes: [[350, 44], [320, 100], [320, 50], [240, 400]] },
                                { viewport: [0, 0], sizes: [] },
                            ]
                        }
                    >
                        <QueryClientProvider client={queryClient}>
                            <Component {...pageProps} />
                            <ReactQueryDevtools initialIsOpen={false} />
                            <OktaSession meetingKey={pageProps.locale}/>
                            {/*<CookieBot domainGroupId="1e754b0c-1441-4a12-80b6-812fe35af6e0" />*/}
                        </QueryClientProvider>
                    </DFPSlotsProvider>
                </GlobalStateProvider>
            </AuthProvider>
        </ChakraProvider>
    )
}

export default MyApp
