import Document, { Html, Head, Main, NextScript } from 'next/document'
import { ColorModeScript, useTheme } from "@chakra-ui/react"
import React from "react";

class MyDocument extends Document {

    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html lang="en">
                <Head>
                    <script id="CookieBot" src="https://consent.cookiebot.com/uc.js" data-cbid="1e754b0c-1441-4a12-80b6-812fe35af6e0" data-blockingmode="auto" type="text/javascript"></script>
                    <meta name="env" content={process.env.NODE_ENV} />
                    <link rel="stylesheet" href="https://use.typekit.net/qbd1ysq.css" />
                    <link rel="stylesheet" href="https://use.typekit.net/sle8jlq.css" />
                    {/*<link rel="stylesheet" href="https://www.crf.org/templates/crf/fonts/285831/5444CB789140D89CE.css" />*/}
                </Head>
                <body>
                    <ColorModeScript initialColorMode="light"/>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument