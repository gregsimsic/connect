import Head from 'next/head'
import dynamic from 'next/dynamic'

import {Box, Flex} from "@chakra-ui/react";

import Layout from "components/layout/layout";
import Body from "components/matrix/body";
import Accordion from "components/matrix/accordion";
import CourseDirectorsMatrix from "components/matrix/courseDirectorsMatrix";

import {getSiteIdByLocale} from "services/sites";
import {getSingle, getSiteData} from "services/craft/craft";
import withSession from "../src/lib/session";

// custom matrix block components
// must list all possible components as dynamic imports
// ref: https://github.com/vercel/next.js/discussions/11291
const componentList = {
    Tht2022HomeTop: dynamic(() => import('components/custom/tht2022/Tht2022HomeTop')),
    Tht2022HomeMain: dynamic(() => import('components/custom/tht2022/Tht2022HomeMain')),
    Tht2022HomeSocial: dynamic(() => import('components/custom/tht2022/Tht2022HomeSocial')),
    Comp2021HomeTop: dynamic(() => import('components/custom/comp2021/Comp2021HomeTop')),
    Comp2021HomeMain: dynamic(() => import('components/custom/comp2021/Comp2021HomeMain')),
    Comp2021HomeSocial: dynamic(() => import('components/custom/comp2021/Comp2021HomeSocial')),
    Tct2021HomeTop: dynamic(() => import('components/custom/tct2021/Tct2021HomeTop')),
    Tct2021HomeMain: dynamic(() => import('components/custom/tct2021/Tct2021HomeMain')),
    Tct2021HomeSocial: dynamic(() => import('components/custom/tct2021/Tct2021HomeSocial')),
    Tvt2021HomeTop: dynamic(() => import('components/custom/tvt2021/Tvt2021HomeTop')),
    Tvt2021HomeMain: dynamic(() => import('components/custom/tvt2021/Tvt2021HomeMain')),
    Tvt2021HomeSocial: dynamic(() => import('components/custom/tvt2021/Tvt2021HomeSocial')),
    Tvt2021Support: dynamic(() => import('components/custom/tvt2021/Tvt2021Support')),
    FellowsConnectHomeMain: dynamic(() => import('components/custom/fellows2021/FellowsConnectHomeMain')),
    FellowsConnectHomeTop: dynamic(() => import('components/custom/fellows2021/FellowsConnectHomeTop')),
    Tct2020SocialMedia: dynamic(() => import('components/custom/tct2020/Tct2020SocialMedia')),
    Tct2020Supporters: dynamic(() => import('components/custom/tct2020/Tct2020Supporters')),
    CtoConnect2021HomeTopVersionA: dynamic(() => import('components/custom/cto2021/CtoConnect2021HomeTopVersionA')),
    CtoConnect2021SocialMedia: dynamic(() => import('components/custom/cto2021/CtoConnect2021SocialMedia'))
}

export default function Home(props) {

    return (
      <Layout site={props.site} className="home">
          <Head>
              <title>{props.site.meeting.meetingName}</title>
          </Head>

          <Flex as="main" direction="column" >

              {props.matrix.map((block, i) => {

                  switch (block.type) {
                      case 'body': {
                          return <Body key={i} block={block} padTop="50px" padBottom="50px" margin="0 auto"/>
                          break
                      }
                      case 'accordion': {
                          return <Accordion key={i} block={block}/>
                          break
                      }
                      case 'courseDirectors': {
                          return <CourseDirectorsMatrix key={i} block={block} meetingId={props.site.meeting.emmaMeetingId}/>
                          break
                      }
                      // TODO: pass props to the custom component -- what data to access/call ?? -- make API call from within the component
                      case 'customComponent': {
                          if (componentList.hasOwnProperty(block.component)) {
                              const Component = componentList[block.component]
                              return <Component key={i} block={block} user={props.user} site={props.site} />
                          } else {
                              console.log('Custom component not found: ' + block.component)
                              return null
                          }
                          break
                      }
                  }
              })}

          </Flex>
      </Layout>
  )
}

export const getServerSideProps = withSession(async function ({ req, res, params, locale, query }) {

    const user = req.session.get("user");

    // these headers invoke SWR to provide a cache-like experience
    res.setHeader(
        'Cache-Control',
        'public, s-maxage=1, stale-while-revalidate=59'
    );

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSingle('homePage', craftSiteId, query.draftId),
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            page: result[0].pages[0],
            site: result[1]
        };
    });

    return {
        props: { // will be passed to the page component as props
            title: data.page.title,
            matrix: data.page.matrix,
            site: data.site,
            locale: locale,
            user: user || false
        }
    }
})