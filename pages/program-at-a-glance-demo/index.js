import React, {useEffect, useState} from "react";
import Head from 'next/head'
import _ from "lodash"

import { getSiteIdByLocale } from "services/sites";
import { getSiteData } from "services/craft/craft";

import {Box, Heading, CircularProgress, useTheme, Flex, Spinner} from "@chakra-ui/react";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import AtAGlance from "components/ataglance/atAGlance";

import {useConferenceEvents} from "services/emma/emma";
import LoadingIndicator from "components/ui/loadingIndicator";
import withSession from "../../src/lib/session";

export default function ProgramAtAGlance(props) {

    const theme = useTheme()

    const { status, data, error, isFetching } = useConferenceEvents(
        props.site.meeting.emmaMeetingId,
        {},
        {
            withVideoOnly: false
        }
    );

    return (
        <Layout site={props.site}>

            <Head>
                <title>Agenda</title>
            </Head>

            <Wrapper bg={theme.colors.color2} py="50px" >

                <Heading as="h1">Program At-A-Glance</Heading>

                <Box mt={50}>
                    {status === "loading" ? (
                        <LoadingIndicator />
                    ) : status === "error" ? (
                        <span>Error: {error.message}</span>
                    ) : (
                        <AtAGlance agenda={data}/>
                    )}
                </Box>

            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get agenda & site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: locale,
            user: user || false
        }
    };

})