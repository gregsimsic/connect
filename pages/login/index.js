import Head from 'next/head'
import withSession from "../../src/lib/session";
import nookies from 'nookies'

import {Box, Center} from '@chakra-ui/react'

import Layout from "components/layout/layout";
import LoginForm from "components/nav/loginForm";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";

export default function Login(props) {

    const { site } = props

    return (
        <Layout site={site}>

            <Head>
                <title>Login | {site.meeting.seoMetaTitle}</title>
                <meta name="description" value="Login Page"/>
            </Head>
            
            <Center h="100%" mt={{base: 0, md: "70px"}}>
                <LoginForm />
            </Center>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function (ctx) {

    const user = ctx.req.session.get("user");

    const cookies = nookies.get(ctx)

    if (user) {
        return {
            redirect: {
                permanent: false,
                destination: cookies.loginRedirect || "/channels"
            }
        }
    }

    const craftSiteId = getSiteIdByLocale(ctx.locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: ctx.locale,
            user: user || false
        }
    }

})
