import Head from 'next/head'

import _ from "lodash";
import {format} from 'date-fns'

import {Box, Flex, Stack, Center, Heading, Text, useTheme} from "@chakra-ui/react";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";
import {useConferenceEvents} from "services/emma/emma";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";

import PlayerEventInfo from "components/portal/playerEventInfo";
import withSession from "../../../src/lib/session";

export default function Event(props) {

    const {site, channels, slug} = props

    const theme = useTheme()

    const headerBackground = theme.colors.color2 + " url(" + theme.backgroundImage[0].url + ")"

    // get event from Emma
    const { status, data, error, isFetching } = useConferenceEvent(
        site.meeting.emmaMeetingId,
        {filter_session_type_id: channel.sessionTypeID},
        true
    );

    return (
        <Layout site={site}>
            <Head>
                <title>{site.title} On-Demand</title>
            </Head>

            <Wrapper bg={headerBackground} h="150px" justify="center" as="header">
                <Heading as="h1">{channel.title}</Heading>
            </Wrapper>

            <Wrapper>
                <Flex mt={12} direction={{base: 'column', md: 'row'}}>
                    <Box w={{base: '100%', md: '66%'}} pr={{base: 0, md: '30px'}} >
                        <VideoPlayer
                            videoId={eventNowPlaying.brightcove_id}
                            onVideoEnd={onVideoEnd}
                        />

                       <PlayerEventInfo event={eventNowPlaying} />

                    </Box>
                </Flex>
            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            channels: data.site.channels,
            slug: params.slug,
            locale: locale,
            user: user || false
        }
    }

})