import {useEffect, useState, useRef} from "react";
import Head from 'next/head'
import {useRouter} from "next/router";

import _ from "lodash";
import {format} from 'date-fns'

import {Box, Flex, AspectRatio, Stack, Center, Heading, Text, useTheme} from "@chakra-ui/react";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";
import {useConferenceEvents} from "services/emma/emma";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import TrackCarousel from "components/portal/trackCarousel";
import TrackList from "components/portal/trackList";
import ChannelMenu from "components/portal/channelMenu";
import VideoPlayer from "components/portal/players/videoPlayer";
import ChannelPlaylist from "components/portal/channelPlaylist";
import Ad from "components/ads/ad"
import ChannelNotReady from "components/portal/channelNotReady";
import BcSetUserEmailOnWindow from "components/portal/BcSetUserEmailOnWindow";

import ModeratorList from "components/agenda-basic/moderatorList";
import PlayerEventInfo from "components/portal/playerEventInfo";
import withSession from "../../src/lib/session";
import EventList from "components/portal/eventList";
import ChannelCarousel from "components/portal/channelCarousel";
import Login from "../login";
import LoadingIndicator from "components/ui/loadingIndicator";
import {VideoReady, VideoNotReady} from "components/portal/slide-overlays";
import AdBlock from "components/ads/adBlock";

export default function OnDemand(props) {

    const {site, channels, channel, channelIsReady, slug, user} = props

    // authorize
    if (channel.guardOnDemandVersion && !user) return <Login site={site}/>

    const router = useRouter()
    const { event } = router.query

    const theme = useTheme()
    const headerBackgroundImage = channel.bannerImageOndemand?.length ? "url(" + channel.bannerImageOndemand[0].url + ")" : null

    const playerId = channel.brightcovePlayerIdOnDemand || site.meeting.brightcovePlayerIdOnDemand
    const playerTopRef = useRef()
    const [eventNowPlaying, setEventNowPlaying] = useState(false)
    const [channelEvents, setChannelEvents] = useState(false)
    const [autoplay, setAutoplay] = useState(false)
    const hasSideAds = channel.adsOnDemandPage[0]?.sideAd1 || channel.adsOnDemandPage[0]?.sideAd2
    const playlistWidthDesktop = hasSideAds ? "75%" : "100%"
    const paddingRightDesktop = hasSideAds ? "30px" : 0

    const emmaParams = channel.channelType === 'sessionType'
        ? { filter_session_type_id: channel.sessionTypeID }
        : { filter_tracks: [channel.sessionTypeID] }

    const eventsConfig = { prepEventListForChannel: true }

    const { status, data, error, isFetching } = useConferenceEvents(
        site.meeting.emmaMeetingId,
        emmaParams,
        eventsConfig
    )

    function selectEvent(event) {
        setAutoplay(true)
        const playerTopPosition = -(document.body.getBoundingClientRect().top - playerTopRef.current.getBoundingClientRect().top + 70)
        document.body.scrollTop = document.documentElement.scrollTop = playerTopPosition

        const newRoute = "/ondemand/" + slug[0] + "/" + event.id
        router.push(newRoute, undefined, { shallow: true })
    }

    useEffect(() => {

        if (!data?.length) return false

        // look for event id in URL
        const eventId = parseInt(router.query.slug[1], 10)
        const newEvent = getEventById(eventId)

        if (newEvent) {
            setEventNowPlaying(newEvent)
        } else {
            setEventNowPlaying(data[0])
        }

        // add overlays
        data.map(event => event.overlay = event.brightcove_id ? <VideoReady /> : <VideoNotReady />)

    }, [router.query.slug, data])

    useEffect(() => {

        if (channelEvents.length || !data?.length) return false

        let organizedEvents = {}

        switch (channel.eventListOrganization) {
            case 'byTrack' :
                organizedEvents = separateEventsByTrack(data)
                break

            case 'bySessionType':
                organizedEvents = separateEventsBySessionType(data)
                break

            default:
                organizedEvents[channel.title] = {
                    label: `More in ${channel.title}`,
                    style: 'large',
                    events: data
                }
        }

        setChannelEvents(organizedEvents)

        // console.log(eventsByTrack);

    }, [data])

    function separateEventsByTrack(data) {
        let organizedEvents = {}
        data.map(event => {
            event.tracks.map(eventTrack => {
                if (!organizedEvents[eventTrack]) {
                    organizedEvents[eventTrack] = {
                        label: eventTrack,
                        style: 'medium',
                        events: []
                    }
                }
                organizedEvents[eventTrack].events.push(event)
            })
        })

        return organizedEvents
    }

    function separateEventsBySessionType(data) {
        let organizedEvents = {}
        data.map(event => {
            if (!organizedEvents[event.session_type_id]) {
                organizedEvents[event.session_type_id] = {
                    label: event.session_type,
                    style: 'medium',
                    events: []
                }
            }
            organizedEvents[event.session_type_id].events.push(event)
        })

        return organizedEvents
    }

    function getEventById(id) {
        return _.find(data, event => {
            return event.id === id
        }) || false
    }

    function onVideoEnd(videoId) {

        // get the next video in the list
        const index = _.findIndex(data, event => {
            return event.brightcove_id === videoId
        })

        if (index < data.length) {
            selectEvent(data[index+1])
        }
    }

    return (
        <Layout site={site}>
            <Head>
                <title>{site.meeting.seoMetaTitle} | {channel.title} On Demand</title>
            </Head>

            <BcSetUserEmailOnWindow user={user} />

            <Wrapper as="header" bgImage={{base: null, sm: headerBackgroundImage}} bgColor="color5"  backgroundSize="cover" backgroundPosition="right center" h="150px" justify="center" >
                <Heading as="h1" fontSize="40px" textTransform="uppercase" color={channel.color1}>{channel.title} <Text as="span" color="color7">On Demand</Text></Heading>
            </Wrapper>

            <ChannelMenu currentChannel={channel} channels={channels} context="ondemand" variant={site.theme.channelNavigation}/>

            <Wrapper>
                <Box mt={10} ref={playerTopRef} >
                    {channelIsReady ? (
                        <AspectRatio ratio={16/9}>
                            {eventNowPlaying ? (
                                eventNowPlaying.brightcove_id ? (
                                    <VideoPlayer
                                        playerId={playerId}
                                        videoId={eventNowPlaying ? eventNowPlaying.brightcove_id : 0}
                                        onVideoEnd={onVideoEnd}
                                        user={user}
                                        autoplay={autoplay}
                                    />
                                ) : (
                                    <Center bg="#333">
                                        <VideoPlayer
                                            playerId={playerId}
                                            videoId={6251097610001}
                                            onVideoEnd={onVideoEnd}
                                            user={user}
                                            autoplay={autoplay}
                                        />
                                    </Center>
                                )
                            ) : (
                                <LoadingIndicator color="#fff"/>
                            )}
                        </AspectRatio>
                    ) : (
                        <ChannelNotReady channel={channel} />
                    )}

                    {channelIsReady && (
                        <PlayerEventInfo
                           event={eventNowPlaying}
                           color={channel.color1}
                       />
                    )}
                </Box>
            </Wrapper>

            {channel.adsOnDemandPage[0]?.leaderboardAd && (
                <Flex justify="center" align="center" bg="#eee" pt="20px" pb="35px" my="30px" direction="column">
                    <Text color="#333" fontSize={11} mb={1}>Advertisement</Text>
                    {/*<Ad slot={{w: "720px", h: "90px", label: "728 x 90 Horizontal Banner Ad"}}/>*/}

                        <AdBlock
                            adUnit={channel.adsOnDemandPage[0].leaderboardAd}
                            sizes={[
                                [728,210],
                                [728,90],
                                [350,44],
                                [320,100],
                                [320,50]
                            ]}
                        />
                </Flex>
            )}

            <Wrapper mb="30px">
                <Flex direction={{base: "column", lg: "row"}}>
                    <Box w={{base: "100%", lg: playlistWidthDesktop}} pr={{base: 0, lg: paddingRightDesktop}}>

                        {channelEvents && Object.entries(channelEvents).map(([key, track]) => (
                            <ChannelPlaylist
                                key={key}
                                style={track.style}
                                label={track.label}
                                channel={channel}
                                events={track.events}
                                currentEvent={eventNowPlaying}
                                selectEvent={selectEvent}
                                colorMode="dark"
                                channelIsReady={channelIsReady}
                            />
                        ))}

                    </Box>

                    {hasSideAds && (
                        <Flex w={{base: "100%", lg: "25%"}} align="center" direction="column">
                            <Text color="#fff" fontSize={11} mb={1} align="center">Advertisement</Text>
                            <Flex direction={{base: "row", lg: "column"}} justify="space-around" align="center">

                                {/*<Ad slot={{w: "240px", h: "400px", label: "240 x 400 Vertical Banner Ad 1"}}/>*/}

                                {channel.adsOnDemandPage[0]?.sideAd1 && (
                                    <AdBlock
                                        adUnit={channel.adsOnDemandPage[0].sideAd1}
                                        sizes={[
                                            [240,400]
                                        ]}
                                    />
                                )}

                                {channel.adsOnDemandPage[0]?.sideAd2 && (
                                    <AdBlock
                                        adUnit={channel.adsOnDemandPage[0].sideAd2}
                                        sizes={[
                                            [240,400]
                                        ]}
                                    />
                                )}

                            </Flex>
                        </Flex>
                    )}

                </Flex>

            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    const thisChannel = data.site.channels.find(channel => channel.slug === params.slug[0])

    // 404
    if (!thisChannel) {
        return {
            notFound: true,
        }
    }

    // is the ondemand version of this channel available?
    const channelIsReady = thisChannel?.currentlyAvailableVersions.includes('ondemand')

    // authorize
    // if (thisChannel.guardOnDemandVersion && !user) {
    //     return {
    //         redirect: {
    //             permanent: false,
    //             destination: "/login"
    //         }
    //     }
    // }

    return {
        props: {
            site: data.site,
            channels: data.site.channels,
            channel: thisChannel,
            slug: params.slug,
            locale: locale,
            user: user || false,
            channelIsReady: channelIsReady
        }
    }

})
