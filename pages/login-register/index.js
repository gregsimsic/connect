import Head from 'next/head'
import withSession from "../../src/lib/session";

import {Stack, Box, Center, Heading, Text, Link, Image, Flex, Button} from '@chakra-ui/react'

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";

import {getSiteIdByLocale} from "services/sites";
import {getSingle, getSiteData} from "services/craft/craft";
import AdBlock from "components/ads/adBlock";

export default function LoginRegister(props) {

    const { site, page } = props

    const bgStyles = {
        bgColor: "color5",
        bgSize: "cover",
        bgPosition: "center center"
    }

    const bgImageSrc = page.pageBackgroundMedia?.length ? page.pageBackgroundMedia[0].url : null
    if (bgImageSrc) {
        bgStyles.bgImage = `url(${bgImageSrc})`
    }

    const buttonStyles = {
        fontSize: "20px",
        fontWeight: 400,
        textTransform: "uppercase",
        py: "35px",
        width: "400px",
        maxWidth: "100%",
        color: "white",
        backgroundColor: "color1",
        borderRadius: 0,
        _hover: {
            backgroundColor: "color4",
            cursor: "pointer"
        }
    }
    
    return (
        <Layout site={site} bgStyles={bgStyles}>

            <Head>
                <title>Login / Register | {site.meeting.seoMetaTitle}</title>
                <meta name="description" value="Login / Register Page"/>
            </Head>
            
            <Wrapper >

                <Center mt="90px" mb="25px">
                    <Image src={page.lockup[0]?.url} maxHeight="205px"/>
                </Center>

                <Flex mb="25px" align="center" direction="column" >
                    <Box
                        dangerouslySetInnerHTML={{__html: page.copy}}
                        mb="20px"
                        textAlign="center"
                        fontSize="21px"
                    />
                    <Stack direction={{base: 'column', md: 'row'}} mx="15px" maxW="100%">

                        <Box mr={{base: 0, md: "15px"}}>
                            <Text mb="10px" fontSize="21px">Already registered? Log in now.</Text>
                            <Link href={"/login"} _hover={{textDecoration:"none"}}>
                                <Button {...buttonStyles} bgColor="color3">
                                    Login
                                </Button>
                            </Link>
                        </Box>
                        <Box >
                            <Text mb="10px" fontSize="21px">Not registered? Register now.</Text>
                            <Link href={page.registerPageSlug} _hover={{textDecoration:"none"}}>
                                <Button {...buttonStyles}>
                                    Register Now
                                </Button>
                            </Link>
                        </Box>

                    </Stack>
                </Flex>

                <Flex align="center" mt="50px" mb="25px" direction="column">
                    {page.leaderboardAdLoginRegisterPage && (
                        <>
                            <Text color="#eee" fontSize={11} mb={1}>Advertisement</Text>
                            <AdBlock
                                adUnit={page.leaderboardAdLoginRegisterPage}
                                sizes={[
                                    [728,210],
                                    [728,90],
                                    [320,100],
                                    [320,50]
                                ]}
                            />
                        </>
                    )}
                </Flex>

            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function (ctx) {

    const user = ctx.req.session.get("user");

    const craftSiteId = getSiteIdByLocale(ctx.locale)

    // get site data
    const data = await Promise.all([
        getSingle('loginRegister', craftSiteId, ctx.query.draftId),
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            page: result[0],
            site: result[1]
        };
    });
    
    return {
        props: {
            site: data.site,
            page: data.page.data,
            locale: ctx.locale,
            user: user || false
        }
    }

})
