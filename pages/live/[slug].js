import {useEffect, useState} from "react";
import Head from 'next/head'
import {useRouter} from "next/router";

import _ from "lodash";
import {isBefore, isAfter, differenceInSeconds, addSeconds, differenceInDays} from 'date-fns'

import {Box, Flex, Stack, AspectRatio, Center, Heading, Text, Link, Button, useTheme} from "@chakra-ui/react";

import {getSiteIdByLocale} from "services/sites";
import {getSiteData} from "services/craft/craft";
import {useConferenceEvents} from "services/emma/emma";
import withSession from "../../src/lib/session";
import sites from 'config/sites'
import livestreamData from 'config/live'

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";
import ChannelMenu from "components/portal/channelMenu";
import PlayerEventInfoLive from "components/portal/playerEventInfoLive";
import ChannelPlaylist from "components/portal/channelPlaylist";
// import EventList from "components/portal/eventList";
import VideoPlayer from "components/portal/players/videoPlayer";
import MockClock from "components/portal/mockClock";
import Pigeonhole from "components/portal/pigeonhole";
import Chat from "components/portal/chat";
import Ad from "components/ads/ad"
import AdBlock from "components/ads/adBlock"
import Login from "../login";
import {ComingUp, ComingUpNext, NowPlaying} from "components/portal/slide-overlays";
import LoadingIndicator from "components/ui/loadingIndicator";
import BcSetUserEmailOnWindow from "components/portal/BcSetUserEmailOnWindow";
import FormattedDateString from "components/agenda/formattedDateString";
import PlayerStatusMessage from "components/portal/playerStatusMessage";
import {utcToZonedTime} from "date-fns-tz";

/*
    Mock Clock

    For testing purposes a mock clock allows you to set the time to something that is during the meeting (overriding the browser's current time)

    Test with URLs like: http://tct2020.crfconnect.lcl:3000/live/main-arena?mock=2020-10-14T09:30:50

 */

export default function Live(props) {

    const {site, channels, channel, user, locale} = props

    // authorize
    if (channel.guardLiveVersion && !user) return <Login site={site}/>

    const router = useRouter()
    const { mock } = router.query

    const theme = useTheme()
    const headerBackgroundImage = channel.bannerImageLive?.length ? "url(" + channel.bannerImageLive[0].url + ")" : null
    
    const emmaParams = channel.channelType === 'sessionType'
        ? { filter_session_type_id: channel.sessionTypeID }
        : { filter_tracks: [channel.sessionTypeID] }

    const eventsConfig = { prepEventListForChannel: true }

    const { status, data, error, isFetching } = useConferenceEvents(
        site.meeting.emmaMeetingId,
        emmaParams,
        eventsConfig
    );
    
    const initialTime = new Date()
    const [mockTime, setMockTime] = useState(false)

    const meetingConfig = sites[locale]
    const isSimulive = !!channel.currentSimuliveEmbedSource
    let simuliveEmbedSrc = isSimulive ? channel.currentSimuliveEmbedSource : null
    simuliveEmbedSrc = user ? simuliveEmbedSrc + "?bc-email=" + user.email : simuliveEmbedSrc

    const hasSideAds = channel.adsLivePage[0]?.sideAd1 || channel.adsLivePage[0]?.sideAd2
    const playlistWidthDesktop = hasSideAds ? "75%" : "100%"
    const paddingRightDesktop = hasSideAds ? "30px" : 0

    const [currentOrNextEvent, setCurrentOrNextEvent] = useState(false)
    const [currentOrNextEventIndex, setCurrentOrNextEventIndex] = useState(0)
    const [currentOrNextEventIsPlaying, setCurrentOrNextEventIsPlaying] = useState(false)
    const [livestreamId, setLivestreamId] = useState(false)
    const [chatSessionId, setChatSessionId] = useState(false)
    const [pastEvents, setPastEvents] = useState([])
    const [futureEvents, setFutureEvents] = useState([])
    const [playerId, setPlayerId] = useState(site.meeting.brightcovePlayerIdLive)

    const LivePlayerDesktopWidth = () => {
        return currentOrNextEventIsPlaying && channel.currentPigeonholeSession ? "66%" : "100%"
    }

    // check for ?mock=[datetime] in the URL to open the mock clock for testing
    useEffect(() => {
        if (mock) {
            setMockTime(new Date(mock))
        }
    }, [])

    // update page state when current time changes
    useEffect(() => {
        if (!data?.length) return false

        // every 5 seconds update the page state
        const updateInterval = setInterval(checkCurrentOrNextEvent, 5000)
        checkCurrentOrNextEvent()
        return () => clearInterval(updateInterval)

    }, [data, mockTime]);

    // add overlays
    useEffect(() => {

        if (!data?.length) return false

        // default overlay
        data.map(event => event.overlay = <ComingUp />)
        data[1].overlay = <ComingUpNext />
        if (futureEvents.length) futureEvents[0].overlay = <ComingUpNext />

    }, [data]);

    // update event info
    useEffect(() => {
        if (currentOrNextEvent) {
            updateEventsLists()

            const meetingDayIndex = getMeetingDayIndex()

            // if today is before the meeting do nothing
            if (meetingDayIndex < 0) return false

            // use the venue ID of the event to find it's room
            const room = livestreamData[locale]?.find(room => room.venueId === currentOrNextEvent.venue.id)

            // let the Craft channel live player id settings take precedent over any config settings
            if (channel.brightcovePlayerIdLive) {
                setPlayerId(channel.brightcovePlayerIdLive)
            } else {
                // set live player id based on event room
                if (room) {
                    setPlayerId(room?.playerId)
                }
            }

            // let the Craft channel stream settings take precedent over any config settings
            if (channel.brightcoveLiveStreamMediaId) {
                setLivestreamId(channel.brightcoveLiveStreamMediaId)
            } else {
                // set live stream id based on event room
                setLivestreamId(room?.days[meetingDayIndex].streamId)
            }

            // let the Craft channel chat settings take precedent over any config settings
            if (channel.currentPigeonholeSession) {
                setChatSessionId(channel.currentPigeonholeSession)
            } else {
                // set chat id based on day and room
                setChatSessionId(room?.days[meetingDayIndex].pigeonHole)
            }

        }
    }, [currentOrNextEvent]);

    function getMeetingDayIndex() {
        
        console.log('meetingStartDate', site.meeting.meetingStartDate);

        // compare now (local time) to the date of the first event of the meeting (converted to local time)
        // to find out which day of the meeting it is
        const timezoneString = new window.Intl.DateTimeFormat().resolvedOptions().timeZone
        const zonedStartDate = utcToZonedTime(new Date(site.meeting.meetingStartDate), timezoneString)
        
        console.log('zonedStartDate', zonedStartDate);
        console.log('now', getTheTime());

        const meetingDayIndex = differenceInDays(getTheTime(), new Date(zonedStartDate))

        console.log('diff in days', meetingDayIndex);

        return meetingDayIndex
    }

    function getTheTime() {
        if (mockTime) {
            return addSeconds(mockTime, Math.abs(differenceInSeconds(initialTime, new Date())))
        } else {
            return new Date()
        }
    }

    // update future and past events lists
    const checkCurrentOrNextEvent = () => {

        // TODO: keep last event up ?? What to show at end "Thank you for watching"
        // find the first event whose end time has not elapsed
        const _currentOrNextEventIndex = _.findIndex(data, event => {
            return isBefore(
                getTheTime(),
                new Date(event.datetime_end)
            )
        })

        if (_currentOrNextEventIndex === -1) return false

        setCurrentOrNextEventIndex(_currentOrNextEventIndex)

        const _currentOrNextEvent = data[_currentOrNextEventIndex]

        setCurrentOrNextEvent(_currentOrNextEvent)

        setCurrentOrNextEventIsPlaying(
            isAfter(
                getTheTime(),
                new Date(_currentOrNextEvent.datetime_start)
            )
        )

    }

    // update future and past events lists
    const updateEventsLists = () => {

        setFutureEvents( _.slice(data, currentOrNextEventIndex + 1))
        if (currentOrNextEventIndex > 0) {
            setPastEvents( _.slice(data, 0, currentOrNextEventIndex))
        }

    }

    // allow updates to the mock time
    function updateTime(t) {
        setMockTime(t)
    }

    return (
        <Layout site={site} bg="#fff">
            <Head>
                <title>{site.meeting.seoMetaTitle} | {channel.title} Live</title>
                {channel.adSlot && (
                    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
                )}
            </Head>

            <BcSetUserEmailOnWindow user={user} />

            <Wrapper bgImage={{base: null, sm: headerBackgroundImage}} bgColor="color11" backgroundSize="cover" backgroundPosition="right center" h="150px" justify="center" as="header">
                <Heading as="h1" fontSize="40px" textTransform="uppercase" color={channel.color1}>{channel.title} <Text as="span" color="color2">Live</Text></Heading>
                {mock && (
                    <MockClock mockTime={mockTime} setMockTime={updateTime}/>
                )}
            </Wrapper>

            <ChannelMenu currentChannel={channel} channels={channels} context="live" variant={site.theme.channelNavigation}/>

            <Wrapper>
                <Flex mt={10} direction={{base: 'column', md: 'row'}}>
                    <Box w={{base: '100%', md: LivePlayerDesktopWidth()}} >

                        {currentOrNextEventIsPlaying ?

                            isSimulive ? (
                                <AspectRatio ratio={16 / 11}>
                                    <iframe
                                        style={{
                                            display: 'block',
                                            border: 'none',
                                            marginLeft: 'auto',
                                            marginRight: 'auto'
                                        }}
                                        src={simuliveEmbedSrc}
                                        allow="autoplay; fullscreen; geolocation; encrypted-media"
                                        allowfullscreen webkitallowfullscreen mozallowfullscreen
                                    ></iframe>
                                </AspectRatio>
                            ) : (
                                <Box>
                                    <AspectRatio ratio={16 / 9}>
                                        <VideoPlayer
                                            playerId={playerId}
                                            videoId={livestreamId}
                                            user={user}
                                        />
                                    </AspectRatio>
                                    <SessionCutOffMessage />
                                </Box>
                            )

                            : (
                                <AspectRatio ratio={{base: 3/2, md: 3/1}}>
                                    <Center>
                                        <Box textAlign="center">
                                            {currentOrNextEvent &&
                                                <PlayerStatusMessage
                                                    prevEvent={data[currentOrNextEventIndex - 1]}
                                                    nextEvent={currentOrNextEvent}
                                                    getTheTime={getTheTime}
                                                />
                                            }
                                            <SessionCutOffMessage />
                                        </Box>
                                    </Center>
                                </AspectRatio>
                            )

                        }

                    </Box>
                    {currentOrNextEventIsPlaying && chatSessionId && (
                        <Box w={{base: '100%', md: '34%'}} pl={{base: 0, md: '30px'}} mt={{base: "20px", md: 0}}>
                            <Pigeonhole
                                sessionId={chatSessionId}
                                passcode={site.meeting.pigeonholeEventPasscode}
                                user={user}
                            />
                        </Box>
                    )}

                </Flex>

                {currentOrNextEvent && (
                    <PlayerEventInfoLive event={currentOrNextEvent} isPlaying={currentOrNextEventIsPlaying} color="color2"/>
                )}

            </Wrapper>

                <Flex justify="center" align="center" bg="#eee" pt="20px" pb="35px" my="30px" direction="column">
                    {channel.adsLivePage[0]?.leaderboardAd && (
                        <>
                            <Text color="#333" fontSize={11} mb={1}>Advertisement</Text>
                            {/*<Ad slot={{w: "720px", h: "90px", label: "728 x 90 Horizontal Banner Ad"}}/>*/}
                            <AdBlock
                                adUnit={channel.adsLivePage[0].leaderboardAd}
                                sizes={[
                                    [728,210],
                                    [728,90],
                                    [350,44],
                                    [320,100],
                                    [320,50]
                                ]}
                            />
                        </>
                    )}
                </Flex>

            <Wrapper mb="30px">
                <Flex direction={{base: "column", lg: "row"}}>
                    <Box w={{base: "100%", lg: playlistWidthDesktop}} pr={{base: 0, lg: paddingRightDesktop}}>

                        {status === "loading" ? (
                            <LoadingIndicator />
                        ) : !futureEvents?.length ? (
                            <Box>
                                <Link href={"/ondemand/" + channel.slug} _hover={{textDecoration:"none"}}>
                                    <Button size="lg" borderRadius={0} bgColor="color2" _hover={{bgColor:"color1"}}>
                                        Watch Past Events in {channel.title}
                                    </Button>
                                </Link>
                            </Box>
                        ) : (
                            <>
                                <Heading as="h2" mb={6} color="color2">What's Next in {channel.title}</Heading>
                                <ChannelPlaylist events={futureEvents} channel={channel} />
                            </>
                        )}

                        {/*{pastEvents.length ? (*/}
                        {/*    <>*/}
                        {/*        <Heading as="h2" mb={6} color="color2">Past Sessions</Heading>*/}
                        {/*        <EventList events={pastEvents} />*/}
                        {/*    </>*/}
                        {/*) : (null)}*/}
                    </Box>

                    {hasSideAds && (
                        <Flex w={{base: "100%", lg: "25%"}} align="center" direction="column">
                            <Text color="#333" fontSize={11} mb={1}>Advertisement</Text>
                            <Flex direction={{base: "row", lg: "column"}} justify="space-between" align="center">

                                {/*<Ad slot={{w: "240px", h: "400px", label: "240 x 400 Vertical Banner Ad 1"}}/>*/}

                                {channel.adsLivePage[0]?.sideAd1 && (
                                    <AdBlock
                                        adUnit={channel.adsLivePage[0].sideAd1}
                                        sizes={[
                                            [240,400]
                                        ]}
                                    />
                                )}

                                {channel.adsLivePage[0]?.sideAd2 && (
                                    <AdBlock
                                        adUnit={channel.adsLivePage[0].sideAd2}
                                        sizes={[
                                            [240,400]
                                        ]}
                                    />
                                )}

                            </Flex>
                        </Flex>
                    )}
                </Flex>
            </Wrapper>
        </Layout>
    )

}

function SessionCutOffMessage() {

    return (
        <Text color="#333" fontSize={15} fontStyle="italic" mb={5} maxWidth="700px">
            If a session runs over its allotted time, the live stream may be moved to the next concurrent session. The entire session will be available for on-demand viewing.
        </Text>
    )
}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    const data = await Promise.all([
        getSiteData(craftSiteId, true)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    const thisChannel = data.site.channels.find(channel => channel.slug === params.slug)

    // 404
    if (!thisChannel) {
        return {
            notFound: true,
        }
    }

    // if the live version of this channel is not available redirect to ondemand
    if (!thisChannel?.currentlyAvailableVersions.includes('live')) {
        return {
            redirect: {
                destination: '/ondemand/' + thisChannel.slug,
                permanent: false,
            }
        }
    }

    // authorize
    // if (thisChannel.guardLiveVersion && !user) {
    //     return {
    //         redirect: {
    //             permanent: false,
    //             destination: "/login"
    //         }
    //     }
    // }

    return {
        props: {
            site: data.site,
            channels: data.site.channels,
            channel: thisChannel,
            slug: params.slug,
            locale: locale,
            user: user || false
        }
    }

})
