import React, {useEffect, useState} from "react";
import Head from 'next/head'

import { getSiteIdByLocale } from "services/sites";
import { getSiteData } from "services/craft/craft";

import Layout from "components/layout/layout";
import Wrapper from "components/layout/wrapper";

import {Box, Heading, Divider, CircularProgress, useTheme} from "@chakra-ui/react";

import Day from "components/agenda-basic/day";

import _ from "lodash"
import {useConferenceEvents} from "services/emma/emma";
import LoadingIndicator from "components/ui/loadingIndicator";
import withSession from "../../src/lib/session";
import SectionBanner from "components/header/sectionBanner";
import Session from "components/agenda-basic/session";

export default function Program(props) {

    const theme = useTheme()

    const [days, setDays] = useState([]);
    const [onDemandSessions, setOnDemandSessions] = useState([]);

    const { status, data, error, isFetching } = useConferenceEvents(
        props.site.meeting.emmaMeetingId,
        {
            filter_status: "All"
        },
        {
            withVideoOnly: false,
            removePrivateAndNotOnDemandSessions: true
        }
    );

    useEffect(async () => {

        if (!data) return false

        // split into 2 groups: live and on-demand only
        const grouped = _(data)
            .groupBy(event => event.params.on_demand)
            .map((value, key) => ({ondemand: key, event: value}))
            .value();

        // group each by day
        const liveDays = groupEventsByDay(grouped[0].event)
        setDays(liveDays)

        // const onDemandDays = groupEventsByDay(grouped[1].event)
        setOnDemandSessions(grouped[1]?.event || null)

    }, [data]);

    function groupEventsByDay(events) {

        const eventsGroupedByDay = _.groupBy(events, 'day')

        return _.map(eventsGroupedByDay, events => {
            return {
                date: events[0].datetime_start,
                events: events
            }
        })

    }

    return (
        <Layout site={props.site}>

            <Head>
                <title>Program Guide : {props.site.meeting.meetingName}</title>
            </Head>

            <SectionBanner
                label="Program"
                flatmenu={props.site.flatmenu}
                theme={props.site.theme}
            />

            <Wrapper py="50px">

                <Heading as="h1" color="color4" mb={4}>Program Guide</Heading>

                { days.length
                    ? days.map((day, i) => (
                        <Day {...day} key={i}/>
                    ))
                    : <LoadingIndicator />
                }

                { onDemandSessions?.length ? (
                    <>
                        <Divider borderBottomWidth="3px" borderColor="color4" mt={16}/>
                        <Heading as="h1" color="color4" mb={4} mt={4}>On-Demand Events</Heading>

                        {onDemandSessions.map((session, i) => (
                            <Session key={i} {...session}/>
                        ))}
                    </>
                ) : (null)}

            </Wrapper>

        </Layout>
    )

}

export const getServerSideProps = withSession(async function ({ req, res, params, locale }) {

    const user = req.session.get("user");

    const craftSiteId = getSiteIdByLocale(locale)

    // get agenda & site data
    const data = await Promise.all([
        getSiteData(craftSiteId)
    ]).then(result => {
        return {
            site: result[0]
        };
    });

    return {
        props: {
            site: data.site,
            locale: locale,
            user: user || false
        }
    };

})