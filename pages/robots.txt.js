// import { getSiteIdByLocale } from "services/sites";
// import { getSiteData } from "services/craft/craft";

// NOTE: this is a basic robots.txt starter that is the same for all sites

export default function Robots() {}

const createRobotsTxt = () => `# Allow all user agents.
User-agent: *
Allow: /
`;

export const getServerSideProps = async function ({ req, res, locale }) {

    // const craftSiteId = getSiteIdByLocale(locale)

    // get site data
    // const data = await getSiteData(craftSiteId)

    res.setHeader('Content-Type', 'text/plain');
    res.write(createRobotsTxt());
    res.end();
    return { props: {} };

}