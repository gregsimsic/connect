const defaultLocale = process.env.DEFAULT_LOCALE || 'tvt2021'

module.exports = {
    async rewrites() {
        return [
            {
                source: '/topics',
                destination: '/channels',
            },
        ]
    },
    async headers() {
        return [
            {
                // This works, and returns appropriate Response headers:
                source: '/(.*).png',
                headers: [
                    {
                        key: 'Cache-Control',
                        value:
                            'public, max-age=180, s-maxage=180, stale-while-revalidate=180',
                    },
                ],
            },
        ]
    },
    i18n: {
        localeDetection: false,
        locales: ['tct2020', 'cto2021', 'fellows2021', 'tvt2021', 'tct2021', 'complications2021', 'tht2022'],
        defaultLocale: defaultLocale,

        domains: [
            // the single dev domain that matches the domain setup in the DO dev app; change the defaultLocale to change which site it loads from Craft
            {
                domain: 'dev.crfconnect.com',
                defaultLocale: defaultLocale,
            },
            // domains on the DO Apps Platform
            {
                domain: 'tct2020.crfconnect.com',
                defaultLocale: 'tct2020',
            },
            {
                domain: 'complications2021.crfconnect.com',
                defaultLocale: 'complications2021',
            },
            {
                domain: 'tht2022.crfconnect.com',
                defaultLocale: 'tht2022',
            },
            {
                domain: 'cto2021.crfconnect.com',
                defaultLocale: 'cto2021',
            },
            {
                domain: 'tct2021.crfconnect.com',
                defaultLocale: 'tct2021',
            },
            {
                domain: 'tvt2021.crfconnect.com',
                defaultLocale: 'tvt2021',
            },
            {
                domain: 'fellows2021.crfconnect.com',
                defaultLocale: 'fellows2021',
            },
            // domains for local development
            {
                domain: 'complications2021.crfconnect.lcl',
                defaultLocale: 'complications2021',
            },
            {
                domain: 'tht2022.crfconnect.lcl',
                defaultLocale: 'tht2022',
            },
            {
                domain: 'tct2020.crfconnect.lcl',
                defaultLocale: 'tct2020',
            },
            {
                domain: 'cto2021.crfconnect.lcl',
                defaultLocale: 'cto2021',
            },
            {
                domain: 'tct2021.crfconnect.lcl',
                defaultLocale: 'tct2021',
            },
            {
                domain: 'tvt2021.crfconnect.lcl',
                defaultLocale: 'tvt2021',
            },
            {
                domain: 'fellows2021.crfconnect.lcl',
                defaultLocale: 'fellows2021',
            },
        ],
    },
}