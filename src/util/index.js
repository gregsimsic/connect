import {format, utcToZonedTime} from "date-fns-tz";
import _ from "lodash";

export function formmattedDateString(event, config) {

    const defaults = {
        format: 'h:mm a zzz',
        showEndTime: false
    }

    const settings = {...defaults, ...config}

    // if showing the start and end times remove the timezone from the start time format
    const startTimeFormat = settings.showEndTime ? settings.format.substr(0, settings.format.lastIndexOf(" ")) : settings.format

    const timezoneString = new window.Intl.DateTimeFormat().resolvedOptions().timeZone
    const zonedStartDate = utcToZonedTime(new Date(event.datetime_start), timezoneString)
    const zonedEndDate = utcToZonedTime(new Date(event.datetime_end), timezoneString)

    let str = format(zonedStartDate, startTimeFormat, { timeZone: timezoneString })

    if (settings.showEndTime) {
        str += " — " + format(zonedEndDate, settings.format, { timeZone: timezoneString })
    }

    return str
}

// search for event id in the agenda recursively through children
export function findEventById(id, events) {

    for (let i = 0; i < events.length; i += 1) {

        if (events[i].id === id) {
            return events[i];
        } else {
            if (events[i].children) {
                const childrenResult = findEventById(id, events[i].children);
                if (childrenResult) return childrenResult
            }
        }
    }

    return false

}