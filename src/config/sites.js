// keys are 'locale' names in next.config.js (i18n section)
export default {
    complications2021: {
        craftId: 6,
        emmaId: 181,
        oktaGroups: [],
    },
    tht2022: {
        craftId: 7,
        emmaId: 178,
        oktaGroups: [],
    },
    tct2020: {
        craftId: 1,
        emmaId: 148,
        oktaGroups: ["TCT2020_ALL_ACCESS"],
    },
    cto2021: {
        craftId: 3,
        emmaId: 167,
        oktaGroups: ["CTO-CONNECT_ALL_ACCESS"],
    },
    tct2021: {
        craftId: 5,
        emmaId: 177,
        oktaGroups: ["TCT2021_FULL_ACCESS"],
    },
    tvt2021: {
        craftId: 4,
        emmaId: 176,
        oktaGroups: ["TVT2021_FULL_ACCESS", "TVT2021_ONDEMAND_ACCESS"],
    },
    fellows2021: {
        craftId: 2,
        emmaId: 179,
        oktaGroups: ["FELLOWS-COURSE-CONNECT-2021_ALL_ACCESS"]
    }
}
