// 1 stream per room
// 1 pigeonhole per room per day
export default {
    tvt2021: {
        // Sparkle
        1074: {
            stream: 1234567890123,
            pigeonholes: [1234567890123, 1234567890123, 1234567890123]
        },
        // Glimmer
        1083: {
            stream: 1234567890123,
            pigeonholes: [1234567890123, 1234567890123, 1234567890123]
        },
        // Fontaine
        1093: {
            stream: 1234567890123,
            pigeonholes: [1234567890123, 1234567890123, 1234567890123]
        }
    }
}
