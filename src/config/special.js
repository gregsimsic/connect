export default {
    // Emma meeting id
    176: {
        // suppress these events on portal pages
        suppressEvents: [80898, 80489, 80477],
        // alternate URLs for events in the program guide & faculty profile page
        alternateLinks: {
            80898: "", // Achieving Optimal Outcomes with an Intra-Annular Self-Expanding TAVR System
            80489: "", // Tricuspid Valve Therapies:  Why a Transcatheter Toolbox Is Required
            80477: "", // Aortic Stenosis, TAVR, ViV-TAVR, SAVR:  Peri-procedural Hemodynamics
        }
    }
}
