// 1 stream & pigeonhole per room per day
export default {
    tvt2021: [
        {
            room: "Sparkle Ballroom",
            playerId: 'TCz4gpNPP',
            venueId: 1074,
            days: [
                {
                    streamId: 6264322186001,
                    pigeonHole: 2290321
                },
                {
                    streamId: 6264322186001,
                    pigeonHole: 2299613
                },
                {
                    streamId: 6264322186001,
                    pigeonHole: 2299614
                }
            ]
        },
        {
            room: "Glimmer Ballroom",
            venueId: 1083,
            playerId: 'ta0nJvEWK',
            days: [
                {
                    streamId: 6264323798001,
                    pigeonHole: 2299616
                },
                {
                    streamId: 6264323798001,
                    pigeonHole: 2299617
                },
                {
                    streamId: 6264323798001,
                    pigeonHole: 2299620
                }
            ]
        },
        {
            room: "Fontaine",
            venueId: 1093,
            playerId: 'btGasAUGC',
            days: [
                {
                    streamId: 6264325361001,
                    pigeonHole: 2299624
                },
                {
                    streamId: 6264325361001,
                    pigeonHole: 2299622
                },
                {
                    streamId: 6264325361001,
                    pigeonHole: 2299621
                }
            ]
        }
    ]
}
