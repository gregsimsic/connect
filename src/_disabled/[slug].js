import Head from 'next/head'
import styles from 'styles/Portal.module.css'
import { gql, GraphQLClient } from 'graphql-request'

export default function Page(page) {

  return (
    <div className={styles.container}>
      <Head>
        <title>{page.title + ' | ' + page.site.meeting.seoMetaTitle}</title>
        <description>{page.site.meeting.seoMetaDescription}</description>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          {page.title}
        </h1>

        <div></div>

      </main>

    </div>
  )

}

