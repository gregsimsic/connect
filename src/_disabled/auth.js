import authApi from "./authApi";

const signin = async (username, password) => {

    const { data: session } = await authApi.post('signin', { username, password })

    if (session) {

        setSession(session)

    }
}

const signout = async (email, password) => {

    const { data: success } = await authApi.post('signout', { email, password })

    if (success) {
        setSession(null)
    }

}

const getSession = async () => {

}

const setSession = async () => {

}

export default { signin, signout, getSession, setSession }