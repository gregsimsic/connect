import { Flex, Box, Heading, Text, Image, Center, Link, Circle } from '@chakra-ui/react'
import { Play } from "react-feather";

import {formmattedDateString} from "../../util";

export default function Slide(props) {

    const {event, link, selectEvent, defaultThumb, colorMode = "light"} = props

    const date = new Date(event.datetime_start)
    const formattedDate = formmattedDateString(event, {format: "MMMM d, Y — h:mm a zzz"})
    const img = event.links?.length ? event.links[0].preview_image_url : defaultThumb
    const selectEventCursor = selectEvent ? "pointer" : "default"
    const styles = {
        dark: {
            textColor: "#fff"
        },
        light: {
            textColor: "#555"
        },
    }

    function conditionalSelectEvent(event) {
        if (selectEvent) selectEvent(event)
    }

    return (
        <Box
            pos="relative"
            m="0 5px"
            cursor={selectEventCursor}
            onClick={() => conditionalSelectEvent(event)}
        >
            <Box pos="relative">
                <Box pos="relative">
                    <Image src={img} alt={event.title} fallbackSrc="/channels/event-slide-fallback-grey.jpg"/>
                    {link && (
                        <Center
                            pos="absolute"
                            left={0}
                            top={0}
                            w="100%"
                            h="100%"
                        >
                            <Circle size="60px" bg="#aaa" >
                                <Play style={{width: "30px", height: "30px", transform: "translateX(3px)", fill: "#333", stroke: "#333"}}/>
                            </Circle>
                        </Center>
                    )}
                    {/* conditional overlay -- now playing, upcoming */}
                    {event.overlay && (
                        <Box
                            pos="absolute"
                            left={0}
                            top={0}
                            w="100%"
                            h="100%"
                        >
                            {event.overlay}
                        </Box>
                    )}
                </Box>
                <Box color={styles[colorMode].textColor}>
                    <Heading as="h3"
                             textAlign="left"
                             fontSize={12}
                             fontWeight={600}
                             my={2}
                    >
                        {event.title}
                    </Heading>
                    {!event.params.on_demand && (
                        <Flex justify="space-between">
                            <Text as="span" fontSize={11}>{formattedDate}</Text>
                            <Text as="span" fontSize={11}>{event.duration} min.</Text>
                        </Flex>
                    )}
                </Box>
            </Box>
            {link && !selectEvent && (
                <Link
                    href={link}
                    alt={event.title}
                    _hover={{textDecoration: "none"}}
                    pos="absolute"
                    left={0}
                    top={0}
                    w="100%"
                    h="100%"
                    display="block"
                />
            )}
        </Box>
    )
}
