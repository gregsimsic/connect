import React, { createContext, useState, useContext, useEffect } from 'react'

import authApi from '../_disabled/authApi';
import axios from "axios";
const jwt = require('jsonwebtoken');

const AuthContext = createContext({});

export const AuthProvider = ({ children, sessionProp }) => {

    const [session, setSession] = useState(sessionProp)
    const [loading, setLoading] = useState(true)

    // check for session on component mount
    useEffect(() => {
        getSession()
    }, [])

    const getSession = async () => {
        const session = await authApi.get('session')
            .then(function (response) {
                console.log('session check', response);
                if (response.data.user) {
                    setSession(response.data.user);
                    return true
                } else {



                }
            })
            .catch(function (error) {
                console.log(error);
                return false
            })
            .then(function () {
                setLoading(false)
            });

        if (!session) {
            // This api call only works in the browser
            await axios.get('https://tctmd.okta.com/api/v1/sessions/me', {withCredentials: true})
                .then(function (response) {
                    console.log('session me browser', response);


                })
                .catch(function (error) {
                    // handle error
                    console.log('session me browser error');
                    console.log(error.response.data);

                })
                .then(function () {
                    // always executed
                });
        }

    }

    const signIn = async (username, password) => {

        const { data: session } = await authApi.post('signin', { username, password })

        if (session) {

            setSession(session)

        }
    }

    const signOut = async (email, password) => {

        const { data: success } = await authApi.get('signout')

        if (success) {
            setSession(null)
        }

        // reload page ?



    }


    return (
        <AuthContext.Provider value={{ isAuthenticated: !!session, session, signIn, loading, signOut }}>
            {children}
        </AuthContext.Provider>
    )
}

export const useAuth = () => useContext(AuthContext)

export const getSession = (req) => {

    const sessionToken = req.cookies.crfToken

    return sessionToken ? jwt.decode(sessionToken) : false

}