import styles from '../styles/Home.module.css'
import {useState, useEffect} from 'react';

export default function Datatest() {

    const [people, setPeople] = useState(
        []
    );

    useEffect(() => {
        fetch('http://www.huntermfastudio.org/people.json')
            .then(response => response.json())
            .then(data => {
                debugger
                setPeople(data.data.slice(0, 10));
            });

    }, []);

  return (
      <div className="datatest">
          <ul>
          {people.map((person) => {
              return (
                <li key={person.id}>{person.title}</li>
              )
          })}
          </ul>
      </div>
  )
}
