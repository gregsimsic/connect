import {Box, Flex} from "@chakra-ui/react";

import Day from './day'

export default function Days({days, count}) {

    return (
        <Flex>
            {days.map((day, i) => {
                return (
                    <Day key={i} {...day}/>
                )
            })}
        </Flex>
    )

}
