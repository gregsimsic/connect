import Axios from "axios";

// let urls = {
//     test: `http://localhost:3334`,
//     development: 'http://localhost:3333/',
//     production: 'https://your-production-url.com/'
// }

const UNAUTHORIZED = 401;

let api =  Axios.create({
    baseURL: '/api/auth/',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

api.interceptors.response.use(
    response => response,
    error => {
        console.log('intercept error')
        console.log(error)
        const {status} = error.response;
        if (status === UNAUTHORIZED) {
            // dispatch(userSignOut());
            console.log('401 intercepted')
        }
        return Promise.resolve(error);
    }
);

export default api
