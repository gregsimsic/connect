// ref: https://reacttricks.com/sharing-global-data-in-next-with-custom-app-and-usecontext-hook/

import { createContext, useContext } from 'react'

const GlobalStateContext = createContext({});

export const GlobalStateProvider = ({ children, site, locale }) => {

    // TODO: add Craft channels data here? needed by presentations & possibly other components

    return (
        <GlobalStateContext.Provider value={{site: site, locale: locale}}>
            {children}
        </GlobalStateContext.Provider>
    )
}

export const useGlobalState = () => useContext(GlobalStateContext)