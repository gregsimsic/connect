import React, { createContext, useState, useContext, useEffect } from 'react'

const AuthContext = createContext({});

export const AuthProvider = ({ children, user }) => {

    return (
        <AuthContext.Provider value={user}>
            {children}
        </AuthContext.Provider>
    )
}

export const useAuth = () => useContext(AuthContext)