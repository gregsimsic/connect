// this file is a wrapper with defaults to be used in both API routes and `getServerSideProps` functions
import {withIronSession} from "next-iron-session";

export default function session(handler) {
    return withIronSession(handler, {
        password: [
            {
                id: 1,
                password: process.env.SECRET_COOKIE_PASSWORD,
            }
        ],
        cookieName: "crfToken",
        cookieOptions: {
            // the next line allows to use the session in non-https environments like
            // Next.js dev mode (http://localhost:3000)
            secure: process.env.NODE_ENV === "production",
        },
    });
}