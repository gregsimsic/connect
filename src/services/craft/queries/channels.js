import {gql} from 'graphql-request';

export default gql`
query ($siteId: [QueryArgument])  {
  channels: entries(section: "channels", siteId: $siteId) {
    id
    slug
    title
    ...on channels_channels_Entry {
      channelType
      sessionTypeID
      eventListOrganization
      color1
      currentlyAvailableVersions
      brightcoveLiveStreamMediaId
      brightcovePlayerIdLive
      brightcovePlayerIdOnDemand
      isSimulive
      currentSimuliveEmbedSource
      currentPigeonholeSession
      guardLiveVersion
      guardOnDemandVersion
      adsLivePage {
          ...on adsLivePage_BlockType {
            leaderboardAd
            sideAd1
            sideAd2
          }
      }
      adsOnDemandPage {
          ...on adsOnDemandPage_BlockType {
            leaderboardAd
            sideAd1
            sideAd2
          }
      }
      bannerImageLive {
        url
      }
      bannerImageOndemand {
        url
      }
      eventListThumbnail {
        url
      }
      channelsPageSlideshowMedia {
        url
      }
      channelSlideshowCopy
    }
  }
}`
