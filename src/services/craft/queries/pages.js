import {gql} from 'graphql-request';

export default gql`
query ($siteId: [QueryArgument]) {
  pages: entries(siteId: $siteId, section: "htmlPages") {
    slug    
  }
}`
