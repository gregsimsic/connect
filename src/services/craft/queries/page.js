import {gql} from 'graphql-request';

export default gql`
query ($siteId: [QueryArgument], $slug: [String], $draftId: Int) {
  pages: entries(siteId: $siteId, section: "htmlPages", slug: $slug, draftId: $draftId) {
    title
    ... on htmlPages_page_Entry {
      userStatus
      showPageHeader
      pageBackgroundMedia {
        url
      }
      colorMode
      leaderboardAd: leaderboardAd {
        ... on leaderboardAd_ad_BlockType {
        adUnit
        sizes {
            ...on sizes_BlockType {
              width
              height
            }
          }
        }
      }
      matrix: pageMatrix {
        ... on pageMatrix_accordion_BlockType {
          type: typeHandle
          style
          accordion: sections {
            ... on sections_BlockType {
              label
              body
            }
          }
        }
        ... on pageMatrix_body_BlockType {
          type: typeHandle
          body
        }
        ... on pageMatrix_agendaSession_BlockType {
          type: typeHandle
          eventId
          showSessionTitle
        }
        ... on pageMatrix_advancedAgenda_BlockType {
          type: typeHandle
          showFilters
          showSearch
          showDownloadLinks
          sessionTypes
          onDemandOnly
        }
        ... on pageMatrix_ad_BlockType {
          type: typeHandle
          adUnit
          sizes {
              ...on sizes_BlockType {
                width
                height
              }
          }
        }
      }
    }
  }
}`
