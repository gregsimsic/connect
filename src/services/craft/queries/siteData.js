import {gql} from 'graphql-request';

export default gql`
query ($siteId: [QueryArgument]) {
  meeting: globalSet(handle: "meeting", siteId: $siteId) {
    siteId
    ... on meeting_GlobalSet {
      isLive
      meetingName
      emmaMeetingId
      seoMetaTitle
      seoMetaDescription
      brightcovePlayerIdLive
      brightcovePlayerIdOnDemand
      googleAdsNetworkId
      leaderboardAdChannelsPage
      leaderboardAdProgramGuide
      programDownloadCsv
      programDownloadPdf
      pigeonholeEventPasscode
      enableFacultyProfileLinks: reusableLightswitch
      sessionTypeTerm
      trackTerm
    }
  }
  theme: globalSet(handle: "theme", siteId: $siteId) {
    ... on theme_GlobalSet {
      colorPalette {
        ...on colorPalette_BlockType {
          color1
          color2
          color3
          color4
          color5
          color6
          color7
          color8
          color9
          color10
          color11
          color12
          color13
          color14
          color15
          color16
        }
      }
      meetingLogo {
        url
      }
      backgroundImage {
        url
      }
      channelNavigation
    }
  }
  flatmenu: entries(section: "navigation", siteId: $siteId) {
    id,
    title,
    slug,
    typeHandle,
    parent {
        id
    }
    ... on navigation_null_Entry {
      highlight
      userStatus
      backgroundImage {
        url
      }
    }
    ... on navigation_subhead_Entry {
      highlight
      userStatus
    }
    ... on navigation_path_Entry {
      highlight
      openInNewTab
      userStatus
      navigationPath
    }
    ... on navigation_link_Entry {
      string
      highlight
      userStatus
    }
    ... on navigation_htmlPage_Entry {
      highlight
      openInNewTab
      userStatus
      page {
          slug
      }
    }
    ... on navigation_action_Entry {
      highlight
      userStatus
      menuItemHandler
    }
  }
}`
