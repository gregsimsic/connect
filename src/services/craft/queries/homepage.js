import {gql} from 'graphql-request';

export default gql`
query ($siteId: [QueryArgument], $draftId: Int) {
  pages: entries(section: "homePage", siteId: $siteId, draftId: $draftId) {
    title
    ... on homePage_homePage_Entry {
      matrix: pageMatrix {
        ... on pageMatrix_accordion_BlockType {
          type: typeHandle
          accordion: sections {
            ... on sections_BlockType {
              label
              body
            }
          }
        }
        ... on pageMatrix_body_BlockType {
          type: typeHandle
          body
          backgroundColor
          backgroundImage {
            url
          }
        }
        ... on pageMatrix_courseDirectors_BlockType {
          type: typeHandle
          head
          backgroundColor
          highlightColor
          backgroundImage {
            url
          }
        }
        ... on pageMatrix_customComponent_BlockType {
          type: typeHandle
          component
        }
      }
    }
  }
}`
