import {gql} from 'graphql-request';

export default gql`
query ($siteId: [QueryArgument], $draftId: Int) {
  data: entry(section: "loginRegister", siteId: $siteId, draftId: $draftId) {
    ... on loginRegister_loginRegister_Entry {
      copy: description
      registerPageSlug: string
      leaderboardAdLoginRegisterPage
      lockup: image {
        url
      }
      pageBackgroundMedia {
        url
      }
    }
  }
}`
