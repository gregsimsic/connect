import _ from "lodash";

import {useQuery} from "react-query";
import {request, gql, GraphQLClient} from "graphql-request";

// GraphQL queries
import siteQuery from './queries/siteData'
import siteQueryWithChannels from './queries/siteDataWithChannels'
import homepageQuery from './queries/homepage'
import loginRegisterQuery from './queries/loginRegister'
import pageQuery from './queries/page'
import channelsQuery from './queries/channels'

export async function getSiteData(craftSiteId, withChannels = false) {

    // TODO: filter out guarded navigation items here ??
    // can't seem to do it with a graphql query in craft for child elements, potential Craft bug

    const query = withChannels ? siteQueryWithChannels : siteQuery

    const siteData = await cmsRequest(
        query,
        { siteId: craftSiteId }
        )
    // build menu hierarchy here
    siteData.menu = convertFlatMenuToStructuredTree(siteData.flatmenu)

    if (withChannels) {
        // add htmlTitle field that replaces hyphens with non-breaking hyphens
        siteData.channels.map(channel => {
            channel.htmlTitle = channel.title.replace(/-/gi, "&#8209;")
        })
    }

    return siteData

};

export function getSingle(section, craftSiteId, draftId = null) {

    let variables = {
        siteId: craftSiteId,
        draftId: draftId
    }

    const queries = {
        homePage: homepageQuery,
        loginRegister: loginRegisterQuery
    }

    return cmsRequest(queries[section], variables)

};

export function getPage(craftSiteId, slug, draftId = null, previewToken = null) {

    // TODO: guard pages that require a session

    let variables = {
        siteId: craftSiteId,
        slug: slug,
        draftId: draftId
    }

    return cmsRequest(pageQuery, variables, previewToken)

};

export async function getChannels(craftSiteId) {

    const channels = await cmsRequest(
        channelsQuery,
        { siteId: craftSiteId }
        )

    // add field that replaces hyphens with non-breaking hyphens
    channels.channels.map(channel => {
        channel.htmlTitle = channel.title.replace(/-/gi, "&#8209;")
    })

    return channels

};

const cmsRequest = async (query, variables, previewToken) => {

    // TODO: add isLoggedIn to all API calls ??
    // variables.isLoggedIn = !!getSession()
    // getSession() hits server api endpoint to return session /api/auth/session

    let endpoint = process.env.CRAFT_GQL_URL || process.env.NEXT_PUBLIC_CRAFT_GQL_URL

    // enable previewing drafts and Craft's live preview feature
    if (previewToken) {
        endpoint += "?token=" + previewToken
    }

    const client = new GraphQLClient( endpoint)

    return await client.request(
        query,
        variables
    ).then((data) => {
        return data;
    });

};



// TODO: convert queries to use the useQuery hook like this
function useSiteData(craftSiteId) {
    return useQuery(["site", craftSiteId], () => getSiteData(craftSiteId) );
}

/*****
    The functions below were examples from the react-query docs
 *****/
const getEventById = async (id) => {
    const { data } = await axios.get(
        emmaApiUrl + '/conferences/' + meetingId + '/event',
        {
            params: {
                depth: 'all',
                filter_session_type_id: sessionTypeID,
                filter_status: 'Published',
                limit: 10,
            }
        }
    );
    return data;
};

// use: const { status, data, error, isFetching } = useEvent(eventId);
function useEvent(eventId) {
    return useQuery(["event", eventId], () => getEventById(eventId), {
        enabled: !!eventId,
    });
}

// https://stackoverflow.com/questions/18017869/build-tree-array-from-flat-array-in-javascript
function convertFlatMenuToStructuredTree(list) {
    var map = {}, node, roots = [], i;

    for (i = 0; i < list.length; i += 1) {
        map[list[i].id] = i; // initialize the map
        list[i].pages = []; // initialize the children
    }

    for (i = 0; i < list.length; i += 1) {
        node = list[i];
        if (node.parent !== null) {
            // if you have dangling branches check that map[node.parentId] exists
            list[map[node.parent.id]].pages.push(node);
        } else {
            roots.push(node);
        }
    }
    return roots;
}