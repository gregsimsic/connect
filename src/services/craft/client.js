/*
    THIS IS NOW UNUSED -- GS, 7-6-2021
 */

import {GraphQLClient} from 'graphql-request';

export default new GraphQLClient( process.env.CRAFT_GQL_URL || process.env.NEXT_PUBLIC_CRAFT_GQL_URL);
