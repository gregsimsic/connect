import sites from 'config/sites'

export function getSiteIdByLocale(locale) {

    return sites[locale].craftId;

}

export function getEmmaMeetingIdByLocale(locale) {

    return sites[locale].emmaId;

}