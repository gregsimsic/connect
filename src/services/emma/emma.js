import {useQuery} from "react-query";
import axios from "axios";
import specialCases from 'config/special'

const emmaApiUrl = process.env.NEXT_PUBLIC_EMMA_API_URL

export function useConference(meetingId, config) {

    const defaultConfig = {
        queryKey: ['conference', meetingId]
    }

    config = {...defaultConfig, ...config}

    return useQuery(
        config.queryKey,
        async () => {

            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId,
            );

            return data.data;
        },{
            refetchOnWindowFocus: false
        });
}

// use: const { status, data, error, isFetching } = useEvents(meetingId, sessionTypeID);
export function useConferenceEvents(meetingId, params, config) {

    const defaultParams = {
        depth: 'all',
        filter_status: 'Published',
    }

    params = {...defaultParams, ...params}

    const defaultConfig = {
        queryKey: 'events',
        withVideoOnly: false,
        onDemandOnly: false,
        removePrivateAndNotOnDemandSessions: false,
        prepEventListForChannel: false
    }

    config = {...defaultConfig, ...config}

    return useQuery(
        config.queryKey,
        async () => {
            // TODO: how to get the meetingId here without passing it
            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId + '/events',
                {
                    params
                }
            );

            let events = data.events.data;

            // TODO: this code that filters for videos and flattens the events is probably no longer needed
            // if (config.withVideoOnly) {
            //     // filter events by those that have links
            //     // flatten array into sections/events
            //     // && !!section.links[0].preview_image_url !== ''
            //     events = events.flatMap((event) => {
            //         return event.children.flatMap((section) => {
            //             // does the section have a brightcove link?
            //             if (section.links?.length) {
            //                 return section
            //             } else {
            //                 // if not, then look at the presentations for brightcove links
            //                 return section.children.flatMap((presentation) => {
            //                     if (presentation.links?.length) {
            //                         return presentation
            //                     }
            //                 })
            //             }
            //         })
            //     });
            // }

            // only include ondemand events
            if (config.onDemandOnly) {
                events = events.filter((event) => {
                    return event.params.on_demand
                });
            }

            // only include events with brightcove video links
            if (config.withVideoOnly) {
                events = events.filter((event) => {
                    return event.links?.length
                });
            }

            // filter out events that are Private and aren't On-demand
            if (config.removePrivateAndNotOnDemandSessions) {
                events = events.filter((event) => {
                    return !(event.status == "Private" && !event.params.on_demand)
                });
            }

            // prep for channels pages
            if (config.prepEventListForChannel) {

                // flatten to section level
                events = events.flatMap(event => {
                    // pass parent tracks to child events
                    event.children.forEach(childEvent => childEvent.tracks = childEvent.tracks.concat(event.tracks))
                    return event.children
                })

                // for on demand only: videos are at the presentation level
                events = events.flatMap(event => {
                    if (event.params.on_demand) {
                        return [...event.children]
                    } else {
                        return event
                    }
                })

                // filter out "breaks"
                events = events.filter((event) => {
                    return event.title !== "Break"
                })

                // filter out suppressed events
                if (specialCases[meetingId]?.suppressEvents) {
                    events = events.filter((event) => {
                        return !specialCases[meetingId].suppressEvents.includes(event.id)
                    })
                }

                // for convenience, add brightcove fields to event
                events.map(event => {
                    const videoLink = event.links?.find(link => link.type === 'video' && link.id)
                    if (videoLink) {
                        event.preview_image_url = videoLink.preview_image_url,
                        event.brightcove_id = videoLink.id
                    }
                })

            }

            return events;
        },{
            refetchOnWindowFocus: false
        });
}

export function useConferenceEvent(meetingId, eventId, params, config) {

    const defaultParams = {
        depth: 'all',
        filter_status: 'Published',
    }

    params = {...defaultParams, ...params}

    const defaultConfig = {
        queryKey: ['event', eventId],
        withVideoOnly: true,
    }

    config = {...defaultConfig, ...config}

    return useQuery(
        config.queryKey,
        async () => {

            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId + '/events/' + eventId,
                {
                    params
                }
            );

            return data.event;
        },{
            refetchOnWindowFocus: false
        });
}

export function useConferenceFaculty(meetingId) {
    return useQuery(
        "faculty",
        async () => {
            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId + '/faculty',
                {
                    params: {
                        limit: 0,
                        filter_role: 'faculty'
                    }
                }
            );

            return data.faculty.data;
        },{
            refetchOnWindowFocus: false
        });
}

// https://agenda.crf.org/api/v1/conferences/179/special_faculty
export function useConferenceSpecialFaculty(meetingId) {
    return useQuery(
        "specialFaculty",
        async () => {
            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId + '/special_faculty',
            );

            return data.faculty.data.filter(faculty => faculty.role !== "Program Planning Group");
        },{
            refetchOnWindowFocus: false
        });
}

// https://agenda.crf.org/api/v1/conferences/176/categories/moderator-roles
export function useConferenceModeratorRoles(meetingId) {
    return useQuery(
        "moderatorRoles",
        async () => {
            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId + '/categories/moderator-roles',
            );

            return data.data.filter(role => role.name !== "Program Planning Group");

        },{
            refetchOnWindowFocus: false
        });
}

export function useConferenceFacultyById(meetingId, facultyId) {
    return useQuery(
        ["faculty", facultyId],
        async () => {
            const { data } = await axios.get(
                emmaApiUrl + '/conferences/' + meetingId + '/faculty' + facultyId
            );

            return data.event.data;
        },{
            refetchOnWindowFocus: false
        });
}
