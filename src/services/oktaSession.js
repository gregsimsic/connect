import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import { Modal, ModalContent, ModalBody, ModalOverlay, Center, Heading, Text, Spinner, Flex } from "@chakra-ui/react"
import axios from "axios";

export default function OktaSession({meetingKey}) {

    const router = useRouter()

    const [hasRun, setHasRun] = useState(false)
    const [userIsRegistered, setUserIsRegistered] = useState(false)

    // TODO: replace the sessionId ?

    // check if the user is already logged in via a local session or Okta session
    useEffect(async () => {

        if (hasRun) return false
        setHasRun(true)

        let localUser, oktaUser, isInGroup

        // first check /api/auth/user for local user session
        await axios.post('/api/auth/user')
            .then((response) => {

                if (response.data.isLoggedIn) {
                    localUser = response.data
                }

            })
            .catch ((error) => {

            })

        // if not found then ask Okta if there is an active session
        if (!localUser) {
            await axios.get('https://tctmd.okta.com/api/v1/sessions/me', {withCredentials: true})
            .then((response) => {

                // create a user object to save to a local session
                oktaUser = {
                    isLoggedIn: true,
                    oktaId: response.data.userId,
                    oktaSessionToken: response.data.id,
                    expiresAt: response.data.expiresAt,
                    name: response.data._links.user.name,
                    email: response.data.login
                }

            })
            .catch ((error) => {

            })

            // If we have an active Okta session, check to see if the user is registered for this meeting
            if (oktaUser) {

                await axios.post('/api/auth/checkGroup', { oktaId: oktaUser.oktaId, meetingKey: meetingKey })
                    .then((response) => {

                        if (response.data.isInGroup) {
                            setUserIsRegistered(true)
                            isInGroup = true
                        }

                    })
                    .catch ((error) => {
                        // TODO: console.log the error?

                    })
            }

            // If the user is in the group, then create a local user session and reload the page
            if (isInGroup) {

                await axios.post('/api/auth/loginWithOktaSession', {user: oktaUser} )
                    .then((response) => {

                    })
                    .catch ((error) => {

                    })

                // reload page
                router.reload()
            }
        } else {

            let sessionIdWasUpdated = false

            // check flag and swap the session token if needed (necessary for proper Okta logout)
            if (localUser.sessionTokenNeedsReplacement) {

                await axios.get('https://tctmd.okta.com/api/v1/sessions/me', {withCredentials: true})
                    .then((response) => {

                        localUser.oktaSessionToken = response.data.id
                        sessionIdWasUpdated = true

                    })
                    .catch ((error) => {

                    })

                if (sessionIdWasUpdated) {

                    localUser.sessionTokenNeedsReplacement = false

                    // save user to session with new session id
                    await axios.post('/api/auth/loginWithOktaSession', {user: localUser} )
                        .then((response) => {

                        })
                        .catch ((error) => {

                        })
                }

            }

        }

    }, [])

    return (
        <>
        {userIsRegistered
            ? <Modal isOpen={true} size="xs" isCentered>
                <ModalOverlay />
                <ModalContent>
                    <ModalBody>
                        <Center>
                            <Flex direction="column" align="center" py={8}>
                                <Spinner
                                         thickness="3px"
                                         speed="0.45s"
                                         emptyColor="gray.200"
                                         color="color1"
                                         size="lg"
                                />
                                <Text color="#999">Found your user. Logging you in...</Text>
                            </Flex>
                        </Center>
                    </ModalBody>
                </ModalContent>
            </Modal>
            : null }
        </>
    )
}
