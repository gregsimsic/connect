import React, {useState} from "react";
import {format} from "date-fns"

import {Box, Heading, useTheme} from "@chakra-ui/react";

import SessionGroup from './sessionGroup'

export default function Day(props) {

    const theme = useTheme()

    const groupedSessions = groupEventsBySessionType(props.events)

    function groupEventsBySessionType(events) {

        const eventsGroupedBySessionType = _.groupBy(events, 'session_type')

        return _.map(eventsGroupedBySessionType, events => {
            return {
                session_type: events[0].session_type,
                sessions: events
            }
        })

    }

    return (
        <Box>
            <Heading as="h3"
                     color="color1"
                     size="md"
                     fontWeight="normal"
                     // borderTop={"2px solid " + theme.colors.color1}
                     mt={16} pt={1}
            >
                {format(new Date(props.date), 'EEEE, MMMM d, Y')}
            </Heading>
            {groupedSessions.map((sessionGroup, i) => {
                return (
                    <SessionGroup key={i} {...sessionGroup}/>
                )
            })}
        </Box>
    )

}
