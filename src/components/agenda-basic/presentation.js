import {Flex, Box, Heading, Text, Icon, Tag, TagLeftIcon, TagLabel, useTheme, Link} from "@chakra-ui/react";
import FormattedDateString from "components/agenda/formattedDateString";
import ModeratorList from "./moderatorList"
import {PlayCircle} from "react-feather";
import {useGlobalState} from "../../providers/globalState";
import {isSameDay} from "date-fns";

export default function Presentation(props) {

    const theme = useTheme()

    const {site} = useGlobalState()

    const ConditionalLink = function({children}) {

        if (!site.meeting.isLive || props.title === "Break") return children

        // Render a link to the ondemand page and event if the Brightcove link is present
        if (props.links?.length && props.links[0].id) {
            return (
                <Link href={"/ondemand/" + props.channelSlug + "/" + props.id} isExternal>
                    {children}
                    <Box display="inline" pl={2} verticalAlign="middle"><Icon as={PlayCircle} w="24px" h="24px"/></Box>
                </Link>
            )
            // ... or render a link to the live channel page if event date is today & it's not on-demand only
        } else if (props.level === 0 && !props.params.on_demand && isSameDay(new Date(props.datetime_start), new Date())) {
            return (
                <Link href={"/live/" + props.channelSlug + "/"} isExternal>
                    {children}
                    <Box display="inline" pl={2} verticalAlign="middle"><Icon as={PlayCircle} w="24px" h="24px"/></Box>
                </Link>
            )
        } else {
            return children
        }
    }

    return (
        <Box mt={4} mb={(props.level && !props.children.length) ? 4 : 12}  >

            <Flex align="baseline">
                {(props.level && !props.params.on_demand) ? (
                    <Text mr="4" mt={0} minW={105} color="#eee">
                        <FormattedDateString event={props} />
                    </Text>
                ) : null }

                <Box>
                    <Heading as="h4"
                             size={ props.level ? 'sm' : 'md' }
                             fontWeight={ (props.level && !props.children.length) ? 'normal' : '600' }
                             mt="1" mb="1"
                             color={ props.level ? '#eee' : "color1" }
                    >
                        <ConditionalLink>{props.title}</ConditionalLink>
                    </Heading>
                    {props.description && (
                        <Text
                            fontSize={15}
                            lineHeight={1.2}
                            mt={1}
                            mb={3}
                        >
                            {props.description}
                        </Text>
                    )}

                    {props.type !== "ondemand" ?
                        !props.children.length ?
                            !props.level ? (
                                <Text mt={0} color="#eee">
                                    <FormattedDateString event={props} />
                                </Text>
                            ) : null
                            : null
                        : null }

                    {props.moderators.map((moderatorGroup, i) => {
                        return (
                            <ModeratorList
                                moderators={moderatorGroup.users}
                                label={moderatorGroup.role.name}
                                labelPlural={moderatorGroup.role.name_plural}
                                key={i}
                            />
                        )

                    })}
                </Box>
            </Flex>

            {props.children.map((event, i) => {
                return <Presentation key={i} {...event} level={props.level+1} channelSlug={props.channelSlug}/>
            })}

        </Box>
    )

}
