import {Box, Heading, Text} from "@chakra-ui/react";

import {useGlobalState} from "../../providers/globalState";

import Presentation from "./presentation";
import FormattedDateString from "components/agenda/formattedDateString";

export default function Session(props) {

    const {site} = useGlobalState()

    return (
        <Box mt="20px" pt="5px" >
            {props.showSectionTitles && (
                <Heading as="h3" size="lg" color="#eee" mt={1}>{props.title}</Heading>
            )}

            {site?.meeting.type !== "Virtual" && props.type !== "ondemand" && !props.params.on_demand && (
                <Box m="5px 0 25px" >
                    <Text m={0} color="#eee">
                        <FormattedDateString event={props} config={{showEndTime: true}}/>
                    </Text>
                    <Text m={0} color="#eee">Room: {props.venue?.name}</Text>
                </Box>
            )}

            {props.children.map((event, i) => {
                return <Presentation key={i} {...event} level={0} channelSlug={props.channelSlug}/>
            })}

        </Box>
    )

}