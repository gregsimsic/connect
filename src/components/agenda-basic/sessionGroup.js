import React from "react";

import {Box, Heading} from "@chakra-ui/react";

import Session from './session'

export default function SessionGroup(props) {

    return (
        <Box>
            <Heading as="h2" size="xl" color="#eee" mt={6}>{props.session_type}</Heading>

            {props.sessions.map((session, i) => {
                return (
                    <Session key={i} {...session}/>
                )
            })}
        </Box>
    )

}
