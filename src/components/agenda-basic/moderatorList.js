import {Flex, Box, Heading, Text} from "@chakra-ui/react";

export default function ModeratorList({moderators, label, labelPlural}) {

    label = (label === "Lecturer") ? "Presenter" : label
    labelPlural = (labelPlural === "Lecturers") ? "Presenters" : labelPlural

    function nameList(names) {
        return names.map((user, i) => {
            return user.full_name
        }).join(', ')
    }

    return (
        <Text m={0} color="#999" lineHeight="1.4">
            <Text
                as="span"
                fontWeight={600}
                pr={3}
            >
                {moderators.length > 1 ? labelPlural : label}
            </Text> {nameList(moderators)}
        </Text>
    )
}


