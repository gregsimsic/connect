// Components
import {Flex, Box} from "@chakra-ui/react"

// Custom Components
import CrfStripe from "../header/crfStripe";
import Header from "../header/header";
import Footer from "../footer/footer";

export default function Layout(props) {

    const bg = props.bg || props.site.theme.colorPalette[0].color5

    return (
        <Flex direction="column" minHeight="100vh" bg={bg} className={props.className} {...props.bgStyles}>

            <CrfStripe/>

            <Header site={props.site}/>

            <Box flexGrow={1}>
                {props.children}
            </Box>

            <Footer/>

        </Flex>
    )
}