import { Container, Flex, Spacer, Box } from "@chakra-ui/react"
import React from "react";

export default function Wrapper(props) {

    return (
        <Flex
            direction="column"
            w="100%"
            align="center"
            backgroundSize="cover"
            px="20px"
            {...props}
        >
            <Box
                w="100%"
                maxW={{ xl: 1200 }}
            >
                {props.children}
            </Box>
        </Flex>
    )
}