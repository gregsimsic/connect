import {Box, Icon} from "@chakra-ui/react"
import {ChevronLeft, ChevronRight} from "react-feather";

import styles from "./slider.module.scss"

export function CustomNextArrow(props) {
    const { className, colorMode = "dark", onClick } = props;
    const strokeColor = colorMode === "dark" ? "#fff" : "#333"

    return (
        <Box
            className={styles.slickArrow +" "+ styles.slickNext + " " + className}
            onClick={onClick}
            right="-35px"
        >
            <Icon as={ChevronRight} style={{ stroke: strokeColor }} w="100%" h="100%" />
        </Box>
    )
}

export function CustomPrevArrow(props) {
    const { className, colorMode = "dark", onClick } = props;
    const strokeColor = colorMode === "dark" ? "#fff" : "#333"
    return (
        <Box
            className={styles.slickArrow +" "+ styles.slickPrev + " slick-arrow slick-prev"}
            onClick={onClick}
            left="-35px"
        >
            <Icon as={ChevronLeft} style={{ stroke: strokeColor }} w="100%" h="100%"/>
        </Box>
    )
}