import React, {useEffect, useState} from "react";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from './slider.module.scss'

import Slider from 'react-slick'
import PlaylistSlide from "components/portal/playlistSlide";
import { Flex, Box } from "@chakra-ui/react";
import { CustomPrevArrow, CustomNextArrow } from "./arrows";
import {useConferenceEvents} from "services/emma/emma";
import LoadingIndicator from "../ui/loadingIndicator";

export default function TrackCarousel(props) {

    const slickSettings = {
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: <CustomNextArrow/>,
        prevArrow: <CustomPrevArrow/>
    };

    const { status, data, error, isFetching } = useConferenceEvents(props.meetingId, {filter_session_type_id: props.sessionTypeId});

    return (
        <>
            {status === "loading" ? (
                <LoadingIndicator />
            ) : status === "error" ? (
                <span>Error: {error.message}</span>
            ) : (
                <Slider {...slickSettings}>
                    {data.map(video => {
                        return (<PlaylistSlide {...video} key={video.id}/>)
                    })}
                </Slider>
            )}
        </>

    )
}
