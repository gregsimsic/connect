import Slider from 'react-slick'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

// import styles from './slider.module.scss'

import { Box } from "@chakra-ui/react";
import { CustomPrevArrow, CustomNextArrow } from "./arrows";
import LoadingIndicator from "../ui/loadingIndicator";

export default function VideoCarousel({slides, settings, colorMode, maxSlideCount = 4}) {

    const slickDefaults = {
        infinite: false,
        speed: 500,
        slidesToShow: maxSlideCount,
        slidesToScroll: 1,
        nextArrow: <CustomNextArrow colorMode={colorMode}/>,
        prevArrow: <CustomPrevArrow colorMode={colorMode}/>,
        dots: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: maxSlideCount,
                    slidesToScroll: maxSlideCount
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const slickSettings = {...slickDefaults, ...settings}

    return (
        <Box width="100%" display="block" className="video-slider">
            {slides.length
                ? <Slider {...slickSettings} style={{width: "100%"}} >{slides}</Slider>
                : <LoadingIndicator />
            }
        </Box>

    )
}
