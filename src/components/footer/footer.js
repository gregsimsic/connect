import {Container, Flex, Box, Link} from "@chakra-ui/react"
import SocialIcons from "../header/SocialIcons";

export default function Footer() {
    return (
        <Container as="footer" bg="white" maxW="100%" py="30px" color="gray.600" fontSize="11px" zIndex={10}>
            <Flex maxW={{xl: 1200}} w="100%" mx="auto" justify="space-between" direction={{base: 'column', lg: 'row'}} px={{base: '20px', lg: 0}}>
                <Box>
                    <img alt="CRF Logo" src="/CRF16-Logo-CMYK.png"/>
                </Box>
                <Box mt={{base: '20px', lg: 0}}>
                    <p>1700 Broadway, 9th Floor<br/>
                        New York, NY 10019<br/>
                        646.434.4500<br/>
                        <Link textDecoration="underline" href="mailto:info@crf.org">info@crf.org</Link>
                    </p>
                </Box>
                <Box mt={{base: '20px', lg: '15px'}}>
                    <SocialIcons />
                </Box>
                <Box mt={{base: '20px', lg: 0}}>
                    <p>© 2021, Cardiovascular Research Foundation. All rights reserved.</p>
                    <p>
                        <Link
                            textDecoration="underline"
                            target="_blank"
                            href="https://www.crf.org/privacy-policy"
                        >
                            Privacy Policy
                        </Link>
                        <br/>
                        <Link
                            textDecoration="underline"
                            target="_blank"
                            href="https://www.crf.org/terms-and-conditions-of-use"
                        >
                            Terms and Conditions of Use
                        </Link>
                        <br/>
                        <Link
                            textDecoration="underline"
                            onClick={() => window.Cookiebot.renew() }
                        >
                            Manage Cookie Settings
                        </Link>
                    </p>
                </Box>
            </Flex>
        </Container>
    )
}
