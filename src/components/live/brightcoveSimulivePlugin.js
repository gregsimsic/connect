// this is the plugin written by BC for TC 2020 live/simulive
// That adds a countdown until the channel goes live
//
// http://players.brightcove.net/5270290550001/crf/plugins/videojs-simulive-plugin-v2.js

/*! @name videojs-simulive-plugin @version 1.0.0 @license UNLICENSED */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('video.js')) :
        typeof define === 'function' && define.amd ? define(['video.js'], factory) :
            (global = global || self, global.videojsSimulivePlugin = factory(global.videojs));
}(this, (function (videojs) { 'use strict';

    videojs = videojs && Object.prototype.hasOwnProperty.call(videojs, 'default') ? videojs['default'] : videojs;

    function formatTimeToString(sec, options) {
        var sec_delta = sec;
        var min_delta = sec / 60;
        var hour_delta = sec / 3600;
        var day_delta = sec / 24 / 3600;
        var timeObject = {};
        timeObject.days = Math.floor(day_delta);
        timeObject.hours = Math.floor(hour_delta) - 24 * timeObject.days;
        timeObject.minutes = Math.floor(min_delta) - 60 * timeObject.hours - 60 * 24 * timeObject.days;
        timeObject.seconds = Math.floor(sec_delta) - Math.floor(hour_delta) * 60 * 60 - Math.floor(timeObject.minutes * 60);
        var parts = [];

        if (timeObject.days) {
            parts.push("<span class='bgs-simulive-days'><span class='bgs-simulive-value bgs-simulive-value-days'>" + timeObject.days + "</span><span class='bgs-simulive-label bgs-simulive-label-days'>" + (timeObject.days === 1 ? options.labels.countdown.day : options.labels.countdown.days) + "</span></span>");
        }

        if (timeObject.hours || timeObject.days) {
            parts.push("<span class='bgs-simulive-hours'><span class='bgs-simulive-value bgs-simulive-value-hours'>" + timeObject.hours + "</span><span class='bgs-simulive-label bgs-simulive-label-hours'>" + (timeObject.hours === 1 ? options.labels.countdown.hour : options.labels.countdown.hours) + "</span></span>");
        }

        if (timeObject.minutes || timeObject.hours || timeObject.days) {
            parts.push("<span class='bgs-simulive-minutes'><span class='bgs-simulive-value bgs-simulive-value-minutes'>" + timeObject.minutes + "</span><span class='bgs-simulive-label bgs-simulive-label-minutes'>" + (timeObject.minutes === 1 ? options.labels.countdown.minute : options.labels.countdown.minutes) + "</span></span>");
        }

        if (timeObject.seconds || timeObject.minutes || timeObject.hours || timeObject.days) {
            parts.push("<span class='bgs-simulive-seconds'><span class='bgs-simulive-value bgs-simulive-value-seconds'>" + timeObject.seconds + "</span><span class='bgs-simulive-label bgs-simulive-label-seconds'>" + (timeObject.seconds === 1 ? options.labels.countdown.second : options.labels.countdown.seconds) + "</span></span>");
        }

        return parts.join(" ");
    }

    var utils = {
        formatTimeToString: formatTimeToString
    };

    var version = "1.0.0";

    var defaults = {};
    var ModalDialog = videojs.getComponent('ModalDialog'); // Cross-compatibility for Video.js 5 and 6.

    var registerPlugin = videojs.registerPlugin || videojs.plugin; // const dom = videojs.dom || videojs;

    /**
     * Function to invoke when the player is ready.
     *
     * This is a great place for your plugin to initialize itself. When this
     * function is called, the player will have its DOM and child components
     * in place.
     *
     * @function onPlayerReady
     * @param    {Player} player
     *           A Video.js player object.
     *
     * @param    {Object} [options={}]
     *           A plain object containing options for the plugin.
     */

    var onPlayerReady = function onPlayerReady(player, options) {
        player.addClass('vjs-simulive-plugin'); // setup modal for countdown-overlay

        var modal_options = {
            temporary: false,
            uncloseable: true,
            label: 'countdown-modal',
            content: ''
        };
        var interval;
        var modal = new ModalDialog(player, modal_options);
        modal.addClass('vjs-countdown-modal');
        player.addChild(modal);
        options = options || {};

        if (!options.labels) {
            options.labels = {
                "countdown": {
                    "live": "Live in:",
                    "day": "day",
                    "days": "days",
                    "hour": "hour",
                    "hours": "hours",
                    "minute": "minute",
                    "minutes": "minutes",
                    "second": "second",
                    "seconds": "seconds"
                }
            };
        }

        if (!options.customFieldName) {
            options.customFieldName = "simulive_start";
        }

        if (!options.tagValue) {
            options.tagValue = "simulive";
        }

        if (window.ciabConfig && window.ciabConfig.labels && window.ciabConfig.labels.countdown) {
            if (!options.labels) {
                options.labels = {};
            }

            options.labels.countdown = window.ciabConfig.labels.countdown;
        }

        var getTimeLeftSeconds = function getTimeLeftSeconds(startTime) {
            var currDate = new Date(Date.now());
            var startDate = startTime;
            var diff = (startDate.getTime() - currDate.getTime()) / 1000;
            return Math.floor(diff);
        }; // countdown function


        var startCountdown = function startCountdown(startTime) {
            var timeLeft = getTimeLeftSeconds(startTime);

            var updateTime = function updateTime() {
                timeLeft = getTimeLeftSeconds(startTime) - 1;
            };

            var updateUI = function updateUI() {
                try {
                    if (timeLeft > 0) {
                        player.addClass('vjs-countdown-showing');
                        player.pause();
                        updateTime();
                        document.getElementById('modal_content_timer').innerHTML = utils.formatTimeToString(timeLeft, options);
                    } else {
                        clearInterval(interval);
                        modal.close();
                        player.removeClass('vjs-countdown-showing');
                        setupSimuliveUI();
                        player.play().catch(function () {
                            player.muted(true);
                            player.play();
                        });
                    }
                } catch (e) {
                    clearInterval(interval);
                }
            };

            interval = setInterval(updateUI, 1000);
            updateUI();
        }; // function to remove simulive-controls


        var removeSimuliveUI = function removeSimuliveUI() {
            if (interval) {
                clearInterval(interval);
            }

            modal.close();
            player.removeClass('vjs-countdown-showing');
            Array.from(document.getElementsByClassName("vjs-load-progress")).forEach(function (item) {
                item.style.display = 'block';
            });
            Array.from(document.getElementsByClassName("vjs-mouse-display")).forEach(function (item) {
                item.style.display = 'block';
            });
            Array.from(document.getElementsByClassName("vjs-play-progress")).forEach(function (item) {
                item.style.display = 'block';
            });
            Array.from(document.getElementsByClassName("vjs-seek-to-live-control")).forEach(function (item) {
                item.style.display = 'none';
                item.classList.remove("vjs-seek-show-control");
            });
            Array.from(document.getElementsByClassName("vjs-progress-holder")).forEach(function (item) {
                item.classList.remove("vjs-simulive-progress");
                player.controlBar.progressControl.enable();
            });
        }; // function to enable simulive-controls


        var setupSimuliveUI = function setupSimuliveUI() {
            Array.from(document.getElementsByClassName("vjs-time-control")).forEach(function (item) {
                item.classList.add("vjs-countdown-hide-control");
            }); // add LIVE button

            Array.from(document.getElementsByClassName("vjs-seek-to-live-control")).forEach(function (item) {
                item.style.display = 'block';
                item.classList.add("vjs-seek-show-control");
            });
            Array.from(document.getElementsByClassName("vjs-progress-holder")).forEach(function (item) {
                item.classList.add("vjs-simulive-progress");
                player.controlBar.progressControl.disable();
            });
            Array.from(document.getElementsByClassName("vjs-load-progress")).forEach(function (item) {
                item.style.display = 'none';
            });
            Array.from(document.getElementsByClassName("vjs-mouse-display")).forEach(function (item) {
                item.style.display = 'none';
            });
            Array.from(document.getElementsByClassName("vjs-play-progress")).forEach(function (item) {
                item.style.display = 'none';
            });
            var indicator = document.getElementsByClassName('vjs-seek-to-live-control')[0];

            if (indicator) {
                var clone = indicator.cloneNode(true);
                indicator.parentNode.replaceChild(clone, indicator);

                clone.onclick = function () {
                    player.play();
                };
            }
        };

        var setupCounters = function setupCounters() {
            var curr_time = new Date(Date.now());
            var stream_starttime_date = new Date(player.mediainfo.custom_fields[options.customFieldName]);
            var content_endtime_ms = stream_starttime_date.getTime() + player.mediainfo.duration * 1000;
            var content_endtime = new Date(content_endtime_ms);

            var showOverlay = function showOverlay() {
                var currentTime = new Date(Date.now());

                if (currentTime > stream_starttime_date && currentTime < stream_starttime_date.getTime() + player.mediainfo.duration * 1000) {
                    var seek_time_s = (currentTime.getTime() - stream_starttime_date.getTime()) / 1000;
                    player.currentTime(seek_time_s);
                }
            };

            if (curr_time < stream_starttime_date) {
                var modal_content = '<div id="modal_content_title">' + options.labels.countdown.live + '</div> <div id="modal_content_timer"></div>';
                var contentEl = document.createElement('div');
                contentEl.innerHTML = modal_content;
                modal.content(contentEl);
                modal.open(); // Set the poster image so we don't show the first frame of video

                Array.from(document.getElementsByClassName("vjs-modal-dialog-content")).forEach(function (item) {
                    var posters = player.mediainfo.poster_sources.filter(function (p) {
                        return p.src.substr(0, 5) === 'https';
                    });

                    if (posters.length) {
                        item.style.background = 'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(' + posters[0].src + ')';
                        item.style.backgroundSize = 'cover';
                    }
                });
                startCountdown(stream_starttime_date);
            } else if (curr_time > content_endtime) {
                removeSimuliveUI();
                player.off(['canplay', 'play'], showOverlay);
                return;
            } else if (curr_time < content_endtime && curr_time > stream_starttime_date) {
                setupSimuliveUI();
                player.play();
            } else {
                videojs.log("simulive - unepected");
            }

            player.one(["canplay", "play"], showOverlay);

            if (!(curr_time > content_endtime)) {
                player.on("play", function () {
                    var curr_time_now = new Date(Date.now()),
                        stream_starttime_date = new Date(player.mediainfo.custom_fields[options.customFieldName]),
                        content_endtime_ms = stream_starttime_date.getTime() + player.mediainfo.duration * 1000,
                        content_endtime = new Date(content_endtime_ms);

                    if (!(curr_time > content_endtime) && isSimulive()) {
                        var seek_time_s = (curr_time_now.getTime() - stream_starttime_date.getTime()) / 1000;
                        player.currentTime(seek_time_s);
                    }
                });
            }

            showOverlay();
        };

        var isSimulive = function isSimulive() {
            var player_metadata = player.mediainfo;
            var stream_starttime_date = new Date(player_metadata.custom_fields[options.customFieldName]); // check if tag or start-time is available in the asset

            if (!(player_metadata.tags.includes(options.tagValue) && player_metadata.custom_fields[options.customFieldName]) || isNaN(stream_starttime_date.getTime())) {
                return false;
            } else {
                return true;
            }
        };

        player.on('firstplay', function () {
            player.currentTime(0);
        });
        player.on('loadedmetadata', function () {
            removeSimuliveUI();

            if (!isSimulive()) {
                videojs.log("simulive - tags or start-time missing/invalid, will load vod");
                return;
            } else {
                setupCounters();
            }
        });
    };
    /**
     * Function to invoke when the player has an error.
     *
     * This will attempt to reload the video once and after that does nothing
     *
     * @function onPlayerError
     * @param    {Player} player
     *           A Video.js player object.
     *
     * @param    {Object} [options={}]
     *           A plain object containing options for the plugin.
     */


    var onPlayerError = function onPlayerError(player, options) {
        var loadVideo = function loadVideo(video) {
            player.one("loadedmetadata", function () {
                var currentTime = new Date(Date.now());
                var stream_starttime_date = new Date(video.custom_fields[options.customFieldName]);

                if (currentTime > stream_starttime_date && currentTime < stream_starttime_date.getTime() + player.mediainfo.duration * 1000) {
                    var seek_time_s = (currentTime.getTime() - stream_starttime_date.getTime()) / 1000;
                    player.currentTime(seek_time_s);
                }
            });
            videojs.log('an error occurred. attempting to reload video');
            player.catalog.load(video);
            player.attemptedReload = true;
        };

        if (player.attemptedReload) {
            return;
        }

        if (player.mediainfo.sources.length === 0) {
            player.catalog.getVideo(player.mediainfo.id, function (err, video) {
                if (video) {
                    loadVideo(video);
                }
            });
        } else {
            loadVideo(player.mediainfo);
        }
    };
    /**
     * A video.js plugin.
     *
     * In the plugin function, the value of `this` is a video.js `Player`
     * instance. You cannot rely on the player being in a "ready" state here,
     * depending on how the plugin is invoked. This may or may not be important
     * to you; if not, remove the wait for "ready"!
     *
     * @function simulivePlugin
     * @param    {Object} [options={}]
     *           An object of options left to the plugin author to define.
     */


    var simulivePlugin = function simulivePlugin(options) {
        var _this = this;

        this.ready(function () {
            onPlayerReady(_this, videojs.mergeOptions(defaults, options));
        });
        this.on('error', function (err) {
            onPlayerError(_this, videojs.mergeOptions(defaults, options));
        });
    }; // Register the plugin with video.js.


    registerPlugin('simulivePlugin', simulivePlugin); // Include the version number.

    simulivePlugin.VERSION = version;

    return simulivePlugin;

})));