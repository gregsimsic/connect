import Wrapper from "../layout/wrapper";
import FilterableAgenda from "components/agenda/filterableAgenda";

export default function AdvancedAgenda({block, site, channels}) {

    let emmaParams = {}

    if (block.sessionTypes)  {
        emmaParams.filter_session_type_id = block.sessionTypes.split(',')
    }

    let eventsConfig = {}

    if (block.onDemandOnly)  {
        eventsConfig.onDemandOnly = true
    }

    return (
        <Wrapper bg="#fff" pt="20px" pb="40px">
            <FilterableAgenda
                meetingId={site.meeting.emmaMeetingId}
                craftSiteId={site.meeting.siteId}
                site={site}
                channels={channels}
                type="byDay"
                showSearch={block.showSearch}
                showFilters={block.showFilters}
                showDownloadLinks={block.showDownloadLinks}
                sessionTypeCount={block.sessionTypes.split(',').length}
                emmaParams={emmaParams}
                eventsConfig={eventsConfig}
            />
        </Wrapper>
    )
}

