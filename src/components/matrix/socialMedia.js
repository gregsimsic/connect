import {Flex, Box, Link, Heading, Container, Text, Icon, Circle, Center} from "@chakra-ui/react";
import Wrapper from "components/layout/wrapper";
import SocialIcons from "components/header/SocialIcons";
import {Facebook, Instagram, Linkedin, Twitter, Youtube} from "react-feather";

export default function SocialMedia({block}) {

    const iconColor = "color2"
    const iconSize = {base: "32px", md: "36px"}
    const iconPadding = 6
    const circleSize = {base: "24px", md: "36px"}

    // determine background
    let bg = block.backgroundColor ? block.backgroundColor : ""
    const bgImage = block.backgroundImage ? block.backgroundImage[0] : null
    bg += bgImage ? " url(" + bgImage.url + ")" : ""

    const innerCircle = {
        content: '""',
        position: "absolute",
        width: "100%",
        height: "100%",
        border: "8px solid",
        color: "color2",
        borderRadius: "100px",
        left: "0",
        top: "0",
        zIndex: "1"
    }

    return (
        <Container bg={bg}
                   backgroundSize="cover"
                   maxW="100%"
                   py="35px"
        >
            <Flex maxW={{xl: 1200}}
                  w="100%"
                  mx="auto"
                  justify="space-between"
                  direction={{base: 'column', lg: 'row'}}
                  px={{base: '20px', lg: 0}}>
                <Box w={{ base: "100%", lg: "50%"}} mb="30px" className="body" >
                    <Box maxW="900px" dangerouslySetInnerHTML={{__html: block.body}}/>
                </Box>
                <Center w={{ base: "100%", lg: "50%"}} position="relative">

                    <Flex mr="15px">
                        <Link href="https://twitter.com/CRFHeart" target="_blank"
                              mr={5}
                              p={iconPadding}
                              bg="color1"
                              borderRadius={50}
                              border="4px solid"
                              color="color4"
                              pos="relative"
                              _after={innerCircle}
                        >
                            <Circle size={circleSize} bg="color1" >
                                <Icon as={Twitter} w={iconSize} h={iconSize} fill={iconColor} stroke="none"/>
                            </Circle>
                        </Link>
                        <Link href="https://www.facebook.com/CRFheart/" target="_blank"
                              mr={5}
                              p={iconPadding}
                              bg="color1"
                              borderRadius={50}
                              border="4px solid"
                              color="color4"
                              pos="relative"
                              _after={innerCircle}
                        >
                            <Circle size={circleSize} bg="color1" >
                                <Icon as={Facebook} w={iconSize} h={iconSize} fill={iconColor} stroke="none"/>
                            </Circle>
                        </Link>
                        <Link href="https://www.instagram.com/crfheart/" target="_blank"
                              mr={5}
                              p={iconPadding}
                              bg="color1"
                              borderRadius={50}
                              border="4px solid"
                              color="color4"
                              pos="relative"
                              _after={innerCircle}
                        >
                            <Circle size={circleSize} bg="color1" >
                                <Icon as={Instagram} w={iconSize} h={iconSize} stroke={iconColor}/>
                            </Circle>
                        </Link>
                        <Link href="https://www.linkedin.com/company/cardiovascular-research-foundation" target="_blank"
                              p={iconPadding}
                              bg="color1"
                              borderRadius={50}
                              border="4px solid"
                              color="color4"
                              pos="relative"
                              _after={innerCircle}
                        >
                            <Circle size={circleSize} bg="color1" >
                                <Icon as={Linkedin} w={iconSize} h={iconSize} fill={iconColor} stroke="none"/>
                            </Circle>
                        </Link>
                    </Flex>

                </Center>
            </Flex>
        </Container>
    )
}
