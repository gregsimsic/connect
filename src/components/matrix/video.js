import {Flex, Box, Link, Heading, Container} from "@chakra-ui/react";
import React from "react";

export default function Video({block}) {

    // const ConditionalWrapper = ({ condition, wrapper, children }) =>
    //     condition ? wrapper(children) : children;

    // <ConditionalWrapper
    //     condition={link}
    //     wrapper={children => <a href={link}>{children}</a>}
    // >

    // console.log(block)

    const slide = block.slides[0];

    const textBlock = slide.label
        ? <Box
            bg="color1"
            color="color2"
            p={{ base: "15px 20px", lg: "25px 50px" }}
            position="absolute"
            bottom={{ base: "5px", lg: "50px" }}
            w={{ base: "90%", lg: "unset" }}
            left="50%"
            transform="translateX(-50%)"
            textAlign="center"
        >
            <Link href={slide.linkUrl ? slide.linkUrl : null}>
                <Heading as="h3" fontSize={20}>{slide.label}</Heading>
            </Link>
        </Box>
        : null

    return (
        <Box position="relative" >
            <video autoPlay muted playsInline loop src={slide.videoUrl} ></video>
            {textBlock}
        </Box>
    )
}
