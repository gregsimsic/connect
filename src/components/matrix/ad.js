import {Box, Flex, Text} from "@chakra-ui/react";

import Wrapper from "../layout/wrapper";
import AdBlock from "components/ads/adBlock";

export default function Ad({block}) {

    // format sizes as an array of arrays, as Google likes 'em
    const sizesArray = block.sizes.map(size => {
        return [size.width, size.height]
    })

    return (
        <Wrapper bg="#eee" >

            <Flex justify="center" align="center" py="20px" direction="column">
                <Text color="#333" fontSize={11} mb={1}>Advertisement</Text>
                <AdBlock
                    adUnit={block.adUnit}
                    sizes={sizesArray}
                />
            </Flex>

        </Wrapper>
    )
}

