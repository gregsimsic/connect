import React, {useState} from 'react';

import { Plus, Minus } from "react-feather";
import {Box, Heading} from "@chakra-ui/react";

export default function Faqs({block}) {

    const initState = block.accordion.map( () => false )

    const [accordions, setAccordions] = useState( initState );

    const toggle = (key) => {
        setAccordions({...accordions, [key]: !accordions[key]})
    }

    return block.accordion.map((section, j) => {
        return (
            <Box key={j} >
                <Box
                    cursor="pointer"
                    color="color1"
                    onClick={() => toggle(j)}
                >
                    <Heading as="h4" size="lg" display="inline">{section.label}</Heading>
                    { accordions[j] ? <Minus/> : <Plus/> }
                </Box>
                { accordions[j]
                    ? <Box dangerouslySetInnerHTML={{__html: section.body}}/>
                    : ''
                }
            </Box>
        )
    })
}
