import {useEffect, useState} from "react";

import {useConferenceEvent} from "services/emma/emma";

import { Flex, Box, Link, Image, Text, Heading, useTheme } from "@chakra-ui/react";

import Wrapper from "../layout/wrapper";
import LoadingIndicator from "../ui/loadingIndicator";
import Session from "components/agenda-basic/session";

export default function AgendaSession({block, meetingId, channels}) {

    // get event from Emma
    const { status, data, error, isFetching } = useConferenceEvent(
        meetingId,
        block.eventId
    );

    const [channelSlug, setChannelSlug] = useState()
    const [ready, setReady] = useState(false)

    useEffect(() => {

        if (!data) return false

        const channel = channels.find(channel => parseInt(channel.sessionTypeID, 10) === data.session_type_id)
        const channelSlug = channel ? channel.slug : null

        setChannelSlug(channelSlug)
        setReady(true)

    }, [data])

    return (
        <Wrapper>
            {ready ? (
                <Box w="100%" m="0 auto">
                    {block.showSessionTitle && (
                        <Heading as="h3" textStyle="homeHeading">{data.title}</Heading>
                    )}
                    <Session {...data} showSectionTitles={false} channelSlug={channelSlug}/>
                </Box>
            ) : (
                <LoadingIndicator />
            )}
        </Wrapper>

    )
}

