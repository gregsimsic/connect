import React, {useState} from 'react';

import { Plus, Minus } from "react-feather";
import {Box, Flex, Heading, Text, Icon} from "@chakra-ui/react";
import Wrapper from "../layout/wrapper";

export default function Accordion({block}) {

    const initState = block.accordion.map( () => false )

    const [accordions, setAccordions] = useState( initState );

    const toggle = (key) => {
        setAccordions({...accordions, [key]: !accordions[key]})
    }

    return (
        <Wrapper>
            <Box w="100%" px={{base: "20px", lg: 0}} my="10px" borderBottom="1px solid">
                {block.accordion.map((section, j) => {
                    return (
                        <Box key={j} borderTop="1px solid" py="15px">
                            <Flex
                                cursor="pointer"
                                onClick={() => toggle(j)}

                                align="center"
                                justify="space-between"
                            >
                                <Heading
                                    as="h4"
                                    size="sm"
                                    color="#fff"
                                    mt={0}
                                    display="inline"
                                >
                                    {section.label}
                                </Heading>
                                { accordions[j] ? <Icon as={Minus} w="22px" h="22px"/> : <Icon as={Plus} w="22px" h="22px"/> }
                            </Flex>
                            { accordions[j]
                                ? <Box p="20px 15px 20px 0" className="body" dangerouslySetInnerHTML={{__html: section.body}}/>
                                : ''
                            }
                        </Box>
                    )
                })}
            </Box>
        </Wrapper>
    )
}
