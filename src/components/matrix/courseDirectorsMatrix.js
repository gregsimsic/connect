import Wrapper from "../layout/wrapper";
import CourseDirectors from "components/faculty/courseDirectors";
import {useConferenceModeratorRoles} from "services/emma/emma";

import {Heading} from "@chakra-ui/react";

export default function CourseDirectorsMatrix({block, meetingId}) {

    // determine background
    let bg = block.backgroundColor ? block.backgroundColor : ""
    const bgImage = block.backgroundImage ? block.backgroundImage[0] : null
    bg += bgImage ? " url(" + bgImage.url + ")" : ""

    const { status: moderatorRolesStatus, data: moderatorRolesData } = useConferenceModeratorRoles(meetingId);

    return (
        <Wrapper
            bg={bg}
            p="50px 0"
        >
            {block.head && (
                <Heading as="h3" mt={0} mb="20px" ml={{base: "40px", xxl: "-10px"}} color="color4">{block.head}</Heading>
            )}
            {moderatorRolesData ? (
                <CourseDirectors meetingId={meetingId} moderatorRoles={moderatorRolesData} highlightColor={block.highlightColor || "#fff"}/>
            ) : ( null )}
        </Wrapper>

    )
}
