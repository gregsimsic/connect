import {Box} from "@chakra-ui/react"
import Wrapper from "../layout/wrapper";

export default function Body({block, colorMode, padTop, padBottom, margin = null}) {

    // determine background
    let bg = block.backgroundColor ? block.backgroundColor : ""
    const bgImage = block.backgroundImage ? block.backgroundImage[0] : null
    bg += bgImage ? " url(" + bgImage.url + ")" : ""

    return (
        <Wrapper
            className={"body colorMode--" + colorMode}
            bg={bg}
            bgPosition="bottom"
            py="30px"
        >
            <Box
                m={margin}
                dangerouslySetInnerHTML={{__html: block.body}}
            />
        </Wrapper>
    )
}
