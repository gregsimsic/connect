import React, {useState} from 'react';

import { Plus, Minus } from "react-feather";
import {Box, Flex, Heading, Text, Icon, Accordion, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon} from "@chakra-ui/react";
import Wrapper from "../layout/wrapper";

export default function AccordionComponent({block}) {

    const buttonStyle = block.style === 'largeHead'
        ? {
            color: "color4",
            fontWeight: 600,
            fontSize: "20px"
        }
        : {
            color: "#fff",
            fontWeight: 500
        }

    return (
        <Wrapper>
            <Accordion allowToggle>
                {block.accordion.map((section, j) => {
                    return (
                        <AccordionItem key={j} borderColor="#666">
                            <Heading as="h4" mt={0} color="white">
                                <AccordionButton p="15px 0" _focus={{outline: 0}} >
                                    <AccordionIcon color="color1"/>
                                    <Box flex="1" textAlign="left" pl={1} {...buttonStyle}>
                                        {section.label}
                                    </Box>
                                </AccordionButton>
                            </Heading>
                            <AccordionPanel pb={4} pl={0}>
                                <Box p="5px 15px 20px 0" className="body" dangerouslySetInnerHTML={{__html: section.body}}/>
                            </AccordionPanel>
                        </AccordionItem>
                    )
                })}
            </Accordion>
        </Wrapper>
    )
}
