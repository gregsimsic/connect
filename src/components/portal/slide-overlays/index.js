import {Box, Center, Circle, Text, Icon} from "@chakra-ui/react";
import {Play, Lock, Clock, PlayCircle, Rss} from "react-feather";

const textLabelStyle = {
    pos: "absolute",
    bottom: "15px",
    left: "15px",
    fontSize: 18,
    fontWeight: 600,
    lineHeight: 1,
    m: 0
}

export function VideoReadyOriginal() {
    return (
        <Center h="100%">
            <Circle size="60px" bg="#aaa" >
                <Play style={{width: "30px", height: "30px", transform: "translateX(3px)", fill: "#333", stroke: "#333"}}/>
            </Circle>
        </Center>
    )
}

export function VideoReady() {
    return (
        <Center h="100%">
            <Circle size="60px" bg="#aaa" >
                <Icon as={Play} w="37px" h="37px" transform="translateX(3px)"/>
            </Circle>
        </Center>
    )
}

export function VideoNotReady() {
    return (
        <Center h="100%">
            <Icon as={Lock} w="45px" h="45px"  />
            <Text {...textLabelStyle} >On Demand Coming Soon</Text>
        </Center>
    )
}

export function NowPlaying() {
    return (
        <Box p={3}>
            <Center
                pos="absolute"
                left={0}
                top={0}
                w="100%"
                h="100%"
            >
                <Icon as={Rss} w="45px" h="45px"  />
            </Center>
            <Text {...textLabelStyle}>Now Playing</Text>
        </Box>
    )
}

export function ComingUpNext() {
    return (
        <Box p={3}>
            <Center
                pos="absolute"
                left={0}
                top={0}
                w="100%"
                h="100%"
            >
                <Icon as={Clock} w="45px" h="45px"  />
            </Center>
            <Text {...textLabelStyle}>Coming Soon</Text>
        </Box>
    )
}

export function ComingUp() {
    return (
        <Box p={3}>
            <Center
                pos="absolute"
                left={0}
                top={0}
                w="100%"
                h="100%"
            >
                <Icon as={Clock} w="45px" h="45px"  />
            </Center>
            <Text {...textLabelStyle}>Coming Soon</Text>
        </Box>
    )
}