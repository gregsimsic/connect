import {Flex, Box, Stack, Heading, Image, Link} from "@chakra-ui/react";
import LoadingIndicator from "../ui/loadingIndicator";

export default function ChannelListSideScroller(props) {

    const { events, currentEvent, selectEvent } = props

    return (
        <>
            {events && events.length ? (
                <Stack
                    h={{ base: 'none', md: 428}}
                    overflowY={{ base: 'none', md: 'scroll'}}
                    mt={{ base: '30px', md: 0 }}
                    spacing="25px"
                >
                    {events.map((event, i) => (
                        <Flex key={i}
                             onClick={() => selectEvent(event)}
                             cursor="pointer"
                             role="group"
                        >
                            <Box
                                pos="relative"
                                mr={3}
                                flex="0 0 37%"
                            >
                                <Image
                                    src={event.preview_image_url}
                                    fallbackSrc="https://via.placeholder.com/300x168.png?text=Image+Not+Found"
                                    alt={event.title}
                                    w="100%"
                                />
                                <Box
                                    pos="absolute"
                                    top={0}
                                    left={0}
                                    right={0}
                                    bottom={0}
                                    border="5px solid white"
                                    display="none"
                                    _groupHover={{
                                        display: "block"
                                    }}

                                />
                            </Box>
                            <Heading as="h3"
                                 fontSize={13}
                                 lineHeight={1.3}
                                 _groupHover={{
                                     textDecoration: "underline"
                                 }}
                            >
                                {event.title}
                            </Heading>

                        </Flex>
                    ))}
                </Stack>
            ) : (
                <LoadingIndicator />
            )}
        </>

    )
}
