import {useEffect} from "react"
import _ from "lodash"

import {Flex, Box, Heading, Image} from "@chakra-ui/react";

import VideoCarousel from "../carousel/videoCarousel";

import {useConferenceEvents} from 'services/emma/emma'
import LoadingIndicator from "../ui/loadingIndicator";
import {format} from "date-fns";

export default function TrackList(props) {

    const {meetingId, track, color} = props

    const { status, data, error, isFetching } = useConferenceEvents(
        meetingId,
        {
            filter_tracks: [track],
            limit: 5
        },
        {
            queryKey: track
        }
    );

    return (
        <>
            {status === "loading" ? (
                <LoadingIndicator />
            ) : status === "error" ? (
                <span>Error: {error.message}</span>
            ) : (
                <>
                    <Flex wrap="wrap" mr={6}>
                        <Heading as="h2" mb={6} color={color}>{track}</Heading>
                        {data.map((event) => (
                            <Box
                                key={event.id}
                                w="33%"
                                pr={3}
                                mb={7}
                            >
                                <Box
                                    pos="relative"
                                >
                                    <Image
                                        src={event.preview_image_url}
                                        fallbackSrc="https://via.placeholder.com/300x168.png?text=Image+Not+Found"
                                        alt={event.title}
                                        w="100%"
                                    />
                                    <Box
                                        pos="absolute"
                                        top={0}
                                        left={0}
                                        right={0}
                                        bottom={0}
                                        border="5px solid white"
                                        display="none"
                                        _groupHover={{
                                            display: "block"
                                        }}

                                    />
                                </Box>
                                <Heading
                                    as="h3"
                                    fontSize={16}
                                    lineHeight={1.3}
                                    my={3}
                                >
                                    {event.title}
                                </Heading>
                                <Box fontSize={11}>
                                    {format(new Date(event.datetime_start), 'MMMM d, Y')}
                                </Box>
                            </Box>
                        ))}
                    </Flex>
                    <Box
                        mt={6}
                    >{isFetching ? "Updating..." : " "}</Box>
                </>
            )}
        </>

    )
}
