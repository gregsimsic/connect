import React, {useEffect, useState} from "react";

import { Flex, Box } from "@chakra-ui/react";
import VideoCarousel from "../carousel/videoCarousel";

import PlaylistSide from 'components/portal/playlistSlide'

import _ from "lodash"
import {useConferenceEvents} from "services/emma/emma";
import LoadingIndicator from "../ui/loadingIndicator";

export default function TrackCarousel(props) {

    const {meetingId, channel, startEvent} = props

    const [slides, setSlides] = useState([]);

    function selectSlide(e) {
        console.log(e)
    }

    const { status, data, error, isFetching } = useConferenceEvents(
        meetingId,
        {filter_session_type_id: channel.sessionTypeID},
        {}
    );

    useEffect(async () => {

        const slides = videosArray.map((video, i) => {
            return (<PlaylistSide slide={video} selectSlide={selectSlide} key={i}/>)
        })

        setSlides(slides);

    }, [data]);

    return (
        <>
            {slides.length
                ? <VideoCarousel slides={slides} />
                : <LoadingIndicator />
            }
        </>

    )
}
