import {Flex, Box, Stack, Heading, Text, Image, Link, SimpleGrid} from "@chakra-ui/react";
import LoadingIndicator from "../ui/loadingIndicator";
import FormattedDateString from "components/agenda/formattedDateString";

export default function EventList({ events, channel }) {

    const defaultThumb = channel.eventListThumbnail[0] ? channel.eventListThumbnail[0].url : "/channels/event-slide-fallback-grey.jpg"

    return (
        <>
            {events && events.length ? (
                <SimpleGrid minChildWidth="260px" spacingX="15px" spacingY="20px" mt="40px" color="#333">
                    {events.map((event, i) => (
                        <Box key={i}
                             role="group"
                        >
                            <Box
                                pos="relative"
                                flex="0 0 37%"
                            >
                                <Image
                                    alt={event.title}
                                    w="100%"
                                    src={event.preview_image_url || defaultThumb}
                                    fallbackSrc="/channels/event-slide-fallback-grey.jpg"
                                />
                                {/*<Box*/}
                                {/*    pos="absolute"*/}
                                {/*    top={0}*/}
                                {/*    left={0}*/}
                                {/*    right={0}*/}
                                {/*    bottom={0}*/}
                                {/*    border="5px solid white"*/}
                                {/*    display="none"*/}
                                {/*    _groupHover={{*/}
                                {/*        display: "block"*/}
                                {/*    }}*/}

                                {/*/>*/}
                            </Box>
                            <Heading as="h3"
                                 fontSize={14}
                                 lineHeight={1.3}
                                 mt={3}
                            >
                                {event.title}
                            </Heading>
                            {!event.params.on_demand && (
                                <Text
                                    fontSize={12}
                                    mt={0}
                                >
                                    <FormattedDateString event={event} config={{format: "MMMM d, Y — h:mm a zzz"}}/>
                                </Text>
                            )}

                        </Box>
                    ))}
                </SimpleGrid>
            ) : (
                <LoadingIndicator />
            )}
        </>

    )
}
