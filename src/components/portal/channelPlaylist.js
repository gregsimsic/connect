import {Flex, Box, Stack, Heading, Image, Link, Text, Center, Circle, SimpleGrid} from "@chakra-ui/react";
import LoadingIndicator from "../ui/loadingIndicator";
import PlaylistSlide from "components/portal/playlistSlide";

export default function ChannelPlaylist(props) {

    const { channel, channelIsReady, events, currentEvent, selectEvent, colorMode = "light" } = props

    const defaultThumb = channel.eventListThumbnail[0] ? channel.eventListThumbnail[0].url : "/channels/event-slide-fallback-grey.jpg"

    const headStyles = {
        medium: { fontSize: "20px", mb: 0, mt: 8 },
        large: { fontSize: "4xl", mb: 3 }
    }

    return (
        <>
            <Heading as="h2" {...headStyles[props.style]} color={channel.color1}>{props.label}</Heading>
            {events && events.length ? (
                <SimpleGrid
                    spacingX="15px"
                    spacingY="20px"
                    mt={props.style === 'large' ? '40px' : '20px'}
                    gridTemplateColumns={
                        {
                            base: "repeat(auto-fit, minmax(260px, 1fr))",
                            md: "repeat(auto-fit, minmax(260px, 260px))"
                        }
                    }
                >
                    {events.map((event, i) => (
                        <PlaylistSlide
                            key={event.id}
                            event={event}
                            isSimulive={channel.isSimulive}
                            selectEvent={selectEvent}
                            colorMode={colorMode}
                            margin="0"
                            defaultThumb={defaultThumb}
                            isCurrentEvent={channelIsReady && currentEvent && currentEvent.id === event.id}
                        />
                    ))}
                </SimpleGrid>
            ) : (
                <LoadingIndicator color="#fff"/>
            )}
        </>

    )
}
