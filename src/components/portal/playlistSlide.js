import {Flex, Box, Stack, Heading, Image, Link, Text, Center, Circle} from "@chakra-ui/react";
import FormattedDateString from "components/agenda/formattedDateString";
import {Play} from "react-feather";
import { isAfter } from "date-fns"
import {useGlobalState} from "../../providers/globalState";

export default function PlaylistSlide(props) {

    const { event, link = false, isCurrentEvent, isSimulive, selectEvent, defaultThumb, colorMode = "light" } = props

    const {site} = useGlobalState()

    // event has a BC link or a URL (link) prop has been passed in AND the meeting is live
    const hasReadyVideo = (event.brightcove_id || link) && (site.meeting.isLive || event.params.on_demand)

    const selectEventCursor = (hasReadyVideo && selectEvent) ? "pointer" : "default"
    const styles = {
        dark: {
            textColor: "#fff"
        },
        light: {
            textColor: "#555"
        },
    }

    function conditionalSelectEvent(event) {
        if (hasReadyVideo && selectEvent) selectEvent(event)
    }

    return (
        <Box
             role="group"
             pos="relative"
             m={props.margin || "0 5px"}
             cursor={selectEventCursor}
             onClick={() => conditionalSelectEvent(event)}
        >
            <Box
                pos="relative"
                flex="0 0 37%"
            >
                <Image
                    w="100%"
                    src={event.preview_image_url || defaultThumb}
                    fallbackSrc="/channels/event-slide-fallback-grey.jpg"
                    alt={event.title}
                />
                {event.overlay && (
                    <Box
                        pos="absolute"
                        left={0}
                        top={0}
                        w="100%"
                        h="100%"
                    >
                        {event.overlay}
                    </Box>
                )}
            </Box>
            <Box color={styles[colorMode].textColor} >
                <Heading as="h3"
                         fontSize={14}
                         lineHeight={1.3}
                         mt={3}
                         color={styles[colorMode].textColor}
                >
                    {event.title}
                </Heading>
                {event.description && (
                    <Text
                        fontSize={13}
                        lineHeight={1.1}
                        mt={1}
                        mb={2}
                    >
                        {event.description}
                    </Text>
                )}
                <Flex justify="space-between">
                    {event.params.on_demand ? (
                        <Text as="span" fontSize={12} mt={0}>{event.moderators[0].role.name + ": " + event.moderators[0].users[0].full_name}</Text>
                    ) : (
                        <Text as="span" fontSize={12} mt={0}>
                            <FormattedDateString event={event} config={{format: "MMMM d, Y — h:mm a zzz"}}/>
                        </Text>
                    )}
                    <Text as="span" fontSize={12} mt={0}>{event.duration} min.</Text>
                </Flex>
                {isCurrentEvent && (
                    <Text color="red" my={0} fontWeight={600} textTransform="uppercase" fontSize={13}>Now Playing</Text>
                )}
            </Box>
            {link && !selectEvent && hasReadyVideo && (
                <Link
                    href={link}
                    alt={event.title}
                    _hover={{textDecoration: "none"}}
                    pos="absolute"
                    left={0}
                    top={0}
                    w="100%"
                    h="100%"
                    display="block"
                />
            )}
        </Box>
    )
}
