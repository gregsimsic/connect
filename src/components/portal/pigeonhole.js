import {Box} from "@chakra-ui/react";

export default function Pigeonhole({passcode, sessionId, user}) {

    passcode = (user) ? passcode + "-" + user.oktaId : passcode
    const userParams = (user) ? "&phName=" + user.name + "&phEmail=" + user.email : ""
    const pigeonHoleIframeSrc = "https://pigeonhole.at/" + passcode + "/i/" + sessionId + "?disablebackbutton" + userParams

    return (
        <Box h="100%">
            <Box as="iframe"
                 src={pigeonHoleIframeSrc}
                 h="500px" w="100%"
                 border="1px solid #eee"
                 borderBottom="2px solid"
                 borderBottomColor="color1"
            />
        </Box>
    )
}
