import {useState, useEffect, useRef} from "react"
import {format, differenceInSeconds, add} from "date-fns"

import {Box, Text, Input, InputGroup} from "@chakra-ui/react"

export default function MockClock({mockTime, setMockTime}) {

    const baseTime = mockTime || new Date()

    const [elapsedSeconds, setElapsedSeconds] = useState(0);
    const [clockTime, setClockTime] = useState(baseTime)
    const [isUpdating, setIsUpdating] = useState(false)

    const inputTimeEl = useRef(null);

    // every 1 second set the number of seconds elapsed since the start
    // using this technique to account for any variations within the client's clock
    useEffect(() => {
        const interval = setInterval(() => {
            // setElapsedSeconds(Math.abs(differenceInSeconds(startTime, new Date())));
            setElapsedSeconds( eS => eS + 1 );
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    // add the elapsed seconds to the clock
    useEffect(() => {
        if (!isUpdating) {
            const t = add(baseTime, {seconds: elapsedSeconds})
            setClockTime(t)
        }
    }, [elapsedSeconds])

    // set the input value to the current clock value
    useEffect(() => {
        if (isUpdating) {
            inputTimeEl.current.value = format(clockTime,"yyyy-M-d HH:mm:ss")
        }
    }, [isUpdating])

    function setTime(e) {

        e.preventDefault()
        e.stopPropagation()

        if (e.key === 'Enter' || e.keyCode === 13) {
            setElapsedSeconds(0)
            setIsUpdating(false)
            setMockTime(new Date(e.target.value))
        }

    }

    function prepInput(e) {
        setIsUpdating(true)
    }

    useEffect(() => {
        if (isUpdating) {
            inputTimeEl.current.focus()
        }
    }, [inputTimeEl.current])

    return (
        <Box color="color2">
            {!isUpdating ? (
                <Text mt={2} onClick={() => prepInput()}>
                    Local time: {format(clockTime, "MMM d, yyyy — HH:mm:ss a")}
                </Text>
            ) : (
                <InputGroup size="md">
                    <Input
                        ref={inputTimeEl}
                        pr="4.5rem"
                        onKeyUp={setTime}
                    />
                </InputGroup>
            )}
        </Box>
    )
}