import {Center, Text} from "@chakra-ui/react";

export default function ChannelNotReady({channel}) {

    return (
        <Center p="80px 0 100px" textAlign="center">
            <Text
                fontSize={22}
                fontWeight={600}
                m={0}
            >
                On-demand content coming soon. Please check back.
            </Text>
        </Center>
    )

}