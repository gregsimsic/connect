import { Box } from "@chakra-ui/react";

export default function Chat({channelId}) {

    return (
        <Box
            bg="gray.400"
            p={10}
        >
            Pigeonhole Chat
        </Box>
    )
}
