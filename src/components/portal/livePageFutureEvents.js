import {Flex, Box, Stack, Heading, Text, Image, Link, SimpleGrid} from "@chakra-ui/react";
import LoadingIndicator from "../ui/loadingIndicator";
import Session from "components/agenda/session";

export default function LivePageFutureEvents({ events, channel }) {

    // const defaultThumb = channel.eventListThumbnail[0] ? channel.eventListThumbnail[0].url : "/channels/event-slide-fallback-grey.jpg"

    return (
        <>
            {events && events.length ? (
                // <SimpleGrid minChildWidth="260px" spacingX="15px" spacingY="20px" mt="40px" color="#333">
                    events.map((event, i) => (
                        <Box key={i}
                             role="group"
                        >
                            <Heading as="h3"
                                     color="color2"
                                 fontSize={24}
                                 lineHeight={1.3}
                                 mt={5}
                            >
                                {event.title}
                            </Heading>
                            <Session {...event} />

                        </Box>
                    ))
                // </SimpleGrid>
            ) : (
                <LoadingIndicator />
            )}
        </>

    )
}
