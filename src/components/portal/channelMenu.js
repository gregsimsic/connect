import {useEffect, useState} from "react";
import _ from "lodash";

import {
    Stack, Box, Center, HStack,
    Text, Heading,
    Menu, MenuButton, MenuList, MenuItem, Button,
    Circle,
    useBreakpointValue, DarkMode
} from "@chakra-ui/react";
import {ChevronDown} from "react-feather";
import Wrapper from "components/layout/wrapper";
import Layout from "components/layout/layout";

import {useGlobalState} from 'providers/globalState'

export default function ChannelMenu({channels, currentChannel, context, variant}) {

    const {site} = useGlobalState()

    // TODO: move this to a more global location. In CMS or Next config?
    const channelModeLabels = {
        ondemand: "On Demand",
        live: "Live"
    }

    const isMobile = useBreakpointValue({base: true, md: false})

    // always display dropdowns in mobile view
    variant = isMobile ? 'dropdowns' : variant

    let allChannels = []

    channels.map(channel => channel.channelMode = "ondemand")

    const liveChannels = _.cloneDeep(channels).filter(channel => channel.currentlyAvailableVersions.includes('live'))

    liveChannels.map(channel => channel.channelMode = "live")

    const onDemandChannels = channels.filter(channel => channel.currentlyAvailableVersions.includes('ondemand'))

    allChannels = liveChannels.concat(onDemandChannels)

    allChannels.map(channel => {
        channel.isCurrent = (currentChannel && channel.id === currentChannel.id && channel.channelMode === context)
        channel.fullTitle = channel.htmlTitle + "<br/><span style='font-weight: 600;'>" + channelModeLabels[channel.channelMode] + "</span>"
    })

    const groupedChannels = []

    liveChannels.length && groupedChannels.push({
        label: "Live",
        slug: 'live',
        channels: liveChannels
    })

    onDemandChannels.length && groupedChannels.push({
        label: "On Demand",
        slug: 'ondemand',
        channels: onDemandChannels
    })

    const accentColor = currentChannel?.color1 || "#333"

    return variant === "dropdowns" ? (
        <Box borderTop={`1px solid ${accentColor}`}>
            <Wrapper pt={4}>
                <DarkMode>
                    {context === 'channels' && (
                        <Text mt={4} mb={1} fontWeight={600} fontSize="18px" color="#333" textAlign={{base: 'left', md: 'center'}} maxW="100%">Select a {site.meeting.sessionTypeTerm || "Channel"}:</Text>
                    )}
                    <HStack justify={!isMobile && context === 'channels' ? 'center' : null}>
                        {groupedChannels.map(group => (
                            <Menu arrowPadding={8}>
                                <MenuButton
                                    as={Button}
                                    fontWeight={600}
                                    border={0}
                                    bgColor={group.slug === 'live' ? "color2" : "color7"}
                                    color="#fff"
                                    borderRadius={1}
                                    pl={5} pr={2} py={6}
                                    _hover={{bg: "color5", color: "#fff"}}
                                    _expanded={{bg: "color5", color: "#fff"}}
                                    rightIcon={<ChevronDown />}
                                >
                                    {group.label}
                                </MenuButton>
                                <MenuList borderRadius={1} bg="color5" border="0">
                                    {group.channels.map(channel => (
                                        <MenuItem
                                            as="a"
                                            key={channel.id}
                                            href={`/${group.slug}/${channel.slug}`}
                                            color={channel.isCurrent ? channel.color1 : 'inherit'}
                                        >
                                            <Circle
                                                size="12px"
                                                borderWidth="2px"
                                                borderColor={channel.color1}
                                                bgColor={channel.isCurrent ? channel.color1 : 'transparent'}
                                                mr={3}
                                            />
                                            {channel.title}
                                        </MenuItem>
                                    ))}
                                </MenuList>
                            </Menu>
                        ))}
                    </HStack>
                </DarkMode>
            </Wrapper>
        </Box>
        ) : (
            <HStack spacing="4px">
                {allChannels.map(channel => {
                    const opacity = (!currentChannel || channel.isCurrent) ? 1 : 0.5
                    return (
                        <Box
                            key={channel.id + channel.channelMode}
                            w={["100%", 100 / allChannels.length + "%"]}
                            h="70px"
                        >
                            <Center as="a"
                                    h="100%"
                                    px="10px"
                                    bg={channel.channelMode === "live" ? "color2" : "color7"}
                                    textTransform="uppercase" textAlign="center" fontSize={13}
                                    href={'/' + channel.channelMode + '/' + channel.slug}
                                    role="group"
                            >
                                <Text
                                    fontSize="12px"
                                    lineHeight={1.2}
                                    mt={0}
                                    opacity={opacity}
                                    transition="opacity 200ms"
                                    dangerouslySetInnerHTML={{__html: channel.fullTitle}}
                                    _groupHover={{opacity: 1}}
                                />
                            </Center>
                        </Box>
                    )
                })}
            </HStack>
        )
}
