import { Box } from "@chakra-ui/react";
import VideoCarousel from "../carousel/videoCarousel";

import PlaylistSlide from "components/portal/playlistSlide";
import LoadingIndicator from "../ui/loadingIndicator";
import {useRouter} from "next/router";

export default function ChannelCarousel(props) {

    const {channel, events, selectEvent, maxSlideCount = 4, colorMode = "light"} = props

    const router = useRouter()

    const defaultThumb = channel.eventListThumbnail[0] ? channel.eventListThumbnail[0].url : "/channels/event-slide-fallback-grey.jpg"

    function getLink(event) {
        const videoIsAvailable = event.links?.length
        return videoIsAvailable ? "/ondemand/" + channel.slug + "/" + event.id : ""
    }

    const slides = events.map((event, i) => {
        return (
            <PlaylistSlide
                key={event.id}
                event={event}
                isSimulive={channel.isSimulive}
                selectEvent={selectEvent}
                colorMode={colorMode}
                defaultThumb={defaultThumb}
                link={getLink(event)}
            />
        )
    })

    return (
        <>
            {slides.length
                ? <VideoCarousel slides={slides} maxSlideCount={maxSlideCount} colorMode={colorMode}/>
                : <LoadingIndicator />
            }
        </>

    )
}
