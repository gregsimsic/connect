import {useState, useEffect} from "react"
import ReactPlayerLoader from "@brightcove/react-player-loader";

import { Flex, Box } from "@chakra-ui/react";
import LoadingIndicator from "../../ui/loadingIndicator";
import _ from "lodash";
import {isBefore} from "date-fns";

const brightcoveAccountId = process.env.NEXT_PUBLIC_BC_ACCOUNT_ID

export default function FakeLivePlayer(props) {

    // TODO: this is really a channel player -- plays a list of events

    const {events, currentEvent, seek} = props

    const [player, setPlayer] = useState()

    const [currentEventIndex, setCurrentEventIndex] = useState(0)

    const [currentVideoId, setCurrentVideoId] = useState(0)

    function onSuccess(e) {
        setPlayer(e.ref)
    }

    function onFailure(e) {
        console.log('fake live video player failed to load')
    }

    // player.playlist.autoadvance -- TVT uses a playlist set to auto-advance & the sidebar list listens to the 'loadstart' event
    useEffect(() => {
        // listen to player events
        if (player) {

            setCurrentVideoId(events[currentEventIndex].brightcove_id)

            const timeInSeconds = seek || 0;
            player.currentTime(timeInSeconds)

            // TODO: first remove old listeners ? This re-adds listeners after the videoId changes
            player.on('loadstart', (e) => {
                // console.log(o)
            })
            // Bingo: this event works
            player.on('ended', () => {
                if (currentEventIndex < events.length) {
                    setCurrentEventIndex(currentEventIndex + 1)
                }
            })
        }

    }, [player, currentEventIndex])




    function seekToTime(timeInSeconds) {
        player.currentTime(timeInSeconds)
    }

    function getCurrentEventIndex(event) {
        return _.findIndex(events, evt => {
            return event.id === evt.id
        })
    }

    return (
        <>
        {events && events.length ? (

            <ReactPlayerLoader
                accountId={brightcoveAccountId}
                playerId="qFVRi9aPDE"
                videoId={currentVideoId}
                onSuccess={onSuccess}
                onFailure={onFailure}
            />
        ) : (
            <LoadingIndicator />
        )}
        </>
    )
}
