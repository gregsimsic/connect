import {useState, useEffect} from "react"
import ReactPlayerLoader from "@brightcove/react-player-loader";

import { Flex, Box } from "@chakra-ui/react";
import LoadingIndicator from "../../ui/loadingIndicator";

const brightcoveAccountId = process.env.NEXT_PUBLIC_BC_ACCOUNT_ID

export default function VideoPlayer(props) {

    const {playerId, videoId, seek, onVideoEnd, autoplay, user} = props

    const [player, setPlayer] = useState()

    function onEmbedCreated(element) {
        if (user) {
            element.setAttribute('data-bc-email', user.email)
        }
    }

    function onSuccess(e) {
        setPlayer(e.ref)
    }

    function onFailure(e) {
        // TODO: give the user a good error here, use a Toast alert
        console.log('videoplayer failed to load')
    }

    // initialize player, re-run if props change
    useEffect(() => {

        if (player) {

            player.autoplay(autoplay)

            // if a seek prop is set, then advance the playhead
            const timeInSeconds = seek || 0;
            seekToTime(timeInSeconds)

            // other events: loadstart, ...
            if (onVideoEnd) {
                player.on('ended', () => {
                    onVideoEnd(videoId)
                })
            }
        }

    }, [player, videoId])

    useEffect(() => {
        if (player) {
            seekToTime(seek)
        }
    }, [seek])

    function seekToTime(timeInSeconds) {
        player.currentTime(timeInSeconds)
    }

    return (
        <>
            {videoId ? (
                <ReactPlayerLoader
                    accountId={brightcoveAccountId}
                    playerId={playerId}
                    videoId={videoId}
                    // playlistId="1668957681694436482"
                    onSuccess={onSuccess}
                    onFailure={onFailure}
                    onEmbedCreated={onEmbedCreated}
                />
            ) : (
                <LoadingIndicator color="#fff"/>
            )}
        </>
    )
}
