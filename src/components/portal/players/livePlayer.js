import ReactPlayerLoader from "@brightcove/react-player-loader";

const brightcoveAccountId = process.env.NEXT_PUBLIC_BC_ACCOUNT_ID

export default function LivePlayer({channelId}) {

    function onSuccess(e) {
        console.log('live player success')
        console.log(e.ref)
    }

    return (
        <ReactPlayerLoader
            accountId={brightcoveAccountId}
            playerId="qk3rEx8bs" // Test player with simulated live plugin
            // videoId={channelId}
            onSuccess={onSuccess}
        />

    )
}
