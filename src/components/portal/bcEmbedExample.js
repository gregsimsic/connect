import {Box} from "@chakra-ui/react";

export default function BCEmbedExample() {
    return (
        <Box
            pos="relative"
            maxW="960px"
        >
            <Box
                pt="56.25%"
            >
                <Box as="iframe"
                     src="///preview-players.brightcove.net/v2/accounts/5270290550001/players/qFVRi9aPDE/preview/embeds/default/master/index.html"
                     allowFullScreen=""
                     allow="encrypted-media"

                     pos="absolute"
                     top="0"
                     right={0}
                     left={0}
                     bottom={0}
                     w="100%"
                     height="100%"
                />
            </Box>
        </Box>
    )
}