import {Box, Text} from "@chakra-ui/react";
import {isSameDay} from 'date-fns'
import FormattedDateString from "components/agenda/formattedDateString";

export default function PlayerStatusMessage({prevEvent, nextEvent, getTheTime}) {

    // error check
    if (!nextEvent) return null

    let message = {}

    function setMessage() {

        const isFirstEventOfMeeting = typeof prevEvent === "undefined"
        const isFirstEventOfTheDay = !isFirstEventOfMeeting && !isSameDay(new Date(prevEvent.datetime_start), new Date(nextEvent.datetime_start))
        const nextEventIsToday = isSameDay(getTheTime(), new Date(nextEvent.datetime_start))

        // is the first event of the meeting
        if (isFirstEventOfMeeting) {
            if (nextEventIsToday) {
                message = {
                    text: "The stream will be begin at ",
                    format: "h:mm a zzz"
                }
            } else {
                message = {
                    text: "The stream will be begin ",
                    format: "MMMM d 'at' h:mm a zzz"
                }
            }
            return false
        }

        // is it the beginning of the day
        if (isFirstEventOfTheDay && nextEventIsToday) {
            message = {
                text: "The stream will be begin at ",
                format: "h:mm a zzz"
            }
            return false
        }

        // is the next event today
        if (nextEventIsToday) {
            message = {
                text: "Thank you for watching. The stream will resume at ",
                format: "h:mm a zzz"
            }
            return false
        }

        // otherwise it's the end of the day and will resume tomorrow or in the future
        message = {
            text: "Thank you for watching. The stream will resume ",
            format: "MMMM d 'at' h:mm a zzz"
        }

    }

    setMessage()

    return (
        <Box mt={{base: 8, md: 0}}>
            <Text fontWeight={600} fontSize="24px" color="color2">
                {message.text} <FormattedDateString event={nextEvent} config={{format: message.format}}/>
            </Text>
        </Box>
    )
}