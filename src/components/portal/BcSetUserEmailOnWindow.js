import {useEffect} from "react";

export default function BcSetUserEmailOnWindow({user}) {

    useEffect(() => {
        if (user) {
            const BcConnection = {
                tctmdBrightcove: {
                    samlAttributes: {
                        emailAddress: user.email
                    }
                }
            }
            window.drupalSettings = BcConnection
        }
    }, [])

    return null
}