import {Box, Flex, Heading, Text} from "@chakra-ui/react";
import ModeratorList from "../agenda-basic/moderatorList";
import FormattedDateString from "components/agenda/formattedDateString";
import LoadingIndicator from "../ui/loadingIndicator";
import _ from "lodash";

export default function PlayerEventInfoLive({event, isPlaying, color}) {

    // TODO: merge with PlayerEventInfo

    let combinedModerators = []

    if (event.children) {
        event.children.map(event => {
            event.moderators.map(mod => {

                if (mod.role.name === "Guest Discussant") return false

                // set up role
                if (!combinedModerators[mod.role.id]) {
                    combinedModerators[mod.role.id] = {
                        id: mod.role.id,
                        label: mod.role.name,
                        labelPlural: mod.role.name_plural,
                        users: mod.users
                    }
                } else {
                    combinedModerators[mod.role.id].users = combinedModerators[mod.role.id].users.concat(mod.users)
                }

            })
        })

        // de-duplicate users
        combinedModerators.map(mod => {
            mod.users = _.uniqBy(mod.users, user => user.id)
        })

    }

    const timeFormatConfig = isPlaying
        ? {showEndTime: true}
        : {format: "MMMM d — h:mm a zzz"}

    return (
        event ? (
            <Box mt={{base: 8, md: 0}}>
                {!isPlaying && (
                    <Text fontWeight={600} color={color} fontSize={17} textTransform="uppercase" pr={2} mb={5} >Coming up next</Text>
                )}
                <Text mr="30px" fontSize={13} color="#666" textTransform="uppercase">
                    <FormattedDateString event={event} config={timeFormatConfig}/>
                </Text>
                <Heading as="h2" mt={1} color={color} >{event.title}</Heading>
                {event.description ? (
                    <Text fontWeight={600} color={color} fontSize={15} textTransform="uppercase" pr={2}>{event.description}</Text>
                ) : (null) }
                <Box my={6}>
                    {event.moderators.map((moderatorGroup, i) => {
                        return (
                            <ModeratorList
                                moderators={moderatorGroup.users}
                                label={moderatorGroup.role.name}
                                labelPlural={moderatorGroup.role.name_plural}
                                key={i}
                            />
                        )
                    })}
                    {combinedModerators.length ?
                        combinedModerators.map(group => {
                                return (
                                    <ModeratorList
                                        moderators={group.users}
                                        label={group.label}
                                        labelPlural={group.labelPlural}
                                        key={group.id}
                                    />
                                )
                            }
                        ) : ( null )}
                </Box>
            </Box>
        ) : (
            <Box>
                <LoadingIndicator />
            </Box>
        )
    )
}