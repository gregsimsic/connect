import {useEffect, useState} from "react";

import { Box } from "@chakra-ui/react";

export default function Pigeonhole({pigeonholeSessionId, passcode}) {

    useEffect(() => {

            (function(p,i,g,e,o,n,s){p[o]=p[o]||function(){(p[o].q=p[o].q||[]).push(arguments)},
            n=i.createElement(g),s=i.getElementsByTagName(g)[0];n.async=1;n.src=e;
            s.parentNode.insertBefore(n,s);})
            (window,document,'script','https://static.pigeonhole.at/widget/pigeon-widget.js','phl');
            phl("create", {
            width: "320px",
            height: "568px",
            passcode: "FELLOWS",
            className: "pigeonhole-iframe",
            sessionId: 1832370,
            current: false,
            disableBackButton: true,
            profile: {
              Name: "John Doe",
              Email: "johndoe@email.com",
            }
        });

    }, [])

    return (
        <Box
            h="100%"
            borderBottom="1px solid #ccc"
        >
            <div className="pigeonhole-iframe"></div>

        </Box>
    )
}
