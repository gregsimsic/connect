import {Box, Flex, Text} from "@chakra-ui/react";
import {format, set} from "date-fns"

import EventList from './eventList'

export default function EventListGroup({title, dayStartTime, eventLists}) {

    return (
        <Box mr="40px">
            {/*<Box*/}
            {/*    fontWeight="bold"*/}
            {/*    fontSize="18px"*/}
            {/*    pb="10px"*/}
            {/*    textAlign="center"*/}
            {/*    borderBottom="2px solid"*/}
            {/*>*/}
            {/*    {title}*/}
            {/*</Box>*/}
            <Flex>
                {eventLists.map((eventList, i) => {
                    return (
                        <EventList key={i} {...eventList} dayStartTime={dayStartTime}/>
                    )
                })}
            </Flex>
        </Box>
    )

}
