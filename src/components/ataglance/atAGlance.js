import React, {useEffect, useState} from "react";

import {Box, CircularProgress, Flex, useTheme} from "@chakra-ui/react";
import _ from "lodash";

import SectionSelector from './sectionSelector'
import FormatSelector from "./formatSelector";
import EventListGroup from './eventListGroup'
import HoursList from './hoursList'
import {format, set} from "date-fns";

export default function AtAGlance({agenda}) {

    const theme = useTheme()

    const [sections, setSections] = useState([]);

    const [displayFormat, setDisplayFormat] = useState('channel');

    // TODO: make this more dynamic -- create the hours range based on events data
    const hours = _.range(8, 19)

    useEffect(async () => {

        setSections([])

        // this quick delay is only for user experience -- to make the transition between 'formats' more obvious
        // it allows the clearing of 'sections' to refresh the view before it is re-poppulated
        setTimeout(() => {

            if (displayFormat === 'day') {
                groupEventsByDayThenChannel(agenda)
            } else {
                groupEventsByChannelThenDay(agenda)
            }
        }, 100)

    }, [displayFormat]);

    function groupEventsByDayThenChannel(events) {

        const eventsGroupedByDay = _.groupBy(events, 'day')

        const daysArray = _.map(eventsGroupedByDay, events => {

            // TODO: if event is earliest or latest time, then record it

            const eventsGroupedByChannel = _.groupBy(events, 'session_type_id')

            const channelsArray = _.map(eventsGroupedByChannel, channel => {

                return {
                    session_type_id: channel[0].session_type_id,
                    title: channel[0].session_type,
                    events: channel
                }
            })

            return {
                title: format(new Date(events[0].datetime_start), 'eeee, MMMM d'),
                eventLists: channelsArray,
                dayStartTime: set(new Date(events[0].datetime_start), {hours: 7, minutes: 0})
            }
        })

        setSections(daysArray);

    }

    function groupEventsByChannelThenDay(events) {

        const eventsGroupedByChannel = _.groupBy(events, 'session_type_id')

        const channelsArray = _.map(eventsGroupedByChannel, events => {

            const eventsGroupedByDay = _.groupBy(events, 'day')

            const daysArray = _.map(eventsGroupedByDay, day => {

                return {
                    date: events[0].datetime_start,
                    title: format(new Date(day[0].datetime_start), 'eeee, MMMM d'),
                    events: day
                }

            })

            return {
                title: events[0].session_type,
                eventLists: daysArray,
                dayStartTime: set(new Date(events[0].datetime_start), {hours: 7, minutes: 0})
            }
        })

        setSections(channelsArray);

    }

    function extractHoursFromEvents(events) {

    }

    return (
        <Box>

            {/*<DisplayStateSelector states={{'byDay','byChannel'}} />*/}
            <>
                {/*<Flex>*/}
                {/*    <Box w="75px"></Box>*/}
                {/*</Flex>*/}
                <Box minH={(hours.length * 60) + 250} pos="relative">
                    <FormatSelector format={displayFormat} setFormat={setDisplayFormat}/>
                    <Box
                        pos="absolute"
                        top="166px"
                        left="-65px"
                        w="100%"
                        opacity={0.3}
                    >
                        <HoursList hours={hours}/>
                    </Box>
                    {sections.length ? <SectionSelector sections={sections}/> : null}
                    <Flex>
                        {sections.length ?
                            sections.map((section, i) => {
                                return (
                                    <EventListGroup key={i} {...section}/>
                                )
                            })
                            : <Box m="auto" width={44}><CircularProgress isIndeterminate color={theme.colors.color1} /></Box>
                        }

                    </Flex>
                </Box>
            </>

        </Box>
    )

}
