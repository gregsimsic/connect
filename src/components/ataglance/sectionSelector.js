import {Box, Flex, Icon} from "@chakra-ui/react";
import {ChevronRight, ChevronLeft} from "react-feather";

export default function SectionSelector({sections}) {

    function prev() {

    }

    function next() {

    }

    return (
        <Flex justify="space-between">
            <Box onClick={prev} cursor="pointer">
                <Icon as={ChevronLeft} w="40px" h="40px"/>
            </Box>
            <Box fontSize="30px" fontWeight="bold">{sections[0].title}</Box>
            <Box onClick={next} cursor="pointer">
                <Icon as={ChevronRight} w="40px" h="40px"/>
            </Box>
        </Flex>
    )

}
