import {Box, Flex, ButtonGroup, Button, Icon, } from "@chakra-ui/react";
import {CalendarIcon, HamburgerIcon} from "@chakra-ui/icons";
import {Menu, Calendar} from "react-feather";

export default function FormatSelector({format, setFormat}) {

    return (
        <ButtonGroup isAttached mb="25px">
            <Button onClick={() => setFormat('day')} leftIcon={<CalendarIcon />} variant="outline">
                By Day
            </Button>
            <Button onClick={() => setFormat('channel')} leftIcon={<HamburgerIcon />} variant="outline">
                By Channel
            </Button>
        </ButtonGroup>
    )

}
