import {Box, Flex, Text} from "@chakra-ui/react";
import {set, differenceInMinutes, format} from "date-fns";

export default function HoursList({hours}) {

    let date = new Date()

    return (
        <Box>
            {hours.map((hour, i) => {
                date.setHours(hour,0,0)
                return (
                    <Box key={i}
                         h="60px"
                         fontSize="16px"
                         fontWeight="bold"
                         borderTop="1px dotted"
                    >
                        {format(date, 'h')}
                        <Text as="span"
                              fontSize="10px"
                              verticalAlign="text-top"
                        > {format(date, 'aaa')}
                        </Text>
                    </Box>
                )
            })}
        </Box>
    )

}
