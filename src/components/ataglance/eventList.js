import {Box, Flex, Text, Tooltip} from "@chakra-ui/react";
import {set, differenceInMinutes, format} from "date-fns";

export default function EventList({title, events, dayStartTime}) {

    // TODO: day start time should just be time only
    function eventPosition(startTime) {

        var offsetfromDayStart = differenceInMinutes(
            new Date(dayStartTime),
            new Date(startTime)
        )

        return -offsetfromDayStart+1 + "px";
    }

    function eventHeight(duration) {
        return (duration - 1) + "px";
    }

    function formmattedDateString(props) {

        let str = format(new Date(props.datetime_start), 'h:mm a')

        str += " - " + format(new Date(props.datetime_end), 'h:mm a')

        return str
    }

    function eventData(event) {
        return "(" + event.id + ") "
            + formmattedDateString(event)
    }

    function noOfLinesToShowInEventTitle(duration) {
        return Math.floor(duration / 20)
    }

    function showDetails(event) {

    }

    return (
        <Box w="350px" mr="1px" h="800px" cursor="pointer">
            <Flex align="center" h="60px" pl="10px" fontWeight="bold" color="color1">{title}</Flex>
            <Box pos="relative" >
                {events.map((event, i) => {
                    return (
                        <Box key={i}
                             position="absolute"
                             top={eventPosition(event.datetime_start) }
                             left={0}
                             h={eventHeight(event.duration)}
                             w="100%"
                             p="5px 10px"
                             cursor="pointer"
                             bg="#bbb"
                             color="color2"
                             // borderRadius="4px"
                             boxShadow="md"
                             _hover={{bg: "color1"}}
                             onClick={() => showDetails(event)}
                        >
                            <Text
                                m={0}
                                fontSize="sm"
                                fontWeight="bold"
                                lineHeight={1.2}
                                isTruncated
                                noOfLines={noOfLinesToShowInEventTitle(event.duration)}
                                cursor="pointer"
                            >
                                {event.title}
                            </Text>
                        </Box>
                    )
                })}
            </Box>
        </Box>
    )

}
