import Router from "next/router";
import {useEffect} from "react"

import {Flex, Icon, List, ListItem, Avatar, useBreakpointValue, useDisclosure, useTheme} from "@chakra-ui/react"
import {User} from "react-feather";

import MenuItem from "components/nav/menuItem";



// *** NOTE: This component is unused




export default function UserMenu({user, doAction}) {

    const theme = useTheme();

    const {isOpen, onToggle, onOpen, onClose} = useDisclosure({defaultIsOpen: false})
    const isMobile = useBreakpointValue({ base: true, xl: false })

    const menu = {
        title: 'User',
        slug: 'user',
        typeHandle: 'null',
        highlight: false
    }

    menu.pages = user?.isLoggedIn ? [
        {
            title: 'Actions',
            slug: 'actions',
            typeHandle: 'subhead',
            highlight: false,
            pages: []
        },
        {
            title: 'My Meeting',
            slug: 'profile',
            typeHandle: 'path',
            navigationPath: 'profile',
            highlight: false,
            pages: []
        },
        {
            title: 'Logout',
            slug: 'logout',
            typeHandle: 'action',
            menuItemHandler: 'logout',
            highlight: false,
            pages: []
        }
    ] : [
        {
            title: 'Actions',
            slug: 'actions',
            typeHandle: 'subhead',
            highlight: false,
            pages: []
        },
        {
            title: 'Login',
            slug: 'login',
            typeHandle: 'action',
            menuItemHandler: 'login',
            highlight: false,
            pages: []
        }
    ]

    return (
        <ListItem
            display="block"
            onMouseOver={isMobile ? undefined : onOpen}
            onMouseOut={isMobile ? undefined : onClose}
            borderLeft="1px solid white"
        >
            <Flex as="a"
                  justify="space-between"
                  href={null}
                  cursor="pointer"
                  onClick={onToggle}
                  h={theme.header.h}
                  align="center"
                  position="relative"
                  fontSize="14px"
                  color="#fff"
                  _hover={{color: "color4"}}

                  bg="color10"
                  p={{ base: "20px 10px", menu: "20px 20px" }}
            >
                <Avatar h="32px" w="32px"
                        border="1px solid #fff"
                        bgColor="color10"
                        icon={user ? null : <User color="#ccc" />}
                        name={user ? user.name : null}
                />
            </Flex>
            {menu.pages?.length ? (
                <List
                    bg="color10"
                    minW={200}
                    zIndex={1000}
                    display={{base: isOpen ? "block" : "none"}}
                    position={{base: "relative", md: "absolute"}}
                    // transform={{base: "none", md: "translateX(-10px)"}}
                    top={{base: 0, md: "100%"}}
                    right={0}
                    pb={4}
                    borderTop="1px solid"
                    borderTopColor="#fff"
                >
                    {menu.pages.map((subitem, i) => {
                        return (
                            <MenuItem key={i} item={subitem} level={1} doAction={doAction}/>
                        )
                    })}
                </List>) : null
            }
        </ListItem>
    )
}
