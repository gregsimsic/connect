import { useState, useEffect, useRef } from "react";
import { useRouter } from 'next/router'
import axios from "axios";
import {parseCookies, destroyCookie} from 'nookies'

import {useGlobalState} from "src/providers/globalState"

import {Flex, Stack, Box, Center, Heading, Text, Link, InputGroup, Input, InputRightElement, Button} from '@chakra-ui/react'

export default function LoginForm() {

    const router = useRouter()
    const {locale} = useGlobalState()

    const [password, setPassword] = useState(false)
    const [username, setUsername] = useState(false)
    const [showPassword, setShowPassword] = useState(false)
    const [errorMsg, setErrorMsg] = useState("");
    const [loadingText, setLoadingText] = useState("Logging in...");
    const [isAuthenticating, setIsAuthenticating] = useState(false)

    const errorMessages = {
        401: <Text>Login failed. Please check your credentials and try again.</Text>,
        403: <Text>Your login is correct, but you are not registered for this meeting. Please <Box as="a" textDecoration="underline" href='/register'>register here</Box>.</Text>
    }

    const usernameRef = useRef()

    useEffect(() => {
        usernameRef.current.focus()
    }, [])

    function togglePassword() {
        setShowPassword(!showPassword)
    }

    async function submit(e) {
        e.preventDefault();

        setIsAuthenticating(true)

        const body = { username: username, password: password, meetingKey: locale }

        await axios.post('/api/auth/login', body)
            .then((response) => {

                // redirect to the page they came from, the page they're on, or the /channels page if they hit the login page directly
                let redirectPage = parseCookies().loginRedirect || router.asPath
                redirectPage = (!redirectPage && router.asPath === "/login") ? "/channels" : redirectPage

                destroyCookie(null, 'loginRedirect')

                const redirectToAfterAuth = window.location.origin + redirectPage
                const oktaRedirectUrl = 'https://tctmd.okta.com/login/sessionCookieRedirect?token=' + response.data.oktaSessionToken + '&redirectUrl=' + redirectToAfterAuth

                setLoadingText("You're in! Hang on...")

                // redirect to okta to set the session cookie
                router.push(oktaRedirectUrl)

            })
            .catch ((error) => {

                // console.log('login error catch', error.response.data.message);

                setIsAuthenticating(false)
                setErrorMsg(errorMessages[error.response.status])

            })
            .then(() =>{
                // always executed

            });

    }

    return (
        <Box
            p={12}
            bgColor="color3"
            maxWidth="500px"
        >

            {errorMsg ? (
                <Box
                    color="#333"
                    display="inline-block"
                >{errorMsg}</Box>

            ) : null }

            <Heading as="h2"
                     fontSize="26px"
                     fontWeight={600}
                     color="white"
            >
                Please use your TCTMD login information.
            </Heading>

            <form method='post' onSubmit={submit}>
                <Stack spacing={4} mt={8}>
                    <Input
                        ref={usernameRef}
                        name="username"
                        placeholder="TCTMD Username"
                        _placeholder={{color: "#fff"}}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                    <InputGroup size="md">
                        <Input
                            name="password"
                            pr="4.5rem"
                            type={showPassword ? "text" : "password"}
                            placeholder="Password"
                            _placeholder={{color: "#fff"}}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <InputRightElement width="4.5rem">
                            <Button h="1.75rem" size="sm" onClick={togglePassword} bg="#fff" color="color5">
                                {showPassword ? "Hide" : "Show"}
                            </Button>
                        </InputRightElement>
                    </InputGroup>
                    <Button
                        color="white"
                        bgColor="color5"
                        isLoading={isAuthenticating}
                        loadingText={loadingText}
                        type="submit"
                    >
                        Login
                    </Button>
                    <Text>
                        <Link href="https://tctmd.okta.com/signin/forgot-password" isExternal mr={3}>Forgot Password?</Link>
                        <Link href="/register-tvt" mr={3}>Need to Register?</Link>
                        <Link href="mailto:accounthelp@crf.org" >Need Support?</Link>
                    </Text>
                </Stack>
            </form>

        </Box>
    )
}