import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalBody,
    Flex,
    Center,
    Spinner,
    Text
} from "@chakra-ui/react"

export default function LogoutModal() {

    return (
        <Modal isOpen={true} size="xs" isCentered>
            <ModalOverlay/>
            <ModalContent>
                <ModalBody>
                    <Center>
                        <Flex direction="column" align="center" py={8}>
                            <Spinner
                                thickness="3px"
                                speed="0.45s"
                                emptyColor="gray.200"
                                color="color1"
                                size="lg"
                            />
                            <Text color="#999">Logging you out...</Text>
                        </Flex>
                    </Center>
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}