import { useRouter } from 'next/router'
import {useEffect, useState} from "react"
// import useUser from "../../lib/useUser";
import {useAuth} from "../../providers/auth"
import UserMenu from "components/nav/userMenu";

import {
    Center, Flex, Text,
    List, ListItem,
    Modal, ModalBody, ModalContent, ModalOverlay, Spinner,
    useBreakpointValue, useDisclosure, useTheme, Avatar
} from "@chakra-ui/react"

import {ChevronDown, ChevronUp, LogIn, User} from "react-feather";
import MenuItem from "components/nav/menuItem"
import LoginModal from "components/nav/loginModal"
import LogoutModal from "components/nav/logoutModal";

export default function Nav(props) {

    const user = useAuth()
    const router = useRouter()

    const [completeMenu, setCompleteMenu] = useState([])
    const [isMenuInit, setIsMenuInit] = useState(false)
    const [isLoggingOut, setIsLoggingOut] = useState(false)
    const [loginModalIsOpen, setLoginModalIsOpen] = useState(false)

    let menu = props.menu

    const userStatusCheck = (user && user.isLoggedIn) ? 'loggedOutOnly' : 'loggedInOnly'

    // filter out menu items based on user status
    menu = menu.filter(item => {
        return item.userStatus !== userStatusCheck
    })

    // filter out SUB menu items based on user status
    menu = menu.map(item => {
        const pages = item.pages.filter(subitem => {
            return subitem.userStatus !== userStatusCheck
        })
        item.pages = pages
        return item
    })

    // attach user menu
    const userMenu = {
        title: 'User',
        typeHandle: 'null',
        bgColor: "color10",
        styles: {
            bg:"color10",
            p: { base: "20px 10px", menu: "20px 20px" }
        },
        label: <Avatar h="32px" w="32px"
                       border="1px solid #fff"
                       bgColor="color10"
                       icon={user ? null : <User color="#ccc" />}
                       name={user ? user.name : null}
        />
    }

    userMenu.pages = user?.isLoggedIn ? [
        {
            title: 'Actions',
            typeHandle: 'subhead'
        },
        {
            title: 'My Meeting',
            typeHandle: 'path',
            navigationPath: 'user/my-meeting'
        },
        {
            title: 'Logout',
            typeHandle: 'action',
            menuItemHandler: 'logout'
        }
    ] : [
        {
            title: 'Actions',
            typeHandle: 'subhead'
        },
        {
            title: 'Login',
            typeHandle: 'action',
            menuItemHandler: 'login'
        }
    ]

    // temporarily disable the user menu until we're ready to fully implement it
    // menu.push(userMenu)

    const actions = {
        logout: async () => {
            setIsLoggingOut(true)
            await fetch("/api/auth/logout")
            // either redirect to the homepage or reload the homepage if we're already there
            if (router.asPath === '/') {
                router.reload()
            } else {
                router.push('/')
            }
        },
        login: () => {
            setLoginModalIsOpen(true)
        }
    }

    const doAction = (action) => {
        actions[action].call()
    }

    useEffect(() => {

        if (isMenuInit) return false

        setCompleteMenu(menu)
        setIsMenuInit(true)

    }, [])

    return (
        <>
            <Flex as={List} direction={{base: "column", menu: "row"}}>
                {completeMenu.map((item, i) => {
                    return (
                        <MenuItem key={i} item={item} level={0} isLast={menu.length === i + 1} doAction={doAction}/>
                    )
                })}
            </Flex>

            {loginModalIsOpen && <LoginModal onClose={setLoginModalIsOpen}/>}

            {isLoggingOut && <LogoutModal />}

        </>
  )
}
