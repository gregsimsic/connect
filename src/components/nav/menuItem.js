import {
    Flex, Icon, List, ListItem, Text, Avatar,
    useBreakpointValue, useDisclosure, useTheme
} from "@chakra-ui/react"
import {ChevronDown, ChevronUp} from "react-feather";

import {useAuth} from "../../providers/auth"

export default function MenuItem({item, level, isLast, doAction}) {

    const theme = useTheme();
    const user = useAuth()

    const {isOpen, onToggle, onOpen, onClose} = useDisclosure({defaultIsOpen: false})

    // TODO: potential Chakra bug--cannot use 'menu' breakpoint in the following. Duplicated it as the 'xl' breakpoint in theme.js
    const isMobile = useBreakpointValue({ base: true, xl: false })

    const href = () => {
        switch (item.typeHandle) {
            case 'htmlPage' :
                return "/" + item.page[0]?.slug
                break;

            case 'path' :
                return "/" + item.navigationPath
                break;

            case 'link':
                return item.string

            default:
                return null
        }
    }

    function onClick() {
        if (item.menuItemHandler) {
            doAction(item.menuItemHandler)
        } else if (isMobile) {
            onToggle()
        } 
    }

    const isSubhead = item.typeHandle === "subhead"

    const cursorStyle = (["null","subhead"].includes(item.typeHandle)) ? 'default' : 'pointer'

    const target = (item.typeHandle === "link" || item.openInNewTab) ? "_blank" : null;

    const hoverStyles = item.highlight ? { bgColor: "color4", color: "#fff" } : { color: "color4" }

    const anchorPadding = level === 0
        ? { base: "20px 20px", menu: "20px 30px" }
        : { base: "10px 20px 10px 30px", menu: "5px 15px" }

    const borderLeft = level === 0
        ? { base: "none", menu: "1px solid white" }
        : "none"

    const subMenuIcon = item.pages?.length
        ? (isOpen
            ? <Icon as={ChevronUp} m="3px 0 0 5px" w="16px" h="16px"/>
            : <Icon as={ChevronDown} m="3px 0 0 5px" w="16px" h="16px"/>)
        : null

    return (
        <ListItem
            display="block"
            onMouseOver={isMobile ? undefined : onOpen}
            onMouseOut={isMobile ? undefined : onClose}
            borderLeft={borderLeft}
        >
            <Flex as="a"
                  justify="space-between"
                  href={href()}
                  target={target}
                  cursor={cursorStyle}
                  onClick={onClick}
                  h={level > 0 ? 'auto': theme.header.h}
                  p={anchorPadding}
                  mt={isSubhead ? '15px' : 0 }
                  align="center"
                  position="relative"
                  textTransform={isSubhead || level === 0 ? 'uppercase' : 'normal'}
                  fontSize={isSubhead ? '12px' : '13px'}
                  fontWeight={isSubhead ? '700' : 'normal'}
                  color={isSubhead ? theme.colors.color4 : "#fff" }
                  bg={item.highlight ? theme.colors.color1 : null}
                  _hover={isSubhead ? {} : hoverStyles}
                  sx={item.styles}
            >
                {item.label || item.title}
            </Flex>
            {item.pages?.length ? (
                <List
                    bg={item.bgColor || "color2"}
                    minW={200}
                    zIndex={1000}
                    display={{base: isOpen ? "block" : "none"}}
                    position={{base: "relative", menu: "absolute"}}
                    top={{base: 0, menu: "100%"}}
                    right={isLast ? 0 : 'unset'}
                    pb={4}
                    borderTop={level === 0 ? '1px solid' : 'none'}
                    borderTopColor="#fff"
                >
                    {item.pages.map((subitem, i) => {
                        return (
                            <MenuItem key={i} item={subitem} level={level+1} isLast={item.pages.length === i+1} doAction={doAction}/>
                        )
                    })}
                </List>) : null
            }
        </ListItem>
    )
}