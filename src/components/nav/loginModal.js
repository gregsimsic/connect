import {Center, Modal, ModalOverlay, ModalContent, ModalBody, ModalCloseButton } from "@chakra-ui/react"

import LoginForm from "components/nav/loginForm";

export default function LoginModal({onClose}) {

    return (
        <Modal isOpen={true} size="lg" bg="color1" isCentered closeOnOverlayClick={true} onClose={onClose}>
            <ModalOverlay/>
            <ModalContent>
                <ModalCloseButton />
                <ModalBody bgColor="color3" p={0}>
                    <Center>
                        <LoginForm />
                    </Center>
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}