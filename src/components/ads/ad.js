import { Center } from "@chakra-ui/react";

export default function Ad({slot}) {

    return (
        <Center
            w={{base: "48%", md: slot.w}}
            h={slot.h}
            p={5} mb={5}
            bg="#ddd"
            color="#333"
            textAlign="center"
        >
            {slot.label}
        </Center>
    )
}
