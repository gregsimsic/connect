import { Box } from "@chakra-ui/react";
import { AdSlot } from 'react-dfp';

export default function AdBlock({adUnit, sizes}) {

    // TODO: responsive refresh

    return (
        <Box>
            <AdSlot
                adUnit={adUnit}
                sizes={sizes}
                // shouldRefresh={true}
            />
        </Box>
    )
}
