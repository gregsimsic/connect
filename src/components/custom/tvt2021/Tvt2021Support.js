import {
    Box,
    Heading, Text,
    List, ListItem,
    Link,
    useTheme
} from "@chakra-ui/react"
import Wrapper from 'components/layout/wrapper'

export default function Tvt2021Support() {

    const theme = useTheme()

    const ulStyles = {
        pl: "30px",
        styleType: "disc",
        mb: 0,
        spacing: 0
    }

    return (
        <Wrapper
            bgColor={theme.colors.color11}
            color={theme.colors.color5}
            p="10px 20px 50px"
        >
            <Box className="body">
                <h2>Educational Grant Support</h2>
                <p>TVT: The Structural Heart Summit is sponsored by the Cardiovascular Research Foundation (CRF) and partially funded through educational grants from commercial supporters.</p>
                <p>This program is made possible in part by the educational grant support of our industry colleagues who, like CRF, are dedicated to educating medical professionals and the public to enhance patient outcomes. CRF gratefully acknowledges the educational grant support from the following:</p>
                <h3>Major Benefactors</h3>
                <List {...ulStyles}>
                    <ListItem>Abbott</ListItem>
                    <ListItem>Boston Scientific Corporation</ListItem>
                    <ListItem>Edwards Lifesciences</ListItem>
                    <ListItem>Medtronic</ListItem>
                </List>
                <h3>Benefactors</h3>
                <List {...ulStyles}>
                    <ListItem>Baylis Medical Company Inc.</ListItem>
                    <ListItem>Biosense Webster</ListItem>
                    <ListItem>Laminar Inc.</ListItem>
                    <ListItem>Neovasc Inc.</ListItem>
                    <ListItem>W. L. Gore & Associates</ListItem>
                </List>
                <Link className="btn" href="/supporters" mt={8}>Supporters</Link>
            </Box>
        </Wrapper>
    )
}