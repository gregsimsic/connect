import {Flex, Box, Center, Link, Button, AspectRatio, useTheme, Image, useBreakpointValue} from "@chakra-ui/react"
import { ArrowForwardIcon } from '@chakra-ui/icons'
import Wrapper from 'components/layout/wrapper'

export default function Tvt2021HomeTop({site, user}) {

    const theme = useTheme()
    const link = user ? '/topics' : '/login-register'

    const videoSrc = useBreakpointValue({
        base: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_MainHeader_mobile_02_no_logo-2.mp4",
        md: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_MainHeader_desktop_03_2500x750_no_logo2.mp4"
    })

    return (
        <Flex position="relative" overflow="hidden">
            <AspectRatio ratio={{base: 3/3, lg: 1200/500, xxl: 1200/360}} width="100%">
                <Box as="video"
                    autoPlay muted playsInline loop
                    src={videoSrc}
                    poster={{
                        base: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/placeholder-mobile.jpeg",
                        lg: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/video-placeholder.jpeg"
                    }}
                ></Box>
            </AspectRatio>

            <Box
                pos="absolute"
                bottom={0}
                right={{base: "10px", md: "30px"}}
                maxWidth={{base: "45vw", md: "35vw", lg: "100%"}}
                zIndex={10}
            >
                <Link href="https://connect.crf.org/?destination=/conference/tvt-connect-2020" target="_blank" >
                    <img src="https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2020-Button.png" alt="" />
                </Link>
            </Box>

            <Center
                position="absolute"
                w="100%" h="100%"
                top={0} left={0}
            >
                    <Image
                        src="https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVTLive2021_logo_mobile_06.png"
                        alt="TVT 2021"
                        maxWidth={{base: "70%", md: "60%", lg: "120%"}}
                        maxHeight={{base: "70%", md: "60%", lg: "120%"}}
                    />
                    {site.meeting.isLive && (
                        <Link
                            href={link}
                            display="block"
                            pos="absolute" bottom={{base: "45px", sm: 0}} left={{base: 0, xl: "50%"}}
                            _hover={{textDecoration: "none"}}
                        >
                            <Button
                                borderRadius={0}
                                bg="color1"
                                color="white"
                                size={{base: "md", sm: "lg"}}
                                rightIcon={<ArrowForwardIcon />}
                                p={{base: "10px 20px", sm: "20px 30px"}}
                                _hover={{bgColor: "color4"}}
                            >
                                Watch Now
                            </Button>
                        </Link>
                    )}
            </Center>
        </Flex>
    )
}