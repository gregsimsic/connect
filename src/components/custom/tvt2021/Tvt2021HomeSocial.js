import {
    Flex,
    Box,
    Center,
    Link,
    useTheme,
    Heading, Text
} from "@chakra-ui/react"
import Wrapper from 'components/layout/wrapper'
import SocialIcons from "components/header/SocialIcons";

export default function Tvt2021HomeSocial() {

    const theme = useTheme()

    return (
        <Wrapper
            bg={theme.colors.color4 + " url(https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/tvt-spots-bg75.jpeg) center top no-repeat"}
            p="110px 0 50px"
        >
            <Center>
                <Box textAlign="center">
                    <Heading as="h2" color="#000" fontWeight={600} fontSize={42} mb={0}>Join the Conversation</Heading>
                    <Text color="#000" fontSize="28px" fontWeight={600} mt={0} mb={2}>#TVT2021</Text>
                    <Flex justify="center">
                        <SocialIcons color="color5" size={{ base: "34px", lg: "40px"}} padding={4}/>
                    </Flex>
                </Box>
            </Center>
        </Wrapper>
    )
}