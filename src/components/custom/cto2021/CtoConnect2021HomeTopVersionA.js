import {Container, Flex, Box, Link, Text, Spacer, Heading, useTheme, Image, Icon} from "@chakra-ui/react"
import SocialIcons from "components/header/SocialIcons";
import Wrapper from "components/layout/wrapper";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'

import { CustomPrevArrow, CustomNextArrow } from "../../carousel/arrows";
import {ArrowRight} from "react-feather";

export default function CtoConnect2021HomeTopVersionA() {

    const theme = useTheme()

    const infoSlidesData = [
        {
            img: "https://connectadmin.crf.lcl/media/site/ctoconnect-highlights-faq.jpg",
            bgColor: "#85ACF5",
            title: "FAQ",
            link: "/faq",
            linkLabel: "Learn More"
        },
        {
            img: "https://connectadmin.crf.lcl/media/site/ctoconnect-highlights-register.jpg",
            bgColor: theme.colors.color4,
            title: "Register Now!<br>It's free.",
            link: "https://www.crf.org/login-ctoconnect2021-registration",
            linkLabel: "Register Now"
        },
        {
            img: "https://connectadmin.crf.lcl/media/site/ctoconnect-highlights-social.jpg",
            bgColor: "#85ACF5",
            title: "Connect on Social Media",
            socialIcons: true
        },
        {
            img: "https://connectadmin.crf.lcl/media/site/ctoconnect-highlights-program.jpg",
            bgColor: theme.colors.color4,
            title: "Program Guide",
            link: "/agenda",
            linkLabel: "View Now"
        },
    ]

    const infoSlides = infoSlidesData.map((slide, i) => {
        return <InfoSlide key={i} {...slide}/>
    })

    const infoSlideSlickSettings = {
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <Wrapper
            bg={theme.colors.color2 + " url(https://connectadmin.crf.lcl/media/site/cto2021-homepage-content-bg.jpg)"}
            backgroundPosition="bottom center"
            py="50px"
        >
            <Box w="100%" mb="120px">
                <Heading as="h2" color="color4">Learn from the best!</Heading>
                <Text>Join us on <strong>February 19-20, 2021</strong>, for <strong>CTO Connect</strong> as we transition the annual Chronic Total Occlusion Summit: A Live Case Demonstration Course into a compelling and interactive virtual experience — free of charge.</Text>
                <Text>This action-packed, two-day event will deliver first-rate education and training on recanalization of chronic total occlusions. CTO-PCI practitioners will learn state-of-the-art, advanced techniques from the most brilliant minds in the field.</Text>
            </Box>

            <Box w="100%" mb="80px">
                <Heading as="h2" color="#ebd9c7">2 Days. 2 Education-Rich Channels. 12 Live Cases. 80 World-Class Faculty From 18 Countries.</Heading>
                <Box as={Slider} {...infoSlideSlickSettings} mt="40px"> {infoSlides} </Box>
            </Box>

            <Box w="100%" position="relative" mb="30px">

                <Heading as="h2" color="#ebd9c7">Program At A Glance</Heading>
                <Text mt={0}>Click boxes below for detailed information.</Text>

                <Box as="iframe" src="https://s3.amazonaws.com/prod.tctmd.com/public/crfconnect/agenda-at-a-glance.html?meeting_id=148" w="100%" h="560px" border="none" mt="30px" mb="40px"/>

                <Link
                    href="/program"
                    color="#85acf2"
                    border="2px solid #85acf2"
                    display="inline-block"
                    p=".8rem 1.7rem"
                    fontWeight="bold"
                    _hover={{
                        textDecoration: "none",
                        bg: "rgba(133,172,242,0.1)",
                        color: "color4"
                    }}
                >
                    VIEW PROGRAM GUIDE
                </Link>

            </Box>
        </Wrapper>
    )
}

function InfoSlide(slide) {

    return (
        <Flex direction="column" align="center" mx="15px" textAlign="center">
            <Box >
                <Image src={slide.img} alt={slide.title}/>
            </Box>
            <Flex textAlign="left" w="100%" bg={slide.bgColor} p="15px" direction="column" justify="space-between" minHeight="200px">
                <Heading as="h3" color="color2" lineHeight="1" dangerouslySetInnerHTML={{__html: slide.title}}/>
                <Spacer/>
                <Box>
                    {!slide.socialIcons ?
                        <Link href={slide.link}
                              display="inline-block"
                              bg="red"
                              px={2} py={3}
                              _hover = {{
                                  bg: "white",
                                  color: "red"
                              }}
                              role="group"
                        >
                            <Text as="span"
                                  textTransform="uppercase"
                                  fontWeight="bold"
                                  fontSize="16px"
                                  p="8px"
                            >
                                {slide.linkLabel} <Icon as={ArrowRight}
                                                        w="24px" h="24px"
                                                        fill="none"
                                                        stroke="white"
                                                        verticalAlign="bottom"
                                                        _groupHover={{
                                                            stroke: "red"
                                                        }}
                                                    />
                            </Text>
                        </Link>
                    :   <SocialIcons color="red" size="32px" padding={1} />
                    }
                </Box>
            </Flex>
        </Flex>
    )
}

