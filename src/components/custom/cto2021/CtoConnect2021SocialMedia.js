import {Container, Flex, Box, Link, Text, Heading} from "@chakra-ui/react"
import Wrapper from "components/layout/wrapper";
import SocialIconsHexagonals from "components/header/SocialIconsHexagonals";

export default function CtoConnect2021SocialMedia() {
    return (
            <Wrapper
                  bg="url(https://connectadmin.crf.lcl/media/site/cto-connect-homepage-social-media-bg.jpg)"
                  py="80px"
            >
                <Flex justify="space-between" direction={{base: 'column', lg: 'row'}} >
                    <Box w={{ base: "100%", lg: "50%"}} mb="30px">
                        <Heading as="h2" color="color2">Join the Conversation</Heading>
                        <Text fontSize={18} maxW="500px" mt="15px">Follow us on social media, so you can hear from us, and we can hear from you!</Text>
                        <Text color="color4" fontSize="32px" fontWeight="bold" my="15px">
                            <Link href="https://twitter.com/hashtag/CTO2021">#CTO2021</Link> <Link href="https://twitter.com/hashtag/CTOConnect">#CTOConnect</Link>
                        </Text>
                    </Box>
                    <Flex w={{ base: "100%", lg: "50%"}} position="relative" align="center">
                        <Flex>
                            <SocialIconsHexagonals color="color1" size={{ base: "56px", lg: "120px"}}/>
                        </Flex>
                    </Flex>
                </Flex>
            </Wrapper>
    )
}
