import {Flex, Box, Center, Circle, Link, useTheme, Image, Heading, Spacer, Text, Icon} from "@chakra-ui/react"
import Wrapper from 'components/layout/wrapper'

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import {CustomNextArrow, CustomPrevArrow} from "components/carousel/arrows";
import {ArrowRight, Facebook, Instagram, Linkedin, Twitter, Youtube} from "react-feather";

export default function FellowsConnectHomeMain() {

    const theme = useTheme()

    let bg = "#113e7e url(https://assets.crfconnect.com/meetings/fellowsConnect2021/homepage/Section-Transition.jpg)"

    const infoSlidesData = [
        {
            img: "https://assets.crfconnect.com/meetings/fellowsConnect2021/homepage/Fellows2021-register.jpg",
            imgAlt: "Watch On Demand",
            title: "Watch On Demand",
            link: "/login-register",
        },
        {
            img: "https://assets.crfconnect.com/meetings/fellowsConnect2021/homepage/Fellows2021-faq.jpg",
            imgAlt: "Read our FAQ",
            title: "Read our FAQ",
            link: "/about-faq",
        },
        {
            img: "https://assets.crfconnect.com/meetings/fellowsConnect2021/homepage/Fellows2021-core-concepts.jpg",
            imgAlt: "Watch Core Concepts On Demand",
            title: "Watch Core Concepts<br><strong>On Demand</strong>",
            link: "/program-ondemand",
        },
        {
            img: "https://assets.crfconnect.com/meetings/fellowsConnect2021/homepage/Fellows2021-connect.jpg",
            imgAlt: "Connect with CRF",
            title: "Connect with CRF",
            link: null,
            socialIcons: true
        },
    ]

    const infoSlides = infoSlidesData.map((slide, i) => {
        return <InfoSlide key={i} {...slide}/>
    })

    const infoSlideSlickSettings = {
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <Box position="relative"
             bg={bg}
             bgPosition="bottom"
             bgSize="cover"
             py="30px"
        >
            <Wrapper className="body">
                <h2>Free registration for current Fellows enrolled in a fellowship training program.</h2>
                <p>Join us for Fellows Course Connect, the fully online version of CRF’s 26th Annual Interventional Cardiology Fellows Course—a three-day interactive education program covering key concepts in interventional cardiovascular medicine.</p>
                <h2><span style={{color: "#00c6ff"}}>For Fellows</span></h2>
                <p>For 26 years, the Fellows Course has been providing building blocks for thriving careers in interventional cardiovascular medicine. <strong>Now it’s your turn to attend the Fellows Course and take advantage of this invaluable opportunity. No Fellow should miss it!</strong></p>
                <h2><span style={{color: "#00c6ff"}}>For Everyone!</span></h2>
                <p><strong>Who else should attend Fellows Course Connect?</strong><br/>The course is for every member of the heart team, covering the essentials of interventional cardiology.<br/>Refresh your skills—and learn new ones—if you are a:</p>
                <ul><li>Practicing interventional cardiologist</li><li>Clinical cardiologist</li><li>Catheterization laboratory nurse</li><li>Cardiovascular technician</li><li>Health care professional involved in interventional cardiovascular procedures</li></ul>
            </Wrapper>

            <Wrapper py="50px" overflow="hidden">
                <Box as={Slider} {...infoSlideSlickSettings} mt="40px"> {infoSlides} </Box>
            </Wrapper>

            <Wrapper py="50px">
                <Heading as="h3" color="#00c6ff" mb={3}>Program At a Glance</Heading>
                <Box as="iframe" src="https://www.crf.org/fellowsatglance/" w="100%" h={{base: "720px", lg: "575px"}} border="none" />
                {/*<Box className="body">*/}
                {/*    <a className="btn btn-primary" href="/overview">View Course Overview</a>*/}
                {/*</Box>*/}
            </Wrapper>

        </Box>
    )
}

function InfoSlide(slide) {

    const socialIconSize = "28px"

    const OuterComponent = slide.link ? Link : Box

    return (
        <Box mx="15px" pos="relative">
            <Box as={OuterComponent} href={slide.link}
                  cursor={slide.link ? 'pointer' : 'default'}
                  role="group"
                  pos="relative"
                  display="block"
            >
                <Box>
                    <Image src={slide.img} alt={slide.imgAlt}/>
                </Box>
                <Flex
                    pos="absolute"
                    top={0}
                    w="100%"
                    h="100%"
                    p="15px"
                    textAlign="center"
                    align="flex-end"
                    justify="center"
                    direction="column"
                    overflow="hidden"
                >
                    <Box w="110%" h="100%" bg="#fc9a86" borderRadius={999} pos="absolute" bottom="-62%" left="50%" transform="translateX(-50%)"/>
                    <Box
                        zIndex={1}
                        pos="absolute"
                        left="50%"
                        top="72%"
                        transform="translateX(-50%)"
                        w="100%"
                    >
                        <Heading as="h3"
                                 fontWeight={400}
                                 color="color2"
                                 lineHeight="1"
                                 fontSize={22}
                                 mb={1}
                                //  _groupHover = {{
                                //     color: "#fff"
                                // }}
                                 dangerouslySetInnerHTML={{__html: slide.title}}
                        />
                        {slide.socialIcons && (
                            <Flex mr="15px" justify="center">
                                <Link p={2}  href="https://twitter.com/CRFHeart" target="_blank" >
                                    <Icon as={Twitter} w={socialIconSize} h={socialIconSize} fill="color2" stroke="none"/>
                                </Link>
                                <Link p={2} color="color2" href="https://www.facebook.com/CRFheart/" target="_blank" >
                                    <Icon as={Facebook} w={socialIconSize} h={socialIconSize} fill="color2" stroke="none"/>
                                </Link>
                                <Link p={2} color="color2" href="https://www.instagram.com/crfheart/" target="_blank" >
                                    <Icon as={Instagram} w={socialIconSize} h={socialIconSize} stroke="color2"/>
                                </Link>
                                <Link p={2} color="color2" href="https://www.linkedin.com/company/cardiovascular-research-foundation" target="_blank">
                                    <Icon as={Linkedin} w={socialIconSize} h={socialIconSize} fill="color2" stroke="none"/>
                                </Link>
                            </Flex>
                        )}
                    </Box>
                </Flex>
            </Box>
        </Box>
    )
}
