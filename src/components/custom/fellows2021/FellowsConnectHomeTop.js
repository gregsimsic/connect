import {Flex, Box, Center, Link, Button, AspectRatio, useTheme, Image} from "@chakra-ui/react"
import { ArrowForwardIcon } from '@chakra-ui/icons'
import Wrapper from 'components/layout/wrapper'

import {useGlobalState} from "src/providers/globalState"
import useUser from "lib/useUser";

export default function FellowsConnectHomeTop({user}) {

    const theme = useTheme()
    const {site} = useGlobalState()
    const link = user ? '/channels' : '/login-register'

    return (
        <Flex position="relative" overflow="hidden">
            <AspectRatio ratio={{base: 3/2, lg: 1200/500, xxl: 1200/360}} width="100%">
                <video
                    autoPlay muted playsInline loop
                    src="https://assets.crfconnect.com/meetings/fellowsConnect2021/theme/Fellows-Course-Connect-Website-Header-04.mp4"
                ></video>
            </AspectRatio>

            <Flex
                position="absolute"
                w="100%" h="100%"
                top={0} left={0}
                align="flex-end"
                justify={{base: "center", lg: "left"}}
            >
                <Box maxWidth={{base: "100%", lg: "1200px"}} width="100%" margin={{base: "0 0 0 50px", lg: "0 auto"}} pl={{lg: "20px"}} >
                    <Box maxWidth={{base: "80%", md: "600px", lg: "500px"}} >
                        <Image
                            src="https://assets.crfconnect.com/meetings/fellowsConnect2021/homepage/Fellows-Connect-website-Logo-Lockup-2022.png"
                            alt="Fellows Course Connect"
                        />
                        {site.meeting.isLive && (
                            <Link href={link} mt="60px" pl="60px" display="inline-block">
                                <Button
                                    borderRadius={0}
                                    bg="color4"
                                    color='#333'
                                    size="lg"
                                    rightIcon={<ArrowForwardIcon />}
                                    p={{base: "25px 40px", md: "33px 55px"}}
                                >
                                    Watch On Demand
                                </Button>
                            </Link>
                        )}
                    </Box>
                </Box>
            </Flex>
        </Flex>
    )
}