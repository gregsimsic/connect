import React, {useEffect, useState} from "react";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import { CustomPrevArrow, CustomNextArrow } from "../../carousel/arrows";

import {Flex, Box, Link, Image, Text, Heading, useTheme, Icon} from "@chakra-ui/react";
import {Play, Twitter} from "react-feather"
import Wrapper from "../../layout/wrapper";
import {useConferenceSpecialFaculty} from "services/emma/emma";

export default function FellowsConnectCourseDirectors(props) {

    const theme = useTheme()
    const breakpoints = theme.breakpoints;

    const bg = "url(https://assets.crfconnect.com/meetings/fellowsConnect2021/theme/fellows-star-bg75.jpg)"

    const slickSettings = {
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    // const { status, data, error, isFetching } = useConferenceSpecialFaculty(props.meetingId);

    const directors = [
        {
            name: "Ajay J. Kirtane, MD, SM",
            image: "https://assets.crfconnect.com/meetings/fellowsConnect2021/theme/kirtane-p1.jpeg",
            twitterHandle: "ajaykirtane",
            link: "/faculty/Ajay-Kirtane-1829"
        },
        {
            name: "Manish Parikh, MD",
            image: "https://assets.crfconnect.com/meetings/fellowsConnect2021/theme/parikh-m-p3.jpeg",
            twitterHandle: "WeillCornell",
            link: "/faculty/Manish-Parikh-1750"
        },
        {
            name: "Sahil Parikh, MD",
            image: "https://assets.crfconnect.com/meetings/fellowsConnect2021/theme/parikh-s-p2.jpeg",
            twitterHandle: "sahilparikhmd",
            link: "/faculty/Sahil-Parikh-2265"
        }
    ]

    return (
        <Wrapper bg={bg} color="color2" p="60px 40px 120px" >
            <Box w="100%" m="0 auto">
                <Heading as="h3"
                         color="#fc9a86"
                         mb={6}
                         fontWeight="600"
                >
                    Course Directors
                </Heading>
                <Slider {...slickSettings}>
                {directors.map((faculty, i) => {
                    return (<Slide {...faculty} key={i}/>)
                })}
                </Slider>
            </Box>
        </Wrapper>
    )
}

function Slide(slide) {

    return (
        <Flex direction="column" align="center" mx="10px" textAlign="center" role="group" pos="relative">
            <Link w="100%" pt="150%" position="relative" overflow="hidden" href={slide.link}>
                <Image src={slide.image} alt={slide.name} width="100%" position="absolute" top="50%" transform="translateY(-50%)"/>
            </Link>
            <Text
                mt="15px"
                fontWeight="600"
                size="sm"
                color="#fc9a86"
            >
                {slide.name}
            </Text>
            <Text mt={1}>
                <Link href={"https://twitter.com/" + slide.twitterHandle} target="_blank" color="#fff" _hover={{color: "color4"}} fontSize="13px">
                    <Icon as={Twitter} w="16px" h="16px" fill="color4" stroke="none"/> @{slide.twitterHandle}
                </Link>
            </Text>
            {/*<Flex display="none" _groupHover={{ display: 'block' }} pos="absolute" top={0} left={0} align="center" h="100%" w="100%">*/}
            {/*    More Information*/}
            {/*</Flex>*/}
        </Flex>
    )
}
