import {Container, Flex, Box, Link, Text, Heading} from "@chakra-ui/react"
import SocialIcons from "components/header/SocialIcons";

export default function Tct2020Supporters() {
    return (
        <Container bg="grayMedium"
                   maxW="100%"
                   py="80px"
        >
            <Box maxW={{xl: 1200}}
                  w="100%"
                  mx="auto"
                  px={{base: '20px', lg: 0}}
            >
                <Box mb="10px" align="center" color="color2">
                    <Heading as="h3" >Educational Grant Support</Heading>
                    <Text maxW="800px" mt="15px" color="color2" >TCT Connect is sponsored by the Cardiovascular Research Foundation (CRF) and partially funded through educational grants from commercial supporters.</Text>
                    <Text maxW="800px" mt="15px" color="color2" >This program is made possible in part by the educational grant support of our industry colleagues who, like CRF, are dedicated to educating medical professionals and the public to enhance patient outcomes. CRF gratefully acknowledges the educational grant support from the following:</Text>
                    <Heading as="h4" mt="40px">Major Benefactors</Heading>
                </Box>
                <Flex 
                    direction={{base: "column", lg: "row"}}
                    justify="space-between"
                >
                    <img src="https://www.tctconnect.com/wp-content/uploads/2020/09/egs-abbott-300x90.jpg" alt="Abbott"/>
                    <img src="https://www.tctconnect.com/wp-content/uploads/2020/09/egs-bostonscientific-300x90.jpg" alt="Boston Sci"/>
                    <img src="https://www.tctconnect.com/wp-content/uploads/2020/09/egs-medtronic-300x90.jpg" alt="Medtronic"/>
                    <img src="https://www.tctconnect.com/wp-content/uploads/2020/09/egs-edwards-300x90.jpg" alt="Edwards"/>
                </Flex>
            </Box>
        </Container>
    )
}
