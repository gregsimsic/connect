import {Container, Flex, Box, Link, Text, Heading} from "@chakra-ui/react"
import SocialIcons from "../../header/SocialIcons";

export default function Tct2020SocialMedia() {
    return (
        <Container bg="url(https://connectadmin.crf.lcl/media/site/mp_dots_bg.jpg)"
                   backgroundSize="cover"
                   maxW="100%"
                   py="80px"
        >
            <Flex maxW={{xl: 1200}}
                  w="100%"
                  mx="auto"
                  justify="space-between"
                  direction={{base: 'column', lg: 'row'}}
                  px={{base: '20px', lg: 0}}>
                <Box w={{ base: "100%", lg: "50%"}} mb="30px">
                    <Heading as="h2">Connect to the TCT Community</Heading>
                    <Text maxW="350px" mt="15px">Follow us on social media, so you can hear from us and we can hear from you!</Text>
                    <Text color="color4" fontSize="32px" fontWeight="bold" my="15px">#TCT2020 #TCTConnect</Text>
                    <SocialIcons color="color1" size="28px"/>
                </Box>
                <Box w={{ base: "100%", lg: "50%"}} position="relative">
                        <Box>
                            <img src="https://ovation-crf.s3.amazonaws.com/2020/TCT/mp-assets/iphoneOverlay.png"  alt=""/>
                        </Box>
                        <Box position="absolute" top="8px" zIndex="10" borderRadius="10px" overflow="hidden" m="0 10%">
                            <video width="100%" height="auto" autoPlay muted loop playsInline >
                                <source src="https://ovation-crf.s3.amazonaws.com/2020/TCT/mp-assets/TCT-CONNECT-Social-Media-01.mp4" type="video/mp4"/>
                            </video>
                        </Box>
                </Box>
            </Flex>
        </Container>
    )
}
