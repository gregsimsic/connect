import {Flex, Box, Center, Link, Button, AspectRatio, useTheme, Image, useBreakpointValue} from "@chakra-ui/react"
import { ArrowForwardIcon } from '@chakra-ui/icons'
import Wrapper from 'components/layout/wrapper'

import {useGlobalState} from "src/providers/globalState"

export default function Tht2022HomeTop() {

    const theme = useTheme()
    const {site} = useGlobalState()

    const videoSrc = useBreakpointValue({
        base: "https://assets.crfconnect.com/meetings/tht2022/homepage/THT2022_homepage_mobile_header_01.mp4",
        md: "https://assets.crfconnect.com/meetings/tht2022/homepage/THT2022_homepage_header_01.mp4"
    })


    return (
        <Flex position="relative" overflow="hidden">
            <AspectRatio ratio={{base: 3/3, lg: 1200/500, xxl: 1200/360}} width="100%" maxHeight="480px">
                <Box as="video"
                    autoPlay muted playsInline loop
                    src={videoSrc}
                    poster={{
                        base: "https://assets.crfconnect.com/meetings/tct2021/homepage/placeholder.jpeg",
                        lg: "https://assets.crfconnect.com/meetings/tct2021/homepage/placeholder.jpeg"
                    }}
                ></Box>
            </AspectRatio>

            {/*<Box*/}
            {/*    pos="absolute"*/}
            {/*    bottom={0}*/}
            {/*    right={{base: "10px", md: "30px"}}*/}
            {/*    maxWidth={{base: "45vw", md: "35vw", lg: "100%"}}*/}
            {/*    zIndex={10}*/}
            {/*>*/}
            {/*    <Link href="https://www.tctconnect.com/" target="_blank" >*/}
            {/*        <img src="https://assets.crfconnect.com/meetings/tct2021/homepage/TCT2020-button.png" alt="TCT 2020" />*/}
            {/*    </Link>*/}
            {/*</Box>*/}

            <Center
                position="absolute"
                w="100%" h="100%"
                top={0} left={0}
            >
                <Image
                    src="https://assets.crfconnect.com/meetings/tht2022/homepage/THT2022_desktop_logo_01.png"
                    alt="THT 2022"
                    maxWidth={{base: "80%", lg: "80%"}}
                    maxHeight={{base: "80%", lg: "80%"}}
                    ml={{base: 0, lg: "27%"}}
                />
            </Center>
        </Flex>
    )
}