import {
    Flex,
    Box,
    Center,
    Link,
    useTheme,
    Heading, Text
} from "@chakra-ui/react"
import Wrapper from 'components/layout/wrapper'
import SocialIcons from "components/header/SocialIcons";

export default function Tht2022HomeSocial() {

    const theme = useTheme()

    return (
        <Box
            bg={theme.colors.color4 + " url(https://assets.crfconnect.com/meetings/tht2022/homepage/THT2021_social_bkg_01.jpeg) center no-repeat"}
            backgroundSize="cover"
            p="65px 0 55px"
        >
            <Center>
                <Box textAlign="center">
                    <Heading as="h2" color="color5" fontWeight={600} fontSize={42} my={0}>Join the Conversation</Heading>
                    <Text color="color5" fontSize="28px" fontWeight={600} mt={0} mb={2}>#THT2022</Text>
                    <Flex justify="center">
                        <SocialIcons color="color5" size={{ base: "34px", lg: "40px"}} padding={4}/>
                    </Flex>
                </Box>
            </Center>
        </Box>
    )
}