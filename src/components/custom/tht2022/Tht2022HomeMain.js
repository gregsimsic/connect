import {Flex, Box, Center, Link, Image, Heading, Spacer, Text, Button, List, ListItem} from "@chakra-ui/react"

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import {CustomNextArrow, CustomPrevArrow} from "components/carousel/arrows";
import Wrapper from 'components/layout/wrapper'
import SocialIcons from "components/header/SocialIcons";

export default function Tht2022HomeMain({site}) {

    const ulStyles = {
        pl: "30px",
        styleType: "disc",
        mb: 0,
        spacing: 0
    }
    const h2Styles = {
        color: "color1",
        mb: "5px"
    }

    const infoSlidesData = [
        {
            img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_FAQ.jpeg",
            title: "FAQ",
            link: "/faqs",
            button: "Read Our FAQ"
        },
        {
            img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_social2.jpeg",
            title: "Follow Us",
            link: null,
            socialIcons: true
        },
        {
             img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_Register.jpeg",
             title: "Registration is Open",
             link: "/register-tvt",
             button: "Register Today"
         },
        //{
        //    img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_CallForScience.jpeg",
        //    title: "Call for Science Deadline Extended!",
        //    link: "/topics-categories-guidelines",
        //    button: "Submit Science Now"
        //}
    ]

    const infoSlides = infoSlidesData.map((slide, i) => {
        return <InfoSlide key={i} {...slide}/>
    })

    const infoSlideSlickSettings = {
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <Box position="relative"
             bgColor="color5"
             pt="50px"
             pb="70px"
        >
            <Wrapper>

                <p>CRF is proud to produce its first-ever <strong>Technology and Heart Failure Therapeutics</strong> conference, <strong>THT 2022</strong>. Given the growing prevalence of heart failure worldwide, we recognize a clear mandate for developing new monitoring strategies and treatment options to increase survival, decrease hospitalizations, and improve quality of life — and for achieving these goals via a variety of potential interventions that target the various contributors to heart failure.</p>
                <p>THT 2022 is the conference that focuses on technologies that will fill existing gaps.</p>
                <p>Covering a full range of updates on the current status of drug- and device-based treatments for heart failure across the spectrum, LV ejection fractions, and a late-breaking pivotal studies that have not yet appeared in treatment guidelines, THT 2022 is an immersive experience in the latest innovations that practitioners can put into practice for treating heart failure.</p>
                <p>With unlimited opportunities for learning and global engagement, THT 2022 offers a future-oriented view of the pipeline for heart failure therapeutics, with a focus on in-development technologies (drugs, devices, monitoring strategies) that are currently in clinical trials — as well as an overview of the rapid advances in artificial intelligence and machine learning and how these technologies will transform the care of heart failure patients in the near future.</p>

            </Wrapper>

            {/*<Wrapper pt="10px" pb="50px" overflow="hidden">*/}
            {/*    <Box m={{base: "0 15px", xxl: "0"}}>*/}
            {/*        <Box*/}
            {/*            as={Slider}*/}
            {/*            {...infoSlideSlickSettings}*/}
            {/*            mt="40px"*/}
            {/*        >*/}
            {/*            {infoSlides}*/}
            {/*        </Box>*/}
            {/*    </Box>*/}
            {/*</Wrapper>*/}

            {/*<Wrapper py="50px">*/}
            {/*    <Heading as="h3" mb={3}>Program At a Glance</Heading>*/}
            {/*    <Box as="iframe" src="https://www.crf.org/fellowsatglance/" w="100%" h={{base: "720px", lg: "575px"}} border="none" />*/}
            {/*</Wrapper>*/}

        </Box>
    )
}

function InfoSlide(slide) {

    const OuterComponent = slide.link ? Link : Box

    return (
        <Box mx="5px" pos="relative">
            <Box as={OuterComponent} href={slide.link}
                  cursor={slide.link ? 'pointer' : 'default'}
                  role={slide.link ? "group" : "none"}
                  pos="relative"
                  display="block"
                  _hover={{textDecoration: "none"}}
            >
                <Box>
                    <Image src={slide.img} alt={slide.title}/>
                </Box>
                <Flex
                    bg="color4"
                    p="25px 35px 15px"
                    h="170px"
                    direction="column"
                    justify="space-between"
                >
                    <Heading as="h3"
                             fontWeight={600}
                             color="#fff"
                             lineHeight="1"
                             fontSize={24}
                             mt={0} mb={1}
                             dangerouslySetInnerHTML={{__html: slide.title}}
                    />
                    {slide.button && (
                        <Button
                            borderRadius={0}
                            bgColor="color5"
                            my={3}
                            p="20px 45px"
                            textTransform="uppercase"
                            fontSize="13px"
                            fontWeight="normal"
                            alignSelf="flex-start"
                            _hover={{bgColor: "color2"}}
                        >
                            {slide.button}
                        </Button>
                    )}
                    {slide.socialIcons && (
                        <Flex ml={-3}>
                            <SocialIcons color="color5" size="32px" padding={3}/>
                        </Flex>
                    )}
                </Flex>
            </Box>
        </Box>
    )
}
