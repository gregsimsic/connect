import {Flex, Box, Center, Link, Image, Heading, Spacer, Text, Button, List, ListItem} from "@chakra-ui/react"

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import {CustomNextArrow, CustomPrevArrow} from "components/carousel/arrows";
import Wrapper from 'components/layout/wrapper'
import SocialIcons from "components/header/SocialIcons";

export default function Comp2021HomeMain({site}) {

    const ulStyles = {
        pl: "30px",
        styleType: "disc",
        mb: 0,
        spacing: 0
    }
    const h2Styles = {
        color: "color1",
        mb: "5px"
    }

    const infoSlidesData = [
        {
            img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_FAQ.jpeg",
            title: "FAQ",
            link: "/faqs",
            button: "Read Our FAQ"
        },
        {
            img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_social2.jpeg",
            title: "Follow Us",
            link: null,
            socialIcons: true
        },
        {
             img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_Register.jpeg",
             title: "Registration is Open",
             link: "/register-tvt",
             button: "Register Today"
         },
        //{
        //    img: "https://assets.crfconnect.com/meetings/tvtConnect2021/homepage/TVT2021_CallForScience.jpeg",
        //    title: "Call for Science Deadline Extended!",
        //    link: "/topics-categories-guidelines",
        //    button: "Submit Science Now"
        //}
    ]

    const infoSlides = infoSlidesData.map((slide, i) => {
        return <InfoSlide key={i} {...slide}/>
    })

    const infoSlideSlickSettings = {
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <Box position="relative"
             bgColor="color5"
             pt="30px"
             pb="50px"
        >
            <Wrapper>

                <Heading as="h1" color="color4" fontSize="42px!important" pt="10px" lineHeight={1}>Save the Date!</Heading>
                <p>The most comprehensive course on interventional complications is designed for practicing and future interventional cardiologists, nurses, and technologists seeking to improve their ability to manage complications and proceed with fewer fears about unforeseen and challenging events that can occur during cath lab procedures.</p>
                <p><strong>Interventional Complications at TCT</strong>, which begins two days before TCT 2021, in Orlando, Florida, can only accommodate a limited number of in-person attendees. Those who register for the course will receive a discount on TCT 2021! Details will be released soon.</p>
                <p>The program will focus on thematic and algorithmic structures for putting programs in place to both avoid coronary and structural heart complications and to deal with the aftermath. It will examine not only the interventional management, but also the psychological effects, with proposed structural and team-based solutions for complications management.</p>

                <h3>Why you should attend</h3>
                <p>At the end of the course, you will be able to:</p>
                <List {...ulStyles}>
                    <ListItem>Execute treatment strategies for common and rarer interventional complications.</ListItem>
                    <ListItem>Understand issues relating to clinical indications for complex procedures, including patient-centered care and risk mitigation.</ListItem>
                    <ListItem>Embrace the importance of building support networks to manage challenging technical and clinical aspects of patient care.</ListItem>
                </List>

            </Wrapper>

            {/*<Wrapper pt="10px" pb="50px" overflow="hidden">*/}
            {/*    <Box m={{base: "0 15px", xxl: "0"}}>*/}
            {/*        <Box*/}
            {/*            as={Slider}*/}
            {/*            {...infoSlideSlickSettings}*/}
            {/*            mt="40px"*/}
            {/*        >*/}
            {/*            {infoSlides}*/}
            {/*        </Box>*/}
            {/*    </Box>*/}
            {/*</Wrapper>*/}

            {/*<Wrapper py="50px">*/}
            {/*    <Heading as="h3" mb={3}>Program At a Glance</Heading>*/}
            {/*    <Box as="iframe" src="https://www.crf.org/fellowsatglance/" w="100%" h={{base: "720px", lg: "575px"}} border="none" />*/}
            {/*</Wrapper>*/}

        </Box>
    )
}

function InfoSlide(slide) {

    const OuterComponent = slide.link ? Link : Box

    return (
        <Box mx="5px" pos="relative">
            <Box as={OuterComponent} href={slide.link}
                  cursor={slide.link ? 'pointer' : 'default'}
                  role={slide.link ? "group" : "none"}
                  pos="relative"
                  display="block"
                  _hover={{textDecoration: "none"}}
            >
                <Box>
                    <Image src={slide.img} alt={slide.title}/>
                </Box>
                <Flex
                    bg="color4"
                    p="25px 35px 15px"
                    h="170px"
                    direction="column"
                    justify="space-between"
                >
                    <Heading as="h3"
                             fontWeight={600}
                             color="#fff"
                             lineHeight="1"
                             fontSize={24}
                             mt={0} mb={1}
                             dangerouslySetInnerHTML={{__html: slide.title}}
                    />
                    {slide.button && (
                        <Button
                            borderRadius={0}
                            bgColor="color5"
                            my={3}
                            p="20px 45px"
                            textTransform="uppercase"
                            fontSize="13px"
                            fontWeight="normal"
                            alignSelf="flex-start"
                            _hover={{bgColor: "color2"}}
                        >
                            {slide.button}
                        </Button>
                    )}
                    {slide.socialIcons && (
                        <Flex ml={-3}>
                            <SocialIcons color="color5" size="32px" padding={3}/>
                        </Flex>
                    )}
                </Flex>
            </Box>
        </Box>
    )
}
