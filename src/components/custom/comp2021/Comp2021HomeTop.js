import {Flex, Box, Center, Link, Button, AspectRatio, Image, useBreakpointValue} from "@chakra-ui/react"
import { ArrowForwardIcon } from '@chakra-ui/icons'
import Wrapper from 'components/layout/wrapper'

export default function Comp2021HomeTop() {

    const videoSrc = useBreakpointValue({
        base: "https://assets.crfconnect.com/meetings/complications2021/homepage/Complications-Course-Marketing-Header-Design-1-01-nolockup.mp4",
        md: "https://assets.crfconnect.com/meetings/complications2021/homepage/Complications-Course-Marketing-Header-Design-1-01-nolockup.mp4"
    })

    return (
        <Flex position="relative" overflow="hidden">
            <AspectRatio ratio={{base: 3/3, lg: 1200/500, xxl: 1200/360}} width="100%" maxHeight="480px">
                <Box as="video"
                    autoPlay muted playsInline loop
                    src={videoSrc}
                    poster={{
                        base: "https://assets.crfconnect.com/meetings/tct2021/homepage/placeholder.jpeg",
                        lg: "https://assets.crfconnect.com/meetings/tct2021/homepage/placeholder.jpeg"
                    }}
                ></Box>
            </AspectRatio>

            <Center
                position="absolute"
                w="100%" h="100%"
                top={0} left={0}
            >
                <Image
                    src="https://assets.crfconnect.com/meetings/complications2021/homepage/Interventional-Complications-Static-Header-logo-lockup-01.png"
                    alt="Complications Course 2021"
                    maxWidth={{base: "70%", lg: "70%"}}
                maxHeight={{base: "70%", lg: "60%"}}
                />
            </Center>
        </Flex>
    )
}