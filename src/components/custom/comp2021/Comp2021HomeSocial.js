import {
    Flex,
    Box,
    Center,
    Link,
    useTheme,
    Heading, Text
} from "@chakra-ui/react"
import Wrapper from 'components/layout/wrapper'
import SocialIcons from "components/header/SocialIcons";

export default function Comp2021HomeSocial() {

    const theme = useTheme()

    return (
        <Box
            bg={theme.colors.color4 + " url(https://assets.crfconnect.com/meetings/tct2021/homepage/TCT2021_social_media_bkg_desktop_01.jpeg) center no-repeat"}
            backgroundSize="cover"
            p="50px 0 50px"
        >
            <Center>
                <Box textAlign="center">
                    <Heading as="h2" color="color2" fontWeight={600} fontSize={42} my={0}>Join the Conversation</Heading>
                    <Text color="color2" fontSize="28px" fontWeight={600} mt={0} mb={2}>#CompAtTCT2021</Text>
                    <Flex justify="center">
                        <SocialIcons color="color2" size={{ base: "34px", lg: "40px"}} padding={4}/>
                    </Flex>
                </Box>
            </Center>
        </Box>
    )
}