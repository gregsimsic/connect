import {Flex, Box, Center, Link, Button, AspectRatio, useTheme, Image, useBreakpointValue} from "@chakra-ui/react"
import { ArrowForwardIcon } from '@chakra-ui/icons'
import Wrapper from 'components/layout/wrapper'

import {useGlobalState} from "src/providers/globalState"

export default function Tct2021HomeTop() {

    const theme = useTheme()
    const {site} = useGlobalState()

    const videoSrc = useBreakpointValue({
        base: "https://assets.crfconnect.com/meetings/tct2021/homepage/TCT2021_FINAL_mobile_02.mp4",
        md: "https://assets.crfconnect.com/meetings/tct2021/homepage/TCT2021_FINAL_02_no_logo.mp4"
    })


    return (
        <Flex position="relative" overflow="hidden">
            <AspectRatio ratio={{base: 3/3, lg: 1200/500, xxl: 1200/360}} width="100%" maxHeight="480px">
                <Box as="video"
                    autoPlay muted playsInline loop
                    src={videoSrc}
                    poster={{
                        base: "https://assets.crfconnect.com/meetings/tct2021/homepage/placeholder.jpeg",
                        lg: "https://assets.crfconnect.com/meetings/tct2021/homepage/placeholder.jpeg"
                    }}
                ></Box>
            </AspectRatio>

            <Box
                pos="absolute"
                bottom={0}
                right={{base: "10px", md: "30px"}}
                maxWidth={{base: "45vw", md: "35vw", lg: "100%"}}
                zIndex={10}
            >
                <Link href="https://www.tctconnect.com/" target="_blank" >
                    <img src="https://assets.crfconnect.com/meetings/tct2021/homepage/TCT2020-button.png" alt="TCT 2020" />
                </Link>
            </Box>

            <Center
                position="absolute"
                w="100%" h="100%"
                top={0} left={0}
            >
                <Image
                    src="https://assets.crfconnect.com/meetings/tct2021/homepage/TCT2021_desktop_logo_01.png"
                    alt="TCT 2021"
                    maxWidth={{base: "80%", lg: "80%"}}
                    maxHeight={{base: "80%", lg: "80%"}}
                />
            </Center>
        </Flex>
    )
}