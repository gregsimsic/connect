import {useState} from "react";

import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Heading,
    Text,
    useTheme
} from "@chakra-ui/react";

import SessionGroup from './sessionGroup'

export default function Sessions(props) {

    const theme = useTheme()

    const groupedSessions = groupEventsBySessionType(props.events)

    const defaultOpenAccordionIndexes = props.index === 0 ? [0] : []

    function groupEventsBySessionType(events) {

        const eventsGroupedBySessionType = _.groupBy(events, 'session_type')

        return _.map(eventsGroupedBySessionType, events => {
            return {
                session_type: events[0].session_type,
                color: events[0].color,
                sessions: events
            }
        })

    }

    return (
        <>
            {groupedSessions.length ?
                <Accordion allowToggle allowMultiple defaultIndex={defaultOpenAccordionIndexes}>
                    {groupedSessions.map((sessionGroup, i) => {
                        return (
                            <AccordionItem border={0} key={i}>
                                <Heading as="h2"
                                         mt={0}
                                >
                                    <AccordionButton
                                        bg={sessionGroup.color}
                                        py={2} px={4}
                                        mt={1}
                                        border={0}
                                        _hover={{bg: "#333"}}
                                        _focus={{outline: 0}}
                                    >
                                        <Box flex="1"
                                             textAlign="left"
                                             color="white"
                                             fontSize={17}
                                             fontWeight="600"
                                             textTransform="uppercase"
                                        >
                                            {sessionGroup.session_type}
                                        </Box>
                                        <AccordionIcon color="#fff" w={7} h={7}/>
                                    </AccordionButton>
                                </Heading>
                                <AccordionPanel p={0}>
                                    <Box>
                                        <SessionGroup key={i} {...sessionGroup} index={i} type={props.type}/>
                                    </Box>
                                </AccordionPanel>
                            </AccordionItem>
                        )
                    })}
                </Accordion>
                : <Text color="#333">No results found.</Text>
            }
        </>
    )
}
