import {Box, Button, ButtonGroup, Flex, Icon, Text} from "@chakra-ui/react";
import {Download} from "react-feather";

export default function DownloadLinks({showDownloadLinks, programDownloadCsv, programDownloadPdf}) {
    
    if (!showDownloadLinks) return null

    return (
        <>
            {programDownloadCsv || programDownloadPdf ? (
                <Flex alignItems="center">
                    <Text fontWeight={600} color="color1" m="0 10px 0 0">Download Program: </Text>
                    <ButtonGroup size="sm" variant="outline" color="color1" isAttached>
                        {programDownloadCsv && (
                            <Button as="a" target="_blank" href={programDownloadCsv} rightIcon={<Icon as={Download} w="16px" h="16px"/>}>Excel</Button>
                        )}
                        {programDownloadPdf && (
                            <Button as="a" target="_blank" href={programDownloadPdf} rightIcon={<Icon as={Download} w="16px" h="16px"/>}>PDF</Button>
                        )}
                    </ButtonGroup>
                </Flex>
            ) : ( null )}

        </>
    )

}


