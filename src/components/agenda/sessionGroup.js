import React from "react";

import {Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box, Heading} from "@chakra-ui/react";

import Session from './session'
import Wrapper from "components/layout/wrapper";
import Highlighter from "components/agenda/highlighter";

export default function SessionGroup(props) {

    const defaultOpenAccordionIndexes = props.index === 0 ? [0] : []

    return (
        <Box>
            <Accordion allowToggle allowMultiple defaultIndex={defaultOpenAccordionIndexes}>
                {props.sessions.map((session, i) => {
                    return (
                        <AccordionItem borderTop={0} key={i}>
                            <Heading as="h2" mt={0}>
                                <AccordionButton
                                    role="group"
                                    _focus={{outline: 0}}
                                    _hover={props.sessionTypeCount === 1 ? null : {bg: "#eee"}}
                                    py={2}
                                    pl={props.sessionTypeCount === 1 ? 0 : 4}
                                    pr={4}
                                    mt={1}
                                >
                                    <Box flex="1"
                                         textAlign="left"
                                         color="#333"
                                         fontSize={16}
                                         _groupHover={props.sessionTypeCount === 1 ? {color: props.color} : null}
                                    >
                                        <Highlighter text={session.title} />
                                    </Box>
                                    <AccordionIcon color="#333" w={7} h={7}/>
                                </AccordionButton>
                            </Heading>
                            <AccordionPanel pb={4} px={0}>
                                <Box p="5px 0 20px 0" >
                                    <Session key={i} {...session} type={props.type} color={props.color}/>
                                </Box>
                            </AccordionPanel>
                        </AccordionItem>
                    )
                })}
            </Accordion>
        </Box>
    )

}
