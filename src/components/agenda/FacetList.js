import {
    Box,
    Flex,
    VStack,
    Link,
    Heading,
    Checkbox,
    CheckboxGroup,
    Accordion,
    AccordionItem, AccordionIcon, AccordionButton, AccordionPanel
} from "@chakra-ui/react";

export default function FacetList({agenda, facets, openFacet, handleFilterGroupChange, activeFilters}) {

    return (
        <Box>
            <Accordion defaultIndex={[0]} allowToggle>
                <AccordionItem border={0} >
                    <Heading as="h3"
                             bg="#ddd"
                             mt={0}
                    >
                        <AccordionButton
                            border={0}
                            _focus={{outline: 0}}
                        >
                            <Box flex="1"
                                 textAlign="left"
                                 color="#333"
                                 fontSize={14}
                                 fontWeight="600"
                                 py={3} pl={2} pr={1}
                            >
                                SELECT FILTER
                            </Box>
                            <AccordionIcon color="#333" w={7} h={7}/>
                        </AccordionButton>
                    </Heading>
                    <AccordionPanel p={0}>
                        <Box p="10px 5px 20px 20px" color="#333">
                            {facets.map((facet, i) => {

                                if (facet.filters.length < 2) return false

                                return (
                                    <Box key={i} mb={5}>
                                        <Heading as="h5" fontSize="14px" m="12px 0 5px">{facet.label}</Heading>
                                        <CheckboxGroup
                                            onChange={(value) => handleFilterGroupChange(value, facet)}
                                            value={activeFilters[facet.key]}
                                            colorScheme="facebook"
                                        >
                                            {/*<CheckboxGroup onChange={(value) => console.log(value)} value={activeFilters[facet.key]}>*/}
                                            <VStack spacing={0} align="stretch" >
                                                {facet.filters.map((filter, j) => {
                                                    return (
                                                        <Checkbox key={j}
                                                                  size="md"
                                                                  value={filter.value}
                                                                  borderColor="color12"
                                                            // onChange={(evt) => console.log('change', evt.target.value)}
                                                        >
                                                            {filter.label}
                                                        </Checkbox>
                                                    )
                                                })}
                                            </VStack>
                                        </CheckboxGroup>
                                    </Box>
                                )
                            })}
                        </Box>
                    </AccordionPanel>
                </AccordionItem>
            </Accordion>
        </Box>
    )
}
