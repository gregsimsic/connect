import {Text, Link} from "@chakra-ui/react";
import Highlighter from "components/agenda/highlighter";
import {useGlobalState} from "providers/globalState";

export default function ModeratorList({moderators, label, labelPlural}) {

    label = (label === "Lecturer") ? "Presenter" : label
    labelPlural = (label === "Lecturers") ? "Presenters" : labelPlural

    return (
        <Text m={0} color="#888" lineHeight="1.4">
            <Text as="span" fontWeight={600} pr={3}>
                {moderators.length > 1 ? labelPlural : label}
            </Text>
            <NameList users={moderators}/>
        </Text>
    )
}

function NameList({users}) {
    return users.map((user, i) => {
        return (
            <Text as="span" key={user.id}>
                <ConditionalLink user={user}>
                    <Text as="span"><Highlighter text={user.full_name} /></Text>
                </ConditionalLink>
                {i < users.length-1 && ', '}
            </Text>
        )
    })
}

function ConditionalLink({user, children}) {

    const {site} = useGlobalState()

    if (!site.meeting.enableFacultyProfileLinks) return children

    return (
        <Link href={`/faculty/${user.first_name}-${user.last_name}-${user.id}`} textDecoration="underline" isExternal>
            {children}
        </Link>
    )
}
