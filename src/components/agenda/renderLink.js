import {Box, Icon, Link} from "@chakra-ui/react";
import {ExternalLink, PlayCircle} from "react-feather";
import {isSameDay} from "date-fns";

import specialCases from "config/special";

export default function RenderLink({event, channelSlug, site, children}) {

    let href = '',
        icon = PlayCircle

    // Link to a special TCTMD page if we have an override for it
    if (specialCases[site.meeting.emmaMeetingId]?.alternateLinks?.[event.id]) {
        href = specialCases[site.meeting.emmaMeetingId].alternateLinks[event.id]
        icon = ExternalLink
    } else {
        href = determineLink(event, channelSlug)
    }

    // no link, no dice
    if (!href) return children

    return (
        <Link href={href} textDecoration="underline" isExternal>
            {children}
            <Box display="inline" pl={2} verticalAlign="middle"><Icon as={icon} w="24px" h="24px"/></Box>
        </Link>
    )

}

function determineLink(event, channelSLug) {

    const videoLink = event.links?.find(link => link.type === 'video' && link.id)

    // Link to the ondemand page and event if the Brightcove link is present
    if (videoLink) {
        return "/ondemand/" + channelSLug + "/" + event.id
    }

    // Link to the live channel page if event date is today & it's not on-demand only
    if (event.level === 0 && !event.params.on_demand && isSameDay(new Date(event.datetime_start), new Date())) {
        return "/live/" + channelSLug + "/"
    }

    return false
}