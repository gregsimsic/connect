import {format, utcToZonedTime} from "date-fns-tz";
import {Text} from "@chakra-ui/react";

export default function FormattedDateString({event, config = {}}) {

    const defaults = {
        format: 'h:mm a zzz',
        showEndTime: false
    }

    const settings = {...defaults, ...config}

    // if showing the start and end times remove the timezone from the start time format
    const startTimeFormat = settings.showEndTime ? settings.format.substr(0, settings.format.lastIndexOf(" ")) : settings.format

    const timezoneString = new window.Intl.DateTimeFormat().resolvedOptions().timeZone

    // set a default timezone so this can render on the server
    // const timezoneString = typeof window !== 'undefined'
    //     ? new window.Intl.DateTimeFormat().resolvedOptions().timeZone
    //     : 'America/New_York'

    const zonedStartDate = utcToZonedTime(new Date(event.datetime_start), timezoneString)
    const zonedEndDate = utcToZonedTime(new Date(event.datetime_end), timezoneString)

    let str = format(zonedStartDate, startTimeFormat, { timeZone: timezoneString })

    if (settings.showEndTime) {
        str += " — " + format(zonedEndDate, settings.format, { timeZone: timezoneString })
    }

    // Set am/pm and timezone as small text
    const re = new RegExp("(AM|PM) ?([A-Z]{3})?","g");
    str = str.replace(re, "<small>$1 $2</small>")

    return <Text as="span" dangerouslySetInnerHTML={{__html: str}} />

}
