import {useState} from "react";

import {
    Box, HStack,
    Text,
    Button, IconButton,
    Input, InputGroup, InputRightElement,
    LightMode,
    useBreakpointValue
} from "@chakra-ui/react";

import {CloseIcon, SearchIcon} from "@chakra-ui/icons";
import _ from "lodash";

export default function TextSearchForm({handleTextSearch}) {

    const [searchText, setSearchText] = useState('')
    const [submittedTerms, setSubmittedTerms] = useState([])
    const [errorMessage, setErrorMessage] = useState('')
    const searchTermsMinimumCharacterCount = 2

    const isMobile = useBreakpointValue({ base: true, md: false })

    function handleTextSearchChange(event) {
        setSearchText(event.target.value)
    }

    function handleTextSearchKeyup(event) {
        if (event.keyCode == 13) {
            // allow an empty string to be submitted to clear the search
            const clear = (searchText === '')
            processInput(clear)
        }
    }

    function handleTextSearchSubmit() {
        // if (!searchText) return false
        processInput()
    }

    function processInput(clear = false) {

        // allow an empty string to
        const searchTermsLowercaseArray = (clear)
            ? ['']
            : searchText.toLowerCase().split(" ")

        // don't re-submit the same value
        if (_.isEqual(searchTermsLowercaseArray, submittedTerms)) return false

        // reject short strings
        if (!clear && searchTermsLowercaseArray.some(term => term.length < searchTermsMinimumCharacterCount)) {
            setErrorMessage(`All terms must be at least ${searchTermsMinimumCharacterCount} characters.`)
            return false
        }

        setErrorMessage('')
        setSubmittedTerms(searchTermsLowercaseArray)
        handleTextSearch(searchTermsLowercaseArray)

    }

    function clearTextSearch() {
        setSearchText('')
        processInput(true)
    }

    return (
        <Box>
            <HStack spacing={0}>
                <InputGroup size="md">
                    <LightMode>
                        <Input
                            onChange={handleTextSearchChange}
                            onKeyUp={handleTextSearchKeyup}
                            value={searchText}
                            borderRadius="0"
                            pr={1}
                            color="#333"
                            placeholder="Search titles, faculty or lecturer"
                        />
                    </LightMode>

                    <InputRightElement >
                        {searchText.length &&
                            <IconButton aria-label="Search agenda" h="1.75rem" size="sm" color="#666" icon={<CloseIcon/>} onClick={clearTextSearch}/>
                        }
                    </InputRightElement>
                </InputGroup>
                <Button rightIcon={<SearchIcon />} color="#666" borderRadius="0" px={isMobile ? 3 : 8} onClick={handleTextSearchSubmit}>{isMobile ? '' : 'Search'}</Button>
            </HStack>
            {errorMessage &&
                <Text
                    fontSize="14px"
                    color="error"
                    m="5px 0 0 2px"
                >
                    {errorMessage}
                </Text>
            }
        </Box>
    )

}
