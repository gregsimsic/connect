import React from 'react'
import _ from "lodash";
import {format} from "date-fns";

import {Box, Heading, Text} from "@chakra-ui/react";

import Sessions from "components/agenda/sessions";
import SessionGroup from 'components/agenda/sessionGroup'

function ResultsByDay({agenda, channels, sessionTypeCount}) {

    const eventsGroupedByDayField = _.groupBy(agenda, 'day')

    // split agenda into live and ondemand sessions
    const liveAndOndemandPartition = _.partition( eventsGroupedByDayField, group => !group[0].params.on_demand )

    // re-concatenate with ondemand at the end
    const liveAndOndemandGroups = liveAndOndemandPartition[0].concat(liveAndOndemandPartition[1])

    // add labels to groups, either the date or 'on demand', to be used as accordion labels
    const groups = _.map(liveAndOndemandGroups, events => {
        const isOnDemand = events[0].params.on_demand
        return {
            label: isOnDemand ? "On Demand" : format(new Date(events[0].datetime_start), 'EEEE, MMMM d, Y'),
            events: events
        }
    })

    return (
        <>
            { groups.length
                ? groups.map((group, i) => (
                    <Box key={i} mb={8}>
                        <Heading as="h3"
                                 fontSize={17}
                                 textTransform="uppercase"
                                 color="color5"
                                 mt={1}
                                 pr={1}
                                 pb={0.5}
                        >
                            {group.label}
                        </Heading>
                        {sessionTypeCount === 1
                            ? <SessionGroup key={i} sessions={group.events} color={group.events[0].color} sessionTypeCount={sessionTypeCount} index={i} />
                            : <Sessions {...group} key={i} index={i}/>
                        }

                    </Box>
                ))
                : <Text color="#333">No results found.</Text>
            }
        </>
    )

}

export default React.memo(ResultsByDay)