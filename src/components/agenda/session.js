import {Box, Heading, Text, Button, HStack} from "@chakra-ui/react";
import FormattedDateString from "components/agenda/formattedDateString";
import Presentation from "./presentation";
import {useGlobalState} from "../../providers/globalState";

export default function Session(props) {

    const {site} = useGlobalState()

    function printSession() {
        let url = "/program-guide/print/session/" + props.id
        url += props.type === "ondemand" ? "?type=ondemand" : ""
        const newWindow = window.open(url)
        newWindow.print()
    }

    return (
        <Box p={4} bg="#eee">
            {/*<Heading as="h3" size="lg" color="#333" mt={1}>{props.title}</Heading>*/}

            {site?.meeting.type !== "Virtual" && props.type !== "ondemand" && !props.params.on_demand && (
                <Box m="5px 0 25px" >
                    <Text m={0} color="#333">
                        <FormattedDateString event={props} config={{showEndTime: true}}/>
                    </Text>
                    <Text m={0} color="#333">Room: {props.venue?.name}</Text>
                </Box>
            )}

            {props.children.map((event, i) => {
                return <Presentation key={i} {...event} level={0} type={props.type} suppressLinks={props.suppressLinks} color={props.color}/>
            })}

            <Button
                onClick={printSession}
                className="no-print"
                color="#eee"
                bg={props.color}
                borderRadius="0"
                px={9} py={1} mt={4}
                fontSize="14px"
                _hover={{bg: props.color}}
            >
                PRINT
            </Button>

        </Box>
    )

}
