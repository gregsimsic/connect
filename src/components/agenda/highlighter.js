import {Text} from "@chakra-ui/react";
import {useGlobalState} from "../../providers/globalState"

export default function Highlighter({text, style = {}}) {

    const {site} = useGlobalState()

    const searchTerms = site?.search?.terms

    if (!searchTerms?.length) return text

    // split string by search terms AND keep the delimiters (search terms)
    const gluedTerms = searchTerms.join("|")
    const regExpString = "\(" + gluedTerms + "\)"
    const re = new RegExp(regExpString,"i");
    const textParts = text.split(re);

    return (
        <>
            {textParts.map((part, i) => {
                return searchTerms.includes(part.toLowerCase())
                ? <Text as="span"
                        bgColor="#ffff00"
                        {...style}
                        key={i}
                >
                    {part}
                </Text>
                : part
            })}
        </>
    )

}


