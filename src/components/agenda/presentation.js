import {Flex, Box, Heading, Text, Link, Icon} from "@chakra-ui/react";
import {isSameDay} from "date-fns";
import ModeratorList from "./moderatorList"
import {PlayCircle, ExternalLink} from "react-feather";
import {useGlobalState} from "src/providers/globalState"
import Highlighter from "components/agenda/highlighter";
import FormattedDateString from "components/agenda/formattedDateString";
import RenderLink from "components/agenda/renderLink";

export default function Presentation(props) {

    const {site} = useGlobalState()

    const ConditionalLink = function({children}) {

        if (props.suppressLinks || !site?.meeting.isLive || props.title === "Break") return children

        return <RenderLink event={props} channelSlug={props.channelSlug} site={site}>{children}</RenderLink>

    }

    return (
        <Box mt={4} mb={(props.level && !props.children.length) ? 4 : 12}  >

            <Flex align="baseline">
                {(props.level && !props.params.on_demand && !props.children.length) ? (
                    <Text mr="4" mt={0} minW={70} color="#333">
                        <FormattedDateString event={props} />
                    </Text>
                ) : null }

                <Box>
                    <Heading as="h4"
                             size={ props.level ? 'sm' : 'md' }
                             fontWeight={ (props.level && !props.children.length) ? 'normal' : '600' }
                             mt="1" mb="1"
                             color={ props.level ? '#333' : props.color }
                    >
                        <ConditionalLink>
                            <Highlighter text={props.title} />
                        </ConditionalLink>
                    </Heading>
                    {props.description && (
                        <Text
                            fontSize={15}
                            lineHeight={1.2}
                            mt={1}
                            mb={3}
                            color={props.color}
                        >
                            {props.description}
                        </Text>
                    )}
                    {/*<Heading as="h4" size={ props.level ? 'sm' : 'md' } fontWeight={ props.level ? 'normal' : '600' } mt="1" mb="1" color={ props.level ? '#333' : "color2" }>{props.title}</Heading>*/}

                    {props.type !== "ondemand" ?
                        !props.children.length ?
                            !props.level ? (
                                <Text mt={0} color="#333">
                                    <FormattedDateString event={props} />
                                </Text>
                            ) : null
                         : null
                    : null }

                    {props.moderators.map((moderatorGroup, i) => {
                        return (
                            <ModeratorList
                                moderators={moderatorGroup.users}
                                label={moderatorGroup.role.name}
                                labelPlural={moderatorGroup.role.name_plural}
                                key={i}
                            />
                        )

                    })}
                </Box>
            </Flex>

            {props.children.map((event, i) => {
                return <Presentation key={i} {...event} level={props.level+1} type={props.type} suppressLinks={props.suppressLinks}/>
            })}

        </Box>
    )

}
