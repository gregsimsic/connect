import {useEffect, useState} from "react";
import _ from "lodash"
import {format} from "date-fns";

import {
    Box, Center, Flex, Text,
    Modal, ModalBody, ModalContent, ModalOverlay, Spinner, Link, ButtonGroup, Button, Icon,
} from "@chakra-ui/react";
import {Download, PlayCircle} from "react-feather";

import {useConferenceEvents} from "services/emma/emma";
import {useConference} from "services/emma/emma";

import ResultsByDay from "./resultsByDay";
import TextSearchForm from "components/agenda/textSearchForm";
import FacetList from "./FacetList";
import LoadingIndicator from "components/ui/loadingIndicator";
import Sessions from "components/agenda/sessions";
import Wrapper from "components/layout/wrapper";
import {useGlobalState} from "../../providers/globalState";
import DownloadLinks from "components/agenda/downloadLinks";

export default function FilterableAgenda({
                                             type,
                                             site,
                                             channels,
                                             showSearch=true,
                                             showFilters=true,
                                             showDownloadLinks=true,
                                             sessionTypeCount=0,
                                             emmaParams = {},
                                             eventsConfig = {}
}) {

    const [facets, setFacets] = useState([]);
    const [agenda, setAgenda] = useState();
    const [results, setResults] = useState();
    const [activeFilters, setActiveFilters] = useState([]);
    const [searchTerms, setSearchTerms] = useState([]);
    const [isFiltering, setIsFiltering] = useState(false);
    const [isInitialized, setIsInitialized] = useState(false);
    const [hasFilters, setHasFilters] = useState(true);

    const { status, data, error, isFetching } = useConferenceEvents(
        site.meeting.emmaMeetingId,
        emmaParams,
        eventsConfig
    );

    const { data: conference } = useConference(site.meeting.emmaMeetingId);

    useEffect(() => {
        if (isInitialized || !data || !conference) return false

        // attach meeting to global site state so it can be retrieved by the session component
        site.meeting.type = conference.type
        setAgenda(data)
        setIsInitialized(true)

    }, [data, conference])

    // setup the facets and channel slugs
    useEffect(() => {

        if (!data || !channels) return false

        if (showFilters) {

            const facetsConfig = [
                {
                    key: 'days',
                    label: 'Select Date',
                    type: 'multi',
                    generateFilters: (agenda) => { return _.uniqBy(agenda.map(event => {
                        return {
                            value: event.day,
                            label: format(new Date(event.datetime_start), 'EEEE, MMMM d'),
                            facet: 'days'
                        }
                    }), (event) => event.value ).filter(event => event.value !== "On Demand")},
                    filterFunction: (events, filterGroup) => {

                        return events.filter(event => {
                            return filterGroup.some(filter => {
                                return event.day === filter && !event.params.on_demand
                            })
                        })

                    }
                },
                {
                    key: 'channels',
                    label: 'Select ' + (site.meeting.sessionTypeTerm || 'Channel'),
                    type: 'multi',
                    generateFilters: (agenda) => { return _.uniqBy(agenda.map(event => {
                        return {
                            value: "session-" + event.session_type_id,
                            label: event.session_type,
                            facet: 'channels'
                        }
                    }), (event) => event.value )},
                    filterFunction: (events, filterGroup) => {

                        return events.filter(event => {
                            return filterGroup.some(filter => {
                                return "session-" + event.session_type_id === filter
                            })
                        })

                    }
                },
                {
                    key: 'tracks',
                    label: 'Select ' + (site.meeting.trackTerm || 'Track'),
                    type: 'multi',
                    generateFilters: (agenda) => { return _.uniqBy(agenda.flatMap(event => {
                        return event.tracks.map(track => {
                            return {
                                value: track,
                                label: track,
                                facet: 'tracks'
                            }
                        })
                    }), (item) => item.value )},
                    filterFunction: (events, filterGroup) => {

                        return events.filter(event => {
                            return filterGroup.some(filter => {
                                return event.tracks.some(track => {
                                    return track === filter
                                })
                            })
                        })

                    },
                }
            ]

            let atLeastOneFacetHasMoreThanOneFilter = false

            const facetsWithGeneratedFilterItems = facetsConfig.map(facet => {

                const filters = facet.generateFilters(agenda)
                facet.filters = filters

                if (filters.length > 1) atLeastOneFacetHasMoreThanOneFilter = true

                return facet

            })

            setFacets(facetsWithGeneratedFilterItems)
            setHasFilters(atLeastOneFacetHasMoreThanOneFilter)

        } else {
            setHasFilters(false)
        }

        // add the channel slug to events so they can be used to generate links to the on-demand channel pages
        // TODO: re-visit where the best place to add this data point is
        agenda.map(event => {
            const eventChannel = getChannel(event)
            event.color = eventChannel.color

            event.children.map(section => {
                const channel = getChannel(section)

                section.channelSlug = channel.slug
                // section.channelColor = channel.color

                section.children.map(presentation => {
                    presentation.channelSlug = channel.slug
                })

            })
        })

        setResults(agenda)

    }, [agenda])

    useEffect(() => {

        doSearch()

    }, [activeFilters, searchTerms])

    function renderResults(type) {
        switch (type) {
            case "ondemand" :
                return <Sessions events={results} index={0} type="ondemand"/>
            default:
                return <ResultsByDay agenda={results} sessionTypeCount={sessionTypeCount}/>
        }
    }

    function getChannel(event) {
        const channel = channels.find(channel => parseInt(channel.sessionTypeID, 10) === event.session_type_id)
        return channel ? {
            slug: channel.slug,
            color: channel.color1
        } : {
            slug: null,
            color: null
        }
    }

    function findTermsInEvent(event) {

        // search event title
        // if (event.title.toLowerCase().includes(string)) return true
        if (searchTerms.some(term => event.title.toLowerCase().includes(term))) return true

        // search event faculty
        const found = event.moderators.some(role => {
            return role.users.some(moderator => {
                // return moderator.full_name.toLowerCase().includes(string)
                return searchTerms.some(term => moderator.full_name.toLowerCase().includes(term))

            })
        })
        if (found) return true

        // search children recursively
        if (event.children.length) {
            const foundChild = event.children.some(childEvent => {
                return findTermsInEvent(childEvent)
            })
            if (foundChild) return true
        }

        // found nothing
        return false

    }

    function doSearch() {

        let newResults = agenda

        // Apply filters
        _.forIn(activeFilters, function(filterGroup, key) {

            if (filterGroup.length === 0) return true

            const facet = _.find(facets, { 'key': key} )

            newResults = facet.filterFunction(newResults, filterGroup)
        });

        // Apply text search
        if (searchTerms.length && searchTerms[0] !== '') {

            newResults = newResults.filter(event => {
                return findTermsInEvent(event)
            })

        }

        setResults(newResults)

        setIsFiltering(false)

    }

    function handleFilterGroupChange(value, facet) {

        setIsFiltering(true)

        const valueArray = Array.isArray(value) ? value : [value]

        // this delay is implemented only to improve UI feedback
        // without it there is an odd delay due to react not re-rendering
        setTimeout(() => {
            // works for multi-select, possibly needs to handle single select
            setActiveFilters({
                ...activeFilters,
                [facet.key]: valueArray
            })
        }, 300)

    }

    function handleSearchTextChange(value) {

        // don't repeat the same search
        if (_.isEqual(searchTerms, value)) return false

        // set the search term on the global site object so it's available to the Highlight component
        site.search = {terms: value}

        setIsFiltering(true)
        setSearchTerms(value)
    }

    return (
        <Box>
            {showSearch && (
                <Box mb={4}>
                    <TextSearchForm handleTextSearch={handleSearchTextChange} />
                </Box>
            )}

            <DownloadLinks
                showDownloadLinks={showDownloadLinks}
                programDownloadCsv={site.meeting.programDownloadCsv}
                programDownloadPdf={site.meeting.programDownloadPdf}
            />

            <Flex mt={12} direction={{base: 'column', md: 'row'}}>

                {hasFilters && (
                    <Box
                        w={{base: '100%', md: '330px'}}
                        mr={{base: 0, md: '50px'}}
                        bg="#efefef"
                        mt={1}
                    >
                        <FacetList
                            agenda={agenda}
                            facets={facets}
                            handleFilterGroupChange={handleFilterGroupChange}
                            activeFilters={activeFilters}
                        />
                    </Box>
                )}

                <Box w={{base: '100%'}} >
                    {isInitialized && results
                        ? renderResults(type)
                        : <LoadingIndicator />
                    }
                </Box>

                {isFiltering && (
                    <Modal isOpen={true} size="xs" isCentered motionPreset="none">
                        <ModalOverlay />
                        <ModalContent>
                            <ModalBody>
                                <Center>
                                    <Flex direction="column" align="center" py={8}>
                                        <Spinner
                                            thickness="3px"
                                            speed="0.45s"
                                            emptyColor="gray.200"
                                            color="color2"
                                            size="lg"
                                        />
                                        <Text color="#999">Filtering...</Text>
                                    </Flex>
                                </Center>
                            </ModalBody>
                        </ModalContent>
                    </Modal>
                    ) }
            </Flex>
        </Box>
    )

}
