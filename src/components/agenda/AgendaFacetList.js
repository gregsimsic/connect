import {Box, Flex, Link, Icon} from "@chakra-ui/react";

import {ChevronDown, ChevronUp} from "react-feather";

export default function AgendaFacetList({items, openFacet, toggleOpen}) {

    return (
        <Flex
            p="10px 10px 10px 0"
        >
            {items.map((item, i) => {

                const icon = (openFacet && openFacet.key === item.key) ? ChevronUp : ChevronDown

                return (
                    <Box key={i} mr={6}>
                        <Flex as="a"
                              onClick={() => toggleOpen(item.key)}
                              fontWeight="bold"
                              align="center"
                              cursor="pointer"
                            // p={2}
                            // bg={open ? '#ccc' : 'transparent'}
                        >
                            {item.label} <Icon as={icon} ml="8px" w="18px" h="18px"/>
                        </Flex>
                    </Box>
                )
            })}
        </Flex>
    )
}
