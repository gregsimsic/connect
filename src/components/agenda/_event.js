import React from "react";
import {format} from "date-fns";

import {Box, Heading, Text, CircularProgress, useTheme} from "@chakra-ui/react";

import Presentation from "./presentation"

export default function Event(props) {

    const theme = useTheme()

    // console.log(props)

    return (
        <Box>
            <Heading as="h3">{props.title}</Heading>

            <Text>{format(new Date(props.datetime_start), 'EEEE, MMMM d, Y')}</Text>
            <Text>{format(new Date(props.datetime_start), 'h:mm a')} - {format(new Date(props.datetime_end), 'h:mm a')}</Text>

            {props.children.map((event, i) => {
                return <Presentation key={i} {...event} level={0}/>
            })}

        </Box>
    )

}
