import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import _ from 'lodash'

import { Box, List, ListItem, Link, Text } from "@chakra-ui/react"
import Wrapper from "components/layout/wrapper";

export default function Breadcrumbs({flatmenu}) {

    const router = useRouter();
    const [breadcrumbs, setBreadcrumbs] = useState([]);

    function findParent(item, array) {

        array = typeof array === 'undefined' ? [] : array

        if (item.parent === null) {
            return array
        } else {
            const parent = _.find(flatmenu, ['id', item.parent.id])
            array.push(parent)
            return findParent(parent, array)
        }
    }

    useEffect(() => {

        if (router) {
            const urlSegments = router.asPath.split('/')

            const lastSegment = urlSegments.pop()

            // TODO: this will not work for a faculty detail page (and others) because the last URL segment does not exist in the menu tree (it's a dynamic URL)
            // grab the current page from the menu based on the final URL segment
            const thisPage = _.find(flatmenu, (menuItem) => {
                return menuItem.page?.length
                    ? menuItem.page[0].slug === lastSegment
                    : menuItem.slug === lastSegment
            })

            let crumbsArray = []

            // if we found a page, then build the crumbs
            if (thisPage) {
                crumbsArray = findParent(thisPage)
            }

            // add in the home page
            crumbsArray.push({
                'id': 0,
                'title': 'Home',
                'slug': '/'
            })

            crumbsArray = crumbsArray.reverse()

            // if we found a page, then add it to the crumbs
            if (thisPage) {
                crumbsArray.push(thisPage)
            }

            setBreadcrumbs(crumbsArray)
        }
    }, [router])

    if (!breadcrumbs) {
        return null;
    }

    return (
        <Box bg="color12" color="color10">
            <Wrapper>
                <nav aria-label="breadcrumbs">
                    <List py={2}>
                        {breadcrumbs.map((breadcrumb, i) => {
                            return (
                                <ListItem
                                    key={breadcrumb.id}
                                    display='inline-block'
                                    fontWeight='400'
                                >
                                    { i === 0 ? '' : <Text as="span" px={3}>></Text>}
                                    {breadcrumb.slug === '/'
                                        ? <Link href="/">{breadcrumb.title}</Link>
                                        : <Text as="span">{breadcrumb.title}</Text>
                                    }
                                </ListItem>
                            )
                        })}
                    </List>
                </nav>
            </Wrapper>
        </Box>
    );
};
