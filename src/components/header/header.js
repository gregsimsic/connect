import {Flex, Spacer, Box, List, ListItem, Link, Icon, Container, Image, useDisclosure, useTheme} from "@chakra-ui/react"
import { X, Menu } from "react-feather"

import Nav from "../nav/nav";

export default function Header({site}) {

    const { isOpen, onToggle } = useDisclosure({defaultIsOpen: false})

    const theme = useTheme();

    return (
        <Flex bg="color2" maxW="100%" h={theme.header.h} zIndex={11}>
            <Flex w="100%" align="center">
                <Box pl="10px" mr="10px">
                    <Link href="/" >
                        <Image h={{base:"20px", md:"24px"}} src={site.theme.meetingLogo[0]?.url} alt={site.meeting.meetingName} />
                    </Link>
                </Box>
                <Spacer />
                <Flex as="a"
                    justify="center"
                    h="100%"
                    direction="column"
                    display={{ base: "block", menu: "none" }}
                    onClick={onToggle}
                    pr="10px"
                    cursor="pointer"
                >
                    {isOpen ? <Icon as={X} w="24px" h="100%" verticalAlign="-webkit-baseline-middle"/> : <Icon as={Menu} w="24px" h="100%" verticalAlign="-webkit-baseline-middle"/>}
                </Flex>
                <Box
                    display={{ base: isOpen ? "block" : "none", menu: "block" }}
                    position={{ base: "fixed", menu: "relative"}}
                    top={{ base: "44px", sm: "86px", md: "104px", menu: "unset"}}
                    left={{ base: 0, menu: "unset"}}
                    h={{ base: "100vh", menu: "auto" }}
                    w={{ base: "100%", menu: "auto"}}
                    bg="color2"
                    zIndex="10"
                >
                    <Nav menu={site.menu}/>
                </Box>
            </Flex>
        </Flex>
    )

}
