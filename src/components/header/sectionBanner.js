import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import _ from 'lodash'

import {Box, Heading} from "@chakra-ui/react"
import Wrapper from "components/layout/wrapper";

export default function SectionBanner({flatmenu, theme, bgColor, bgImage, label}) {

    const router = useRouter();
    const [bannerLabel, setBannerLabel] = useState('');
    const [bannerBg, setBannerBg] = useState('');

    function findParent(item, array) {

        array = typeof array === 'undefined' ? [] : array

        if (item.parent === null) {
            return array
        } else {
            const parent = _.find(flatmenu, ['id', item.parent.id])
            array.push(parent)
            return findParent(parent, array)
        }
    }

    useEffect(() => {

        let menuSection = null

        if (router) {
            const urlSegments = router.asPath.split('/')
            const lastSegment = urlSegments.pop()

            // grab the current page from the menu based on the final URL segment
            const thisPage = _.find(flatmenu, (menuItem) => {
                return menuItem.page?.length
                    ? menuItem.page[0].slug === lastSegment
                    : menuItem.slug === lastSegment
            })

            // if we found a page, then find its parents
            if (thisPage) {
                let parents = findParent(thisPage)

                // catch for top-level pages: if there is no parent, then use the current page info
                parents = parents.length ? parents : [thisPage]

                menuSection = parents.pop()
            }
        }

        setBannerLabel(label || menuSection?.title || '')

        let bannerBgImage = bgImage
        if (!bannerBgImage) {
            bannerBgImage = (menuSection && menuSection.backgroundImage?.length)
                ? menuSection.backgroundImage[0]?.url
                : theme.backgroundImage[0]?.url
        }

        const bannerBgColor = bgColor || theme.colorPalette[0].color2

        setBannerBg(bannerBgColor + ` url(${bannerBgImage})`)

    }, [router])

    return (
        <Box as="header" bg={bannerBg} bgSize="cover" bgPosition="top right">
            <Wrapper py="52px">
                <Heading
                    as="h3"
                    mt={0}
                    fontFamily="canada-type-gibson"
                    fontWeight={600}
                    fontSize={40}
                    textTransform="uppercase"
                >
                    {bannerLabel}
                </Heading>
            </Wrapper>
        </Box>
    );
};
