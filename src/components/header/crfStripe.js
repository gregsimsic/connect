import { Flex, Spacer, Box, List, ListItem, Link, Icon } from "@chakra-ui/react"
import SocialIcons from "./SocialIcons"
import CrfLogo from "components/graphics/crfLogo"

export default function CrfStripe() {

    const crfSites = [
        {
            label: "CRF",
            url: "http://www.crf.org"
        },
        {
            label: "SKIRBALL",
            url: "https://www.skirballresearch.org/"
        },
        {
            label: "CTC",
            url: "https://www.crf.org/clinical-trials-center"
        },
        {
            label: "CED",
            url: "https://www.crf.org/center-for-education"
        },
        {
            label: "TCTMD",
            url: "https://www.crf.org/center-for-education/tctmd"
        },
        {
            label: "SHJ",
            url: "https://www.crf.org/crf/shj"
        },
    ]

    return (
        <Flex h={42} bg="white" maxW="100%" align="center" display={{ base: "none", sm: "flex" }} zIndex={10}>
            <Box ml="15px" mr="5" css={{ color: "beige" }}>
                <CrfLogo w={70} h={70}/>
            </Box>
            <nav role="navigation">
                <Flex as={List}>
                    {crfSites.map((site, i) => {
                        return (
                            <ListItem key={i}>
                                <Link
                                    href={site.url}
                                    p={2}
                                    color="#9d9d9f"
                                    fontSize="12px"
                                    target="_blank"
                                    _hover={{
                                        color: "#ed2638",
                                        textDecoration: "none"
                                    }}
                                >{site.label}</Link>
                            </ListItem>
                        )
                    })}
                </Flex>
            </nav>
            <Spacer />
            <SocialIcons />
        </Flex>
    )

}
