import { Flex, Link, Icon } from "@chakra-ui/react"
import { Facebook, Twitter, Instagram, Linkedin, Youtube } from "react-feather"

export default function SocialIcons(props) {

    const color = props.color || "color5"
    const hoverColor = props.hoverColor || "color2"
    const size = props.size || "16px"
    const padding = props.padding || 1

    return (
        <Flex mr="15px">
            <Link p={padding}  href="https://twitter.com/CRFHeart" target="_blank" role="group">
                <Icon as={Twitter} w={size} h={size} fill={color} stroke="none" _groupHover={{fill: hoverColor}}/>
            </Link>
            <Link p={padding} color={color} href="https://www.facebook.com/CRFheart/" target="_blank" role="group">
                <Icon as={Facebook} w={size} h={size} fill={color} stroke="none" _groupHover={{fill: hoverColor}}/>
            </Link>
            <Link p={padding} color={color} href="https://www.instagram.com/crfheart/" target="_blank" role="group">
                <Icon as={Instagram} w={size} h={size} stroke={color} _groupHover={{stroke: hoverColor}}/>
            </Link>
            <Link p={padding} color={color} href="https://www.linkedin.com/company/cardiovascular-research-foundation" target="_blank" role="group">
                <Icon as={Linkedin} w={size} h={size} fill={color} stroke="none" _groupHover={{fill: hoverColor}}/>
            </Link>
            <Link p={padding} color={color} href="https://www.youtube.com/channel/UCy4VDgT1jGbdDq6tU3NwoRQ" target="_blank" role="group">
                <Icon as={Youtube} w={size} h={size} stroke={color} _groupHover={{stroke: hoverColor}}/>
            </Link>
        </Flex>
    )

}