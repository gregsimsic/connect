import {Flex, Spinner} from "@chakra-ui/react";

export default function LoadingIndicator(props) {

    const {container, spinner, color = "color2"} = props

    return (
        <Flex
            align="center"
            justify="center"
            height={44}
            {...container}
        >
            <Spinner
                thickness="2px"
                speed="0.65s"
                color={color}
                emptyColor="transparent"
                size="xl"
                {...spinner}
            />
        </Flex>
    )
}

