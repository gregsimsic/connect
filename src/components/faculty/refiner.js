import {Box, Flex, Text, Link} from "@chakra-ui/react";

export default function Refiner({item, toggleActive}) {

    return (
        <Box mb={2}>
            <Link
                fontWeight={item.active ? 'bold' : 'normal'}
                onClick={(evt) => toggleActive(evt, item)}
                p="5px 10px"
                mr={2}
                bg="white"
                color="#000"
                display="block"
                borderRadius={2}
                fontSize="15px"
                _hover={{textDecoration: 'none'}}
            >
                {item.label}
            </Link>
        </Box>
    )

}
