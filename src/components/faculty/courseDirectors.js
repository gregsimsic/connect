import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'
import { CustomPrevArrow, CustomNextArrow } from "../carousel/arrows";

import {Flex, Box, Stack, Center, Link, Text, Image, Icon} from "@chakra-ui/react";
import {Twitter} from "react-feather"

import {useState, useEffect} from 'react'
import {useConferenceSpecialFaculty, useConferenceModeratorRoles} from "services/emma/emma";
import _ from "lodash";
import {format} from "date-fns";

import LoadingIndicator from "components/ui/loadingIndicator";
import {useGlobalState} from "../../providers/globalState";

export default function CourseDirectors({meetingId, moderatorRoles, highlightColor = "color1"}) {

    const {site} = useGlobalState()

    const slickSettings = {
        infinite: false,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        dots: false,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    };

    const { status, data, error, isFetching } = useConferenceSpecialFaculty(meetingId);

    const [results, setResults] = useState([])
    const [roles, setRoles] = useState([])
    const [activeRole, setActiveRole] = useState("")

    useEffect(() => {

        if (!data || !moderatorRoles) return false

        // fish roles out of faculty data
        const existingRoles = _.uniq(data.map(faculty => faculty.role))

        if (existingRoles.length) {

            // filter meeting mod roles according to existingRoles
            moderatorRoles = moderatorRoles.filter(role => {
                return existingRoles.includes(role.name)
            })

            setRoles(moderatorRoles)
        }

        if (!activeRole) {
            setActiveRole(moderatorRoles[0])
        }

    }, [data, moderatorRoles])

    useEffect(() => {

        if (!activeRole) return false

        const filteredFaculty = filterFaculty(activeRole)
        setResults(filteredFaculty)

    }, [activeRole])

    function filterFaculty(role) {
        return data.filter(faculty => faculty.role === role.name)
    }

    return (
        <Box w="100%" m="0 auto">
            {status === "success" ? (
                <>
                    {roles.length > 1 ? (
                        <Stack
                            direction={{base: "column", md: "row"}}
                            spacing={3}
                            m={{base: "0 20px 20px", md: "0 40px 20px", xxl: "0 0 20px -10px"}}
                        >
                            {roles.map((role, i) => (
                                <Box
                                    key={i}
                                    bg={role.id === activeRole.id ? highlightColor :"#fff" }
                                    color={role.id === activeRole.id ? "#fff" :"#333" }
                                    // transform={role === activeRole ? "translate(5px, 0px) scale(1.15, 1.15)" : "none"}
                                    p="18px 60px"
                                    w={{base: "100%", md: "unset"}}
                                    fontWeight={600}
                                    textAlign="center"
                                    onClick={() => setActiveRole(role)}
                                    cursor="pointer"
                                >
                                    {role.name_plural}
                                </Box>
                            ))}
                        </Stack>
                    ) : (null) }
                    <Box m={{base: "0 30px", xxl: "0 0 0 -20px"}}>
                        <Slider {...slickSettings} >
                            {results.map((faculty, i) => {
                                faculty.overlayText = faculty.medical_institution + ", " + faculty.address
                                return (<Slide
                                    key={faculty.id}
                                    {...faculty}
                                    highlightColor={highlightColor}
                                    enableFacultyProfileLinks={site.meeting.enableFacultyProfileLinks}
                                />)
                            })}
                        </Slider>
                    </Box>
                </>
            ) : (
                <LoadingIndicator color="#fff"/>
            )}
        </Box>
    )
}

function ConditionalLink({link, enabled, children}) {

    const boxStyles = { w:"100%", pt:"150%", position:"relative", overflow:"hidden"}

    if (enabled) return <Link href={link} {...boxStyles}>{children}</Link>

    return <Box href={link} {...boxStyles}>{children}</Box>
}

function Slide(slide) {

    return (
        <Flex direction="column" align="center" mx={{base: "5px", md: "10px"}} textAlign="center" role="group" pos="relative">
            <ConditionalLink enabled={slide.enableFacultyProfileLinks} link={"/faculty/" + slide.first_name + "-" + slide.last_name + "-" + slide.faculty_id} >
                <Image
                    src={slide.image_url}
                    fallbackSrc="/person.png"
                    alt={slide.full_name}
                    position="absolute"
                    width="100%" height="100%"
                    top={0} left={0}
                    objectFit="cover"
                    _groupHover={{ filter: 'brightness(0.5)' }}
                />
                <Center
                    display="none"
                    _groupHover={{ display: 'flex', cursor: slide.enableFacultyProfileLinks ? 'pointer' : 'default' }}
                    pos="absolute" top={0} left={0}
                    h="100%" w="100%"
                    p={3}
                >
                    {slide.overlayText}
                </Center>
            </ConditionalLink>
            <Text
                mt="15px"
                fontWeight="600"
                fontSize="13px"
                color="color9"
            >
                {slide.full_name}, {slide.degree}
            </Text>
            {slide.social_twitter && (
                <Box m={0}>
                    <Link href={"https://twitter.com/" + slide.social_twitter} isExternal color={slide.highlightColor} fontSize="13px" _hover={{textDecoration: "none"}}>
                        <Box display="inline" pr={1} ><Icon as={Twitter} w="18px" h="18px" fill={slide.highlightColor} verticalAlign="middle"/></Box>
                        @{slide.social_twitter}
                    </Link>
                </Box>
            )}
        </Flex>
    )
}
