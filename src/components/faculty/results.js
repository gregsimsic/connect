import {Box, Flex, SimpleGrid, Heading, Image, Link, Text} from "@chakra-ui/react";
import {useGlobalState} from "../../providers/globalState";

export default function Results({items}) {

    const {site} = useGlobalState()

    return (
        <SimpleGrid minChildWidth="260px" spacingX="10px" spacingY="20px" mt="40px">
            {items.map((item, i) => {
                return (
                    <Result key={item.id} {...item} enableFacultyProfileLinks={site.meeting.enableFacultyProfileLinks}/>
                )
            })}
        </SimpleGrid>
    )

}

const ConditionalLink = function({link, enabled, children}) {

    if (!enabled) return children

    return (
        <Link href={link} display="flex" _hover={{textDecoration: 'none'}}>
            {children}
        </Link>
    )
}

function Result({full_name, first_name, last_name, medical_institution, image_url, faculty_id, enableFacultyProfileLinks}) {

    const hoverStyles = enableFacultyProfileLinks
        ? { border: "1px solid white" }
        : null

    return (
        <Flex p="10px"
              borderRadius="5px"
              border="1px solid transparent"
              _hover={hoverStyles}
        >
            <ConditionalLink enabled={enableFacultyProfileLinks} link={"/faculty/" + first_name + "-" + last_name + "-" + faculty_id} >
                <Box w="80px" mr="10px" flexShrink={0} >
                    <Image
                        src={image_url}
                        fallbackSrc="/person.png"
                        alt={full_name}
                        w="80px" h="100px"
                        objectFit="cover"
                        borderRadius="5px"
                    />
                </Box>
                <Box pt="2px">
                    <Heading as="h3" size="xs" color="#fff">{full_name}</Heading>
                    <Text fontSize="xs" mt={2}>{medical_institution}</Text>
                </Box>
            </ConditionalLink>
        </Flex>
    )
}
