import {Box, Flex, Text, useBreakpointValue} from "@chakra-ui/react";

import Refiner from "./refiner";

export default function RefinerList({facet, toggleActive}) {

    const isMobile = useBreakpointValue({ base: true, md: false })

    function toggle(evt, item) {
        evt.preventDefault()
        toggleActive({
            facet,
            item
        })
    }

    return (
        <Flex
            p="5px 0 15px"
            wrap="wrap"
        >
            {facet.items.map((item, i) => {
                return (
                    <Refiner key={i} item={item} toggleActive={(evt, itm) => toggle(evt, itm)} />
                )
            })}
        </Flex>
    )

}
