import {Box, Flex, Text, Link, Icon, useBreakpointValue} from "@chakra-ui/react";

import {X} from "react-feather"

import Refiner from "./refiner";

export default function ActiveRefiners({refiners, toggleActive}) {

    const isMobile = useBreakpointValue({ base: true, md: false })

    return (
        <Flex
            p="15px 0 5px"
            wrap="wrap"
            borderTop="2px solid"
        >
            {refiners.map((item, i) => {
                return (
                    <Flex as="a" key={i}
                          onClick={() => toggleActive(item)}
                          p="5px 8px"
                          borderRadius={2}
                          border="1px solid #fff"
                          mr={2}
                          cursor="pointer"
                          align="center"
                    >
                        {item.item.label} <Icon ml="5px" as={X} w="15px" h="15px" />
                    </Flex>
                )
            })}
        </Flex>
    )

}
