import {Box, Flex, Text, Link, Icon, useBreakpointValue} from "@chakra-ui/react";

import {ChevronDown, ChevronUp} from "react-feather";

export default function FacetList({items, toggleOpen}) {

    const isMobile = useBreakpointValue({ base: true, md: false })

    return (
        <Flex
            p="5px 10px"
            direction={isMobile ? 'column' : 'row'}
        >
            {items.map((item, i) => {
                return (
                    <Facet key={i} label={item.label} open={item.active} toggleOpen={toggleOpen} />
                )
            })}
        </Flex>
    )

}

function Facet({label, open, toggleOpen}) {

    const icon = open ? ChevronUp : ChevronDown

    return (
        <Box>
            <Link onClick={toggleOpen}>
                {label} <Icon as={icon} ml="5px" w="18px" h="18px"/>
            </Link>
        </Box>
    )
}
