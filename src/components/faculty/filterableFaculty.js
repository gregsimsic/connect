import {useEffect, useState} from "react";

import _ from "lodash"

import {Box, Flex, Text, useBreakpointValue} from "@chakra-ui/react";

import FacultyFacetList from "./facultyFacetList";
import RefinerList from "./refinerList";
import Results from "./results";
import ActiveRefiners from "./activeRefiners";

export default function FilterableFaculty({items, moderatorRoles}) {

    const initialFacultySet = items

    const facetsData = [
        {
            open: false,
            key: 'abc',
            label: 'A — Z', // this could be a component in order to show an icon or something custom
            // refinersRenderer: <CustomComponent/>,
            type: 'single',
            // items: _.range('A'.charCodeAt(0), 'Z'.charCodeAt(0) + 1).map(
            //     i => {
            //         return { value: String.fromCharCode(i), label: String.fromCharCode(i) }
            //     }
            // ),
            items: _.uniqBy(items.map(item => {
                return { value: item.last_name[0].toUpperCase(), label: item.last_name[0].toUpperCase() }
            }), (item) => item.value ),
            filterFunction: (items, refiner) => {

                return items.filter(item => {
                    return item.last_name.toUpperCase().startsWith(refiner.item.value)
                })

            },

        },
        {
            open: false,
            key: 'role',
            label: 'Role',
            type: 'single',
            items: (() => {

                const existingRoles = _.uniq(items.map(faculty => faculty.role))

                // filter meeting mod roles according to existingRoles from the faculty feed
                moderatorRoles = moderatorRoles.filter(role => {
                    return existingRoles.includes(role.name)
                })

                // setup roles data for UI
                return moderatorRoles.map(role => {
                    return {
                        value: role.name,
                        label: role.name_plural
                    }
                })

            })(),
            itemsOriginal: _.uniqBy(items.map(item => {
                return { value: item.role, label: item.role }
            }), (item) => item.value ),
            filterFunction: (items, refiner) => {

                return items.filter(item => {
                    return item.role === refiner.item.value
                })

            },
        },
        {
            open: false,
            key: 'country',
            label: 'Country',
            type: 'single',
            // items: [
            //     { value: 'United States', label: 'United States' },
            //     { value: 'International', label: 'International' },
            // ]
            items: _.uniqBy(items.map(item => {
                return { value: item.country, label: item.country }
            }), (item) => item.value ),
            filterFunction: (items, refiner) => {

                return items.filter(item => {
                    if (refiner.item.value === "International") {
                        return item.country !== "United States"
                    } else {
                        return item.country === refiner.item.value
                    }
                })

            },
        }
    ]

    const [facets, setFacets] = useState(facetsData);

    const [results, setResults] = useState(initialFacultySet);

    const [openFacet, setOpenFacet] = useState(false);

    const [activeRefiners, setActiveRefiners] = useState([]);

    const isMobile = useBreakpointValue({ base: true, md: false })

    useEffect(async () => {

        let newResults = initialFacultySet

        activeRefiners.map( refiner => {
            newResults = refiner.facet.filterFunction(newResults, refiner)
        })

        setResults(newResults)

    }, [activeRefiners])


    function toggleRefiner(refiner) {

        // blank out old results before setting new results
        // setResults([])

        const refinerIsAlreadyActive = _.some(activeRefiners, (ref) => {
            return ref.item.value === refiner.item.value
        })

        if (refinerIsAlreadyActive) {
            const resolvedRefiners = _.remove(activeRefiners, (ref) => {
                return ref.item.value !== refiner.item.value
            })
            setActiveRefiners(resolvedRefiners)

        } else {

            // if there is a refiner in this set already and it is o type 'single'
            // then remove it first
            if (refiner.facet.type === 'single') {
                const resolvedRefiners = _.remove(activeRefiners, (ref) => {
                    return ref.facet.key === refiner.facet.key
                })
                setActiveRefiners(resolvedRefiners)
            }

            setActiveRefiners( [ ...activeRefiners, refiner ])
        }

        toggleFacet(refiner.facet)
    }

    function toggleFacet(facet) {

        if (openFacet && openFacet.key === facet) {
            setOpenFacet(null)
            return false
        }

        let facetData = _.find(facets, ['key', facet])

        facetData = facetData || false

        setOpenFacet(facetData)
    }

    return (
        <Box>
            <FacultyFacetList items={facets} openFacet={openFacet} toggleOpen={toggleFacet} moderatorRoles={moderatorRoles}/>
            {openFacet ? <RefinerList facet={openFacet} toggleActive={toggleRefiner}/> : null}
            <ActiveRefiners refiners={activeRefiners} toggleActive={toggleRefiner}/>
            <Results items={results}/>
        </Box>
    )

}
