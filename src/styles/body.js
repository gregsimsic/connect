export default {
    h1: {
        fontSize: "34px",
        mt: "35px",
    },
    h2: {
        fontSize: "24px",
        color: "color4",
        lineHeight: 1.2,
        mt: "45px",
        mb: "4",
    },
    h3: {
        fontSize: "20px",
        color: "color1",
        mt: "35px",
    },
    h4: {
        fontSize: "18px",
        color: "color4",
        mt: "20px",
    },
    p: {
        fontSize: "16px",
        fontWeight: 400,
        lineHeight: "1.5",
        mt: "15px",
    },
    a: {
        textDecoration: "none",
        color: "color1"
    },
    ul: {
        pl: "20px",
        mt: "15px",
        ul: {
            mb: "15px"
        }
    },
    li: {
       mt: "5px"
    },
    ".fontLarge": {
        fontSize: "xl"
    },
    ".textCenter, .align-center": {
        textAlign: "center"
    },
    ".colorPrimary": {
        color: "color1"
    },
    ".btn": {
        display: "inline-block",
        padding: "1rem 2rem",
        marginBottom: "5px",
        color: "white",
        backgroundColor: "color1",
        _hover: {
            backgroundColor: "color4",
            cursor: "pointer"
        }
    },
    ".btn--large": {
        padding: "25px 100px",
        fontSize: "18px",
        textTransform: "uppercase",
    },
    ".btn--yellow": {
        backgroundColor: "yellow",
        color: "black",
        a: {
            color: "black"
        }
    },
    table: {
        width: "100%",
        my: "30px",
        tr: {
            borderBottom: "1px solid #fff",
            td: {
                pb: "15px"
            }
        },
        td: {
            "&.center": {
                textAlign: "center"
            },
            "&.w20": {width: "20%"},
            "&.w40": {width: "40%"},
            "&.bg20": {backgroundColor: "rgba(255,255,255,0.2)"},
            "&.bg40": {backgroundColor: "rgba(255,255,255,0.4)"},
            "&.dim": {color: "rgba(255,255,255,0.2)"}
        },
        "&.table--plain": {
            tr: {
                borderBottom: "none"
            }
        }

    },
    ".table--ScrollWrapper": {
        overflowX: "auto",
        position: "relative",
        table: {
            minWidth: "600px"
        }
    },
    ".table--centeredExceptFirst": {
        "td:not(:first-of-type)": {
            textAlign: "center"
        }
    },
    ".table--borders": {
        border: "1px solid #fff",
        td: {
            borderRight: "1px solid #fff",
            padding: "20px",
            verticalAlign: "top",
            p: {
                m: 0
            }
        }
    },
    ".notebox": {
        padding: "20px",
        margin: "20px 0",
        border: "1px solid #fff"
    },
    "&.colorMode--light": {
        color: "#333",
        "h1, h2, h3, a": {
            color: "color2"
        }
    },
    ".logo-list": {
        display: "flex",
        flexWrap: "wrap",
        listStyle: "none",
        margin: "0 0 0 -20px",
        padding: 0,
        alignItems: "center",
        maxWidth: "1100px",
        "li": {
            maxWidth: "250px",
            padding: "20px"
        }
    },
    ".iconTable": {
        color: "#000",
        margin: "20px 0"
    },
    ".iconTable_row": {
        backgroundColor: "color8",
        marginBottom: 1,
        display: "flex",
        flexDirection: "column"
    },
    ".iconTable_iconCell": {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    ".iconTable_iconCell img": {
        width: "100px",
        height: "100px",
    },
    ".iconTable_textCell": {
        padding: "20px"
    },
    ".iconTable_textCell h4": {
        marginTop: 0,
        color: "color2"
    },
    "@media screen and (min-width: 768px)": {
        ".iconTable_row": {
            flexDirection: "row"
        },
        ".iconTable_iconCell": {
            width: "12.5%"
        },
        ".iconTable_textCell": {
            paddingLeft: "0"
        }
    },
    ".colorPalette": {
        display: "flex",
        flexWrap: "wrap",
        "> div": {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100px",
            height: "100px",
            color: "#fff",
            marginRight: "5px",
            marginBottom: "5px"
        }
    }

}