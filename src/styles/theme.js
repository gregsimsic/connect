/*
 * Chakra theme defaults
 *
 * This file should contain base styles that apply to ALL meetings
 * Essentially the default theme for all meetings
 */

import {extendTheme} from '@chakra-ui/react'
import {createBreakpoints} from '@chakra-ui/theme-tools'

import bodyStyles from './body'
import customThemeStyles from './customMeetingStyles'
import customMeetingStyles from "./customMeetingStyles";

export default function makeTheme(siteTheme, locale) {

    // const meetingStyles = customMeetingStyles[locale]

    const fonts = {
        heading: "canada-type-gibson",
        body: "ubuntu"
    }

    const breakpoints = createBreakpoints({
        sm: '40em', // 640
        md: '52em', // 832
        lg: '64em', // 1024
        menu: '75em', // 1200
        xl: '75em', // 1200
        xxl: '80em', // 1280
    })

    const colors = {
        color1: siteTheme.colorPalette[0].color1,
        color2: siteTheme.colorPalette[0].color2,
        color3: siteTheme.colorPalette[0].color3,
        color4: siteTheme.colorPalette[0].color4,
        color5: siteTheme.colorPalette[0].color5,
        color6: siteTheme.colorPalette[0].color6,
        color7: siteTheme.colorPalette[0].color7,
        color8: siteTheme.colorPalette[0].color8,
        color9: siteTheme.colorPalette[0].color9,
        color10: siteTheme.colorPalette[0].color10,
        color11: siteTheme.colorPalette[0].color11,
        color12: siteTheme.colorPalette[0].color12,
        color13: siteTheme.colorPalette[0].color13,
        color14: siteTheme.colorPalette[0].color14,
        color15: siteTheme.colorPalette[0].color15,
        color16: siteTheme.colorPalette[0].color16,
        grayMedium: "#ddd",
        red: "#ea002a",
        yellow: "#F3BD00",
        error: "#C53030"
    }

    let global = {
        body: {
            fontFamily: "canada-type-gibson",
            bg: colors.color5,
            color: "white",
            minHeight: "100vh!important"
        },
        "b, strong": {
            fontWeight: 600
        },
        svg: {
            display: "inline-block",
            verticalAlign: "unset"
        },
        h1: {
            fontSize: "34px",
            fontWeight: 600,
            color: colors.color1,
        },
        h2: {
            fontSize: "24px",
            fontWeight: 600,
            color: colors.color4,
            mt: "20px",
        },
        h3: {
            fontSize: "20px",
            fontWeight: 600,
            color: colors.color1,
            mt: "20px",
        },
        h4: {
            fontSize: "18px",
            fontWeight: 600,
            color: colors.color4,
            mt: "20px",
        },
        p: {
            mt: "15px",
            maxW: "900px",
        },
        // "img, svg, video, canvas, audio, iframe, embed, object": {
        //     display: "inline-block",
        //     verticalAlign: "unset"
        // },
        a: {
            _hover: {
                textDecoration: "none"
            },
        },
        ".body": bodyStyles,
        ".slick-track": {
            margin: 0
        },
        ".slick-dots": {
            bottom: "-55px",
            "li button:before": {
                fontSize: "12px"
            }
        },
        ".channels-top-slider": {
            ".slick-dots": {
                bottom: "20px",
                "li button:before": {
                    fontSize: "17px",
                    color: "#fff",
                    opacity: 0.5
                },
                "li.slick-active button:before": {
                    opacity: 1
                }
            },
            ".slick-arrow": {
                width: "50px",
                height: "50px"
            },
            ".slick-prev": {
                left: "-5px",
            },
            ".slick-next": {
                right: "-5px"
            }
        },
        ".video-slider": {
            ".slick-arrow": {
                top: { base: "45%", lg: "87px" }
            }
        },
        ".video-js": {
            width: "100%",
            paddingTop: "56.25%",
            "video": {
                position: "absolute",
                top: 0,
                left: 0
            }
        },
        header: {
            h3: {
                color: "#fff"
            }
        },
        ".color1": { color: "color1"},
        ".color1-bg": { bgColor: "color1"},
        ".color2": { color: "color2"},
        ".color2-bg": { bgColor: "color2"},
        ".color3": { color: "color3"},
        ".color3-bg": { bgColor: "color3"},
        ".color4": { color: "color4"},
        ".color4-bg": { bgColor: "color4"},
        ".color5": { color: "color5"},
        ".color5-bg": { bgColor: "color5"},
        ".color6": { color: "color6"},
        ".color6-bg": { bgColor: "color6"},
        ".color7": { color: "color7"},
        ".color7-bg": { bgColor: "color7"},
        ".color8": { color: "color8"},
        ".color8-bg": { bgColor: "color8"},
        ".color9": { color: "color9"},
        ".color9-bg": { bgColor: "color9"},
        ".color10": { color: "color10"},
        ".color10-bg": { bgColor: "color10"},
        ".color11": { color: "color11"},
        ".color11-bg": { bgColor: "color11"},
        ".color12": { color: "color12"},
        ".color12-bg": { bgColor: "color12"},
        ".color13": { color: "color13"},
        ".color13-bg": { bgColor: "color13"},
        ".color14": { color: "color14"},
        ".color14-bg": { bgColor: "color14"},
        ".color15": { color: "color15"},
        ".color15-bg": { bgColor: "color15"},
        ".color16": { color: "color16"},
        ".color16-bg": { bgColor: "color16"},
    }

    // global = {...global, ...meetingStyles}

    const textStyles = {
        homeHeading: {
            fontSize: ["38px", "52px"],
            fontFamily: "canada-type-gibson",
            fontWeight: 600,
            marginY: "30px",
            color: "color1"
        }
    }

    const fontSizes = {
        xs: "12px",
        sm: "14px",
        md: "16px",
        lg: "18px",
        xl: "20px",
        "2xl": "24px",
        "3xl": "28px",
        "4xl": "36px",
        "5xl": "48px",
        "6xl": "64px",
    }

    const components = {
        Heading: {
            baseStyle: {
                fontWeight: 600
            }
        },
        Container: {
            baseStyle: {
                padding: 0
            }
        }
    }

    const theme = {
        initialColorMode: "dark",
        styles: {
            global
        },
        fonts,
        fontSizes,
        breakpoints,
        colors,
        sizes: {
            largeSizes: {
                sm: "640px",
                md: "768px",
                lg: "1024px",
                xl: "1280px",
            }
        },
        textStyles,
        components,
        header: {
          h: { base: "44px", md: "62px" }
        },
        backgroundImage: siteTheme.backgroundImage
    }

    return extendTheme(theme)
}
